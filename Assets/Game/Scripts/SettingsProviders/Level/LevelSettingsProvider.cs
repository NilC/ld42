namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class LevelSettingsProvider : SettingsProvider
    {
        public static string Tag = "LevelSettingsProvider";
        
        public float MinX;
        public float MaxX;
        public float MinY;
        public float MaxY;

        public int MaxGuysCountToLose;
        public float TimeToHold;

        public int RocketProjectileDatabaseId;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawLine(new Vector3(MinX, MinY), new Vector3(MaxX, MinY));
            Gizmos.DrawLine(new Vector3(MaxX, MinY), new Vector3(MaxX, MaxY));
            Gizmos.DrawLine(new Vector3(MaxX, MaxY), new Vector3(MinX, MaxY));
            Gizmos.DrawLine(new Vector3(MinX, MaxY), new Vector3(MinX, MinY));
        }
    }
}
