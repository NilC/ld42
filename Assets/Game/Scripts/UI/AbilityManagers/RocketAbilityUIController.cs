﻿namespace Brutime.LD42
{
    using System;
    using System.Collections.Generic;
    using BruCore;
    using UnityEngine;
    using UnityEngine.UI;

    public class RocketAbilityUIController : Controller, ISerializationCallbackReceiver
    {
        public List<Abilities> Skills;
        public List<Transform> ReloadingBars;

        private Dictionary<Abilities, Transform> abilityProgressBarPairs;

        //public Color DefaultColor;
        //public Color SelectedColor;

        //void Start()
        //{
        //    foreach (var pair in abilityProgressBarPairs)
        //    {
        //        pair.Value.GetComponent<Image>().color = DefaultColor;
        //    }
        //}

        void Update ()
        {
            UpdateProgressBars();
            //UpdateActiveAbilitySelection();
        }

        private void UpdateActiveAbilitySelection()
        {
            //var messages = MessageBus.ActiveAbilityChangedMessages.GetAll();
            //
            //for (int i = 0; i < messages.Count; i++)
            //{
            //    var message = messages[i];
            //
            //    abilityProgressBarPairs[message.NewActiveAbility].GetComponent<Image>().color = SelectedColor;
            //    abilityProgressBarPairs[message.OldActiveAbility].GetComponent<Image>().color = DefaultColor;
            //}
            //
            //MessageBus.ActiveAbilityChangedMessages.Clear();
        }

        private void UpdateProgressBars()
        {
            var messages = MessageBus.UpdateReloadigBarMessages.GetAll();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                abilityProgressBarPairs[message.AbilityType].GetComponent<Image>().fillAmount = message.Progress;
            }

            MessageBus.UpdateReloadigBarMessages.Clear();
        }

        public void OnBeforeSerialize()
        {
            Skills.Clear();
            ReloadingBars.Clear();
            
            foreach (var pair in abilityProgressBarPairs)
            {
                Skills.Add(pair.Key);
                ReloadingBars.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            abilityProgressBarPairs = new Dictionary<Abilities, Transform>();

            for (int i = 0; i != Math.Min(Skills.Count, ReloadingBars.Count); i++)
            {
                abilityProgressBarPairs.Add(Skills[i], ReloadingBars[i]);
            }
        }
    }
}