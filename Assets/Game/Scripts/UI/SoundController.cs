﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class SoundController : Controller
    {
        [SerializeField]
        public AudioClip CastRocketAudio;

        [SerializeField]
        public AudioClip Explosion;

        [SerializeField]
        public AudioClip RepellerAudioClip;

        [SerializeField]
        public AudioClip RadiationCloudAudioClip;

        private void Update()
        {
            ProcessRocketShootMessages();
            ProcessPlayRepellerMessages();
            ProcessPlayRadiationCloudMessages();

            ProcessPlayExplosionMessages();
        }

        private void ProcessRocketShootMessages()
        {
            var messages = MessageBus.PlayRocketShootSoundMessages.GetAll();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                AudioSource.PlayClipAtPoint(CastRocketAudio, message.Position);
            }

            MessageBus.PlayRocketShootSoundMessages.Clear();
        }

        private void ProcessPlayExplosionMessages()
        {
            var messages = MessageBus.PlayRocketExplosionMessages.GetAll();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                AudioSource.PlayClipAtPoint(Explosion, message.Position);
            }
            MessageBus.PlayRocketExplosionMessages.Clear();
        }

        private void ProcessPlayRepellerMessages()
        {
            var messages = MessageBus.PlayRepellerSoundMessages.GetAll();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                AudioSource.PlayClipAtPoint(RepellerAudioClip, message.Position);
            }
            MessageBus.PlayRepellerSoundMessages.Clear();
        }

        private void ProcessPlayRadiationCloudMessages()
        {
            var messages = MessageBus.PlayRadiationCloudMessages.GetAll();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                AudioSource.PlayClipAtPoint(RadiationCloudAudioClip, message.Position);
            }
            MessageBus.PlayRadiationCloudMessages.Clear();
        }
    }
}


