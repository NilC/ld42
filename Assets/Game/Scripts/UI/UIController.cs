﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;
    using UnityEngine.UI;

    public class UIController : Controller
    {
        [SerializeField]
        private Text timerText;

        [SerializeField]
        private Text counterText;

        [SerializeField]
        private Text startLevelText;

        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            timerText.text = string.Format("{0:0.00}", Subsystems.GameDirector.CurrentTimeToHold);

            counterText.text = string.Format("{0}/{1} ", Components.Guys.GetEntitiesIds().Count, SettingsProviders.Level.MaxGuysCountToLose);

            if (MessageBus.StartLevelMessages.Contains())
            {
                var levelNumber = MessageBus.StartLevelMessages.Get().LevelNumber;
                startLevelText.text = string.Format("Level {0}. No more {1}!!", levelNumber, SettingsProviders.Level.MaxGuysCountToLose);
                animator.SetTrigger("StartLevel");
                MessageBus.StartLevelMessages.Clear();
            }

            if (!MessageBus.LevelCompletedMessage.IsEmpty())
            {
                animator.SetTrigger("ShowLevelCompleted");
            }

            if (!MessageBus.GameCompletedMessages.IsEmpty())
            {
                animator.SetTrigger("ShowGameCompleted");
            }

            if (!MessageBus.GameOverMessages.IsEmpty())
            {
                animator.SetTrigger("GameOver");
            }
        }

        public void Restart()
        {
            MessageBus.RestartLevelMessages.Publish(new RestartLevelMessage { });
        }

        public void NextLevel()
        {
            MessageBus.NextLevelMessages.Publish(new NextLevelMessage { });
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
