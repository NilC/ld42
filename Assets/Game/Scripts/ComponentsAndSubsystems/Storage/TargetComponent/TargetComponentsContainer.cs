﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class TargetComponentsContainer : ComponentsContainer<TargetComponent, TargetComponentDataProvider>
    {
        public static string ComponentTag = "Target";

        public override void GetOrCreateNewComponentAndAdd(int entityId, TargetComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.TargetTransform = CoreComponents.Entities[entityId].GameObject.GetTransformWithTag(ComponentTag);
            AddComponent(entityId, component);
        }
    }
}

