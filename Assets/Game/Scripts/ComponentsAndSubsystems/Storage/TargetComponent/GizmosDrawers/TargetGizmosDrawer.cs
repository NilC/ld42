namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class TargetGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {            
            var targetTransforms = CoreComponents
                .Entities[entityId]
                .GameObject
                .GetTransformsWithTag(TargetComponentsContainer.ComponentTag);

            if (targetTransforms.Length == 1)
            {
                DrawCross(targetTransforms[0].position, new Color(0.906f, 0.298f, 0.235f));
            }
        }
    }
}
