﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct TargetComponent
    {
        public Transform TargetTransform;
    }
}