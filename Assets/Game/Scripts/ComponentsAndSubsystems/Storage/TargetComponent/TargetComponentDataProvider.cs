﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using System;

    public class TargetComponentDataProvider : ComponentDataProvider
    {
        public override Type GetComponentType()
        {
            return typeof(TargetComponent);
        }

        public TargetComponent GetComponent()
        {
            return new TargetComponent();
        }

#if UNITY_EDITOR
        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[] { new TargetGizmosDrawer() };
        }
#endif
    }
}
