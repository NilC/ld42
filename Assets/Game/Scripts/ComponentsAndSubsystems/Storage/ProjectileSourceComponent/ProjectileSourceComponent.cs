﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct ProjectileSourceComponent
    {
        public Transform SourceTransform;
    }
}