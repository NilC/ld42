﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using System;

    public class ProjectileSourceComponentDataProvider : ComponentDataProvider
    {
        public override Type GetComponentType()
        {
            return typeof(ProjectileSourceComponent);
        }

        public ProjectileSourceComponent GetComponent()
        {
            return new ProjectileSourceComponent();
        }

#if UNITY_EDITOR
        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[] { new ProjectileSourceGizmosDrawer() };
        }
#endif
    }
}
