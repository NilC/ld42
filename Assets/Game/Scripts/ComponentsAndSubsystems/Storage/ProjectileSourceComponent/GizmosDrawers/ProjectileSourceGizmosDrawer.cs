namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class ProjectileSourceGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            var projectileSourceTransforms = CoreComponents
                .Entities[entityId]
                .GameObject
                .GetTransformsWithTag(ProjectileSourceComponentsContainer.ComponentTag);

            if (projectileSourceTransforms.Length == 1)
            {
                DrawCross(projectileSourceTransforms[0].position, new Color(0.18f, 0.8f, 0.443f));
            }
        }
    }
}
