﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class ProjectileSourceComponentsContainer : ComponentsContainer<ProjectileSourceComponent, ProjectileSourceComponentDataProvider>
    {
        public static string ComponentTag = "ProjectileSource";

        public override void GetOrCreateNewComponentAndAdd(int entityId, ProjectileSourceComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.SourceTransform = CoreComponents.Entities[entityId].GameObject.GetTransformWithTag(ComponentTag);
            AddComponent(entityId, component);
        }
    }
}

