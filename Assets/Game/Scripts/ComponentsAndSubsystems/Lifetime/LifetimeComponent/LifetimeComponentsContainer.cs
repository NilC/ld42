﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class LifetimeComponentsContainer : CommonComponentsContainer<LifetimeComponent, LifetimeComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, LifetimeComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Lifetime = 0;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.Lifetime = 0;
            Components[entityId] = component;
        }
    }
}
