﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class LifetimeComponentDataProvider : CommonComponentDataProvider<LifetimeComponent>
    {
#if UNITY_EDITOR

        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "Lifetime" };
        }
#endif
    }
}
