﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct LifetimeComponent
    {
        public float TimeToLive;
        public float Lifetime;
    }
}
