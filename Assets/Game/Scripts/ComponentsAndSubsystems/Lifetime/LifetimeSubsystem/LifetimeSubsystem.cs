﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class LifetimeSubsystem : Subsystem
    {
        public override void Update()
        {
            foreach (var entityId in Components.Lifetimes.GetEntitiesIds())
            {
                var component = Components.Lifetimes[entityId];
                component.Lifetime += Time.deltaTime;

                if (component.Lifetime >= component.TimeToLive)
                {
                    Destroy(entityId);
                }

                Components.Lifetimes[entityId] = component;
            }
        }

        private void Destroy(int entityId)
        {
            MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });
        }
    }
}
