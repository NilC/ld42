﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct ProjectileDebrisComponent
    {
        public float InitialLifetime;
        public float Lifetime;
        public float CurrentLifetime;
    }
}