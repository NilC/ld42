﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class ProjectileDebrisComponentsContainer : ComponentsContainer<ProjectileDebrisComponent, ProjectileDebrisComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, ProjectileDebrisComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Lifetime = component.InitialLifetime;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];

            component.Lifetime = component.InitialLifetime;
            component.CurrentLifetime = 0.0f;

            Components[entityId] = component;
        }
    }
}

