﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class ProjectileDebrisComponentDataProvider : CommonComponentDataProvider<ProjectileDebrisComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Lifetime", "CurrentLifetime" };
        }
#endif
    }
}
