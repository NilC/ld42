namespace Brutime.LD42
{
    using BruCore;

    public class PositionProjectileComponentsContainer : CommonComponentsContainer<PositionProjectileComponent, PositionProjectileComponentDataProvider>
    { 
    }
}
