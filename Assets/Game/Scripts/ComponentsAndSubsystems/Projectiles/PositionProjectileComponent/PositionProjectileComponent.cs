namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct PositionProjectileComponent
    {
        public Ability Ability;
        public int ProjectileDebrisId;
        public int ProjectileId;
        public int OwnerEntityId;
        public Vector3 TargetPosition;
        public float Radius;
        public bool IsRotating;
    }
}