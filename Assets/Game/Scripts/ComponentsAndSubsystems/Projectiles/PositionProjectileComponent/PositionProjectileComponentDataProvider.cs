namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class PositionProjectileComponentDataProvider : CommonComponentDataProvider<PositionProjectileComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "OwnerEntityId", "TargetPosition" };
        }      
#endif
    }
}
