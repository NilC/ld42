﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class TargetProjectileComponentsContainer : CommonComponentsContainer<TargetProjectileComponent, TargetProjectileComponentDataProvider>
    {
    }
}

