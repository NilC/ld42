﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class TargetProjectileComponentDataProvider : CommonComponentDataProvider<TargetProjectileComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "ProjectileId", "OwnerEntityId", "TargetEntityId" };
        }
#endif
    }
}
