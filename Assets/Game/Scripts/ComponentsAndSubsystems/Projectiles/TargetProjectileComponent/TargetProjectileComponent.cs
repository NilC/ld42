﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct TargetProjectileComponent
    {
        public Ability Ability;
        public int ProjectileDebrisId;
        public int ProjectileId;
        public int OwnerEntityId;
        public int TargetEntityId;
    }
}