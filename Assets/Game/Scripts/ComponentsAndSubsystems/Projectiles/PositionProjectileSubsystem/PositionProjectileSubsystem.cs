﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class PositionProjectileSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessCreatePositionProjectileMessages();
            ProcessProjectileReachedTargetMessages();
        }

        private void ProcessCreatePositionProjectileMessages()
        {
            var messages = MessageBus.CreatePositionProjectileMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                var entityId = Factories.Entities.CreateEntity(message.ProjectileDatabaseId, message.Position);

                var component = Components.PositionProjectiles[entityId];
                component.ProjectileId = message.ProjectileDatabaseId;
                component.OwnerEntityId = message.OwnerEntityId;
                Components.PositionProjectiles[entityId] = component;

                if (component.IsRotating)
                {
                    var transform = Components.Positioning[entityId].Transform;
                    Vector3 targetDir = message.TargetPosition - message.Position;
                    var angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;                   
                    Components.Positioning[entityId].Transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                }

                MessageBus.UpdatePointMessages.Publish(entityId, new UpdatePointMessage
                {
                    EntityId = entityId,
                    Point = message.TargetPosition
                });
            }
            MessageBus.CreatePositionProjectileMessages.Clear();
        }

        private void ProcessProjectileReachedTargetMessages()
        {
            foreach (var entityId in Components.PositionProjectiles.GetEntitiesIds())
            {
                PointReachedMessage message;
                if (MessageBus.PointReachedMessages.TryGetValue(entityId, out message))
                {
                    var component = Components.PositionProjectiles[entityId];
                    MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });

                    MessageBus.CreateProjectileDebrisMessages.Publish(new CreateProjectileDebrisMessage
                    {
                        ProjectileDebrisId = component.ProjectileDebrisId,
                        Position = message.Point,
                        UseTargetEntityId = true,
                        TargetEntityId = 0
                    });

                    var guysEntitiesIds = Components.Guys.GetEntitiesIds();
                    foreach (var guyEntityId in guysEntitiesIds)
                    {
                        if (IsCloseEnough(message.Point, component.Radius, guyEntityId))
                        {
                            component.Ability.Apply(guyEntityId);
                        }
                    }
                }
            }

            var messagesPairs = MessageBus.PointReachedMessages.GetAll();

            foreach (var messagePair in messagesPairs)
            {
                var message = messagePair.Value;
                var entityId = message.EntityId;

                TargetProjectileComponent component;
                var isExists = Components.TargetProjectiles.TryGet(entityId, out component);
                if (isExists)
                {
                    MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });

                    MessageBus.CreateProjectileDebrisMessages.Publish(new CreateProjectileDebrisMessage
                    {
                        ProjectileDebrisId = component.ProjectileDebrisId,
                        Position = message.Point,
                        UseTargetEntityId = true,
                        TargetEntityId = component.TargetEntityId
                    });

                    component.Ability.Apply(component.TargetEntityId);
                }
            }
        }

        private bool IsCloseEnough(Vector3 position, float radius, int guyEntityId)
        {
            var vectorToTarget = position - Components.Positioning[guyEntityId].Transform.position;
            return vectorToTarget.sqrMagnitude <= radius * radius;
        }
    }
}
