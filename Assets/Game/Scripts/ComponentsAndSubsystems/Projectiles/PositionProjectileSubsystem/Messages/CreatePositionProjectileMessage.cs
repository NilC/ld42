namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct CreatePositionProjectileMessage
    {
        public int ProjectileDatabaseId;
        public Vector3 Position;
        public int OwnerEntityId;
        public Vector3 TargetPosition;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreatePositionProjectileMessage> CreatePositionProjectileMessages = new ListMessagesChannel<CreatePositionProjectileMessage>();
    }    
}