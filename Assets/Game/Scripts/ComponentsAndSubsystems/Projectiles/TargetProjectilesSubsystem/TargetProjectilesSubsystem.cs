﻿namespace Brutime.LD42
{
    using BruCore;

    public class TargetProjectilesSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessCreateTargetProjectileMessages();
            ProcessProjectileReachedTargetMessages();

            UpdateTargetsPoints();
        }

        private void ProcessCreateTargetProjectileMessages()
        {
            var messages = MessageBus.CreateTargetProjectileMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                var entityId = Factories.Entities.CreateEntity(message.ProjectileDatabaseId, message.Position);

                var component = Components.TargetProjectiles[entityId];
                component.ProjectileId = message.ProjectileDatabaseId;
                component.OwnerEntityId = message.OwnerEntityId;
                Components.TargetProjectiles[entityId] = component;
            }
            MessageBus.CreateTargetProjectileMessages.Clear();
        }

        private void ProcessProjectileReachedTargetMessages()
        {
            var messagesPairs = MessageBus.PointReachedMessages.GetAll();

            foreach (var messagePair in messagesPairs)
            {
                var message = messagePair.Value;
                var entityId = message.EntityId;

                TargetProjectileComponent component;
                var isExists = Components.TargetProjectiles.TryGet(entityId, out component);
                if (isExists)
                {
                    MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });

                    MessageBus.CreateProjectileDebrisMessages.Publish(new CreateProjectileDebrisMessage
                    {
                        ProjectileDebrisId = component.ProjectileDebrisId,
                        Position = message.Point,
                        UseTargetEntityId = true,
                        TargetEntityId = component.TargetEntityId
                    });

                    component.Ability.Apply(component.TargetEntityId);
                }
            }
        }

        private void UpdateTargetsPoints()
        {
            foreach (var entityId in Components.TargetProjectiles.GetEntitiesIds())
            {
                var component = Components.TargetProjectiles[entityId];
                UpdateTargetPoint(entityId, component.TargetEntityId);
            }
        }

        private void UpdateTargetPoint(int entityId, int targetEntityId)
        {
            if (MessageBus.PointReachedMessages.Contains(entityId))
            {
                return;
            }

            //Already destroyed
            if (!Components.Targets.Contains(targetEntityId))
            {
                return;
            }

            var targetPosition = Components.Targets[targetEntityId].TargetTransform.position;

            MessageBus.UpdatePointMessages.Publish(entityId, new UpdatePointMessage
            {
                EntityId = entityId,
                Point = targetPosition
            });
        }
    }
}
