﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct CreateTargetProjectileMessage
    {
        public int ProjectileDatabaseId;
        public Vector3 Position;
        public int OwnerEntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreateTargetProjectileMessage> CreateTargetProjectileMessages = new ListMessagesChannel<CreateTargetProjectileMessage>();
    }
}