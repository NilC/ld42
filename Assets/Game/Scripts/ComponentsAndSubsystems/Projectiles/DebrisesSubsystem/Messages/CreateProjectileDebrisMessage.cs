﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct CreateProjectileDebrisMessage
    {
        public int ProjectileDebrisId;
        public Vector3 Position;
        public bool UseTargetEntityId;
        public int TargetEntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreateProjectileDebrisMessage> CreateProjectileDebrisMessages = new ListMessagesChannel<CreateProjectileDebrisMessage>();
    }
}