﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class ProjectileDebrisesSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessCreateProjectileDebrisMessages();
            ProcessLifetime();
        }

        private void ProcessCreateProjectileDebrisMessages()
        {
            var messages = MessageBus.CreateProjectileDebrisMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                Factories.Entities.CreateEntity(message.ProjectileDebrisId, message.Position);
                
                MessageBus.PlayRocketExplosionMessages.Publish(new PlayRocketExplosionMessage
                {
                    Position = message.Position
                });
            }

            MessageBus.CreateProjectileDebrisMessages.Clear();
        }

        private void ProcessLifetime()
        {
            foreach (var entityId in Components.ProjectileDebrises.GetEntitiesIds())
            {
                var component = Components.ProjectileDebrises[entityId];
                component.CurrentLifetime += Time.deltaTime;
                Components.ProjectileDebrises[entityId] = component;

                if (component.CurrentLifetime >= component.Lifetime)
                {
                    MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });
                }
            }
        }
    }
}
