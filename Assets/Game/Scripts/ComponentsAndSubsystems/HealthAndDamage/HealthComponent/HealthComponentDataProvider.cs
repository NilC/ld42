﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class HealthComponentDataProvider : CommonComponentDataProvider<HealthComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Health", "CurrentAccumulatedDamage", "IsDead" };
        }
#endif
    }
}
