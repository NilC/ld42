﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct HealthComponent
    {
        public int MaxHealth;
        public int Health;
        public float CurrentAccumulatedDamage;
        public bool IsImmortal;
        public bool IsDead;
    }
}
