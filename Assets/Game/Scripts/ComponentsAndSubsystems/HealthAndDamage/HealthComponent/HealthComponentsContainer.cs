﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class HealthComponentsContainer : ComponentsContainer<HealthComponent, HealthComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, HealthComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Health = component.MaxHealth;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];

            component.Health = component.MaxHealth;
            component.CurrentAccumulatedDamage = 0.0f;
            component.IsImmortal = false;
            component.IsDead = false;

            Components[entityId] = component;
        }

        public bool IsFullHealth(int entityId)
        {
            var component = Components[entityId];
            return component.Health == component.MaxHealth;
        }

        public float GetHealthFactor(int entityId)
        {
            var component = Components[entityId];
            return component.Health / (float)component.MaxHealth;
        }

        public bool IsAlive(int entityId)
        {
            var component = Components[entityId];
            return !component.IsDead;
        }
    }
}

