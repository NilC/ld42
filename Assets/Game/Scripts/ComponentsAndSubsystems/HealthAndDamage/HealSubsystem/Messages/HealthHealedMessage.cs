﻿namespace Brutime.LD42
{
    using BruCore;

    public struct HealthHealedMessage
    {
        public int EntityId;
        public int HealedValue;
        public float HealthFactor;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<HealthHealedMessage> HealthHealedMessages = new ListMessagesChannel<HealthHealedMessage>();
    }
}
