﻿namespace Brutime.LD42
{
    using BruCore;

    public struct HealHealthMessage
    {
        public int EntityId;
        public int HealValue;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<HealHealthMessage> HealHealthMessages = new ListMessagesChannel<HealHealthMessage>();
    }
}
