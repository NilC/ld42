﻿namespace Brutime.LD42
{
    using BruCore;

    public class HealSubsystem : Subsystem
    {
        public override void Update()
        {
            MessageBus.HealHealthMessages.Clear();            

            ProcessHealHealthMessages();
        }

        private void ProcessHealHealthMessages()
        {
            var messages = MessageBus.HealHealthMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                var entityId = message.EntityId;
                var healValue = message.HealValue;

                var component = Components.Health[entityId];

                if (component.IsDead)
                {
                    continue;
                }

                if (component.Health + healValue > component.MaxHealth)
                {
                    healValue = component.MaxHealth - component.Health;
                }

                if (healValue > 0)
                {
                    component.Health += healValue;
                    Components.Health[entityId] = component;
                }

                PublishNewHealthHealedMessage(entityId, healValue);
            }

            MessageBus.HealHealthMessages.Clear();
        }

        private void PublishNewHealthHealedMessage(int entityId, int healValue)
        {
            var healthHealedMessage = new HealthHealedMessage
            {
                EntityId = entityId,
                HealedValue = healValue,
                HealthFactor = Components.Health.GetHealthFactor(entityId)
            };

            MessageBus.HealthHealedMessages.Publish(healthHealedMessage);
        }
    }
}
