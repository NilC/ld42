﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class DefenceComponentsContainer : ComponentsContainer<DefenceComponent, DefenceComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, DefenceComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}

