﻿namespace Brutime.LD42
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public struct DefenceComponent
    {
        public Dictionary<DamageType, int> DamageResistances;
    }
}
