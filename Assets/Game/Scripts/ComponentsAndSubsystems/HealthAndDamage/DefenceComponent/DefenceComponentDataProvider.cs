﻿namespace Brutime.LD42
{
    using System;
    using System.Collections.Generic;
    using Brutime.BruCore;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    public class DefenceComponentDataProvider : ComponentDataProvider
    {
        public int Armor;
        public int FireResistance;
        public int ColdResistance;
        public int PoisonResistance;
        public int UndeadResistance;

        public override Type GetComponentType()
        {
            return typeof(DefenceComponent);
        }

        public DefenceComponent GetComponent()
        {
            var component = new DefenceComponent
            {
                DamageResistances = new Dictionary<DamageType, int>(DamageTypeComparer.Default)
                {
                    {DamageType.Pure, Armor},
                    {DamageType.Fire, FireResistance},
                    {DamageType.Cold, ColdResistance},
                    {DamageType.Poison, PoisonResistance},
                    {DamageType.Undead, UndeadResistance}
                }
            };

            return component;
        }

#if UNITY_EDITOR
        public override void DrawLayout()
        {
            Armor = EditorGUILayout.IntField("Armor", Armor);
            FireResistance = EditorGUILayout.IntField("Fire Resistance", FireResistance);
            ColdResistance = EditorGUILayout.IntField("Cold Resistance", ColdResistance);
            PoisonResistance = EditorGUILayout.IntField("Poison Resistance", PoisonResistance);
            UndeadResistance = EditorGUILayout.IntField("Undead Resistance", UndeadResistance);
        }
#endif
    }
}
