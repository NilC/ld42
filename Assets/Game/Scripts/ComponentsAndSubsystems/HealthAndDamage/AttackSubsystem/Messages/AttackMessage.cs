﻿namespace Brutime.LD42
{
    using BruCore;

    public struct AttackMessage
    {
        public int TargetEntityId;
        public DamageType DamageType;
        public int DamageValueFrom;
        public int DamageValueTo;
        public float CriticalChance;
        public float CriticalModifier;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<AttackMessage> AttackMessages = new ListMessagesChannel<AttackMessage>();
    }
}
