﻿namespace Brutime.LD42
{
    using BruCore;

    public struct ResistMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<ResistMessage> ResistMessages = new ListMessagesChannel<ResistMessage>();
    }
}
