﻿namespace Brutime.LD42
{
    using System.Collections.Generic;

    public class DamageTypeComparer : IEqualityComparer<DamageType>
    {
        public static readonly DamageTypeComparer Default = new DamageTypeComparer();

        public bool Equals(DamageType x, DamageType y)
        {
            return x == y;
        }

        public int GetHashCode(DamageType obj)
        {
            return (int)obj;
        }
    }
}