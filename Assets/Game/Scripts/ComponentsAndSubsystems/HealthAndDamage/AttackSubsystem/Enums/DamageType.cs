﻿namespace Brutime.LD42
{
    public enum DamageType
    {
        Pure,
        Fire,
        Cold,
        Poison,
        Undead
    }
}