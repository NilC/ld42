﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class AttackSubsystem : Subsystem
    {
        public override void Update()
        {           
            MessageBus.ResistMessages.Clear();

            ProcessDamageMessages();
        }

        private void ProcessDamageMessages()
        {
            var messages = MessageBus.AttackMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.TargetEntityId;

                var damageValue = Random.Range(message.DamageValueFrom, message.DamageValueTo + 1);

                var isCritical = Random.Range(1, 101) <= message.CriticalChance;
                if (isCritical)
                {
                    damageValue = Mathf.CeilToInt(damageValue * message.CriticalModifier);
                }

                DefenceComponent component;
                if (Components.Defence.TryGet(entityId, out component))
                {
                    damageValue -= component.DamageResistances[message.DamageType];
                }

                if (damageValue > 0)
                {
                    var damageHealthMessage = new DamageHealthMessage
                    {
                        EntityId = entityId,
                        DamageValue = damageValue,
                        IsCritical = isCritical
                    };

                    MessageBus.DamageHealthMessages.Publish(damageHealthMessage);
                }
                else
                {
                    var resistMessage = new ResistMessage
                    {
                        EntityId = entityId
                    };

                    MessageBus.ResistMessages.Publish(resistMessage);
                }
            }

            MessageBus.AttackMessages.Clear();
        }
    }
}
