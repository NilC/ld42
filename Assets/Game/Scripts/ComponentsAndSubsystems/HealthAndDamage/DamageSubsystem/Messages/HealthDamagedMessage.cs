﻿namespace Brutime.LD42
{
    using BruCore;

    public struct HealthDamagedMessage
    {
        public int EntityId;
        public int DamagedValue;
        public bool IsCritical;
        public float HealthFactor;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<HealthDamagedMessage> HealthDamagedMessages = new ListMessagesChannel<HealthDamagedMessage>();
    }
}
