﻿namespace Brutime.LD42
{
    using BruCore;

    public struct InstantKillMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<InstantKillMessage> InstantKillMessages = new ListMessagesChannel<InstantKillMessage>();
    }
}
