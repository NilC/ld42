﻿namespace Brutime.LD42
{
    using BruCore;

    public struct DeadMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static DictionaryMessagesChannel<DeadMessage> DeadMessages = new DictionaryMessagesChannel<DeadMessage>();
    }
}
