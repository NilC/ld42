﻿namespace Brutime.LD42
{
    using BruCore;

    public struct DamageHealthMessage
    {
        public int EntityId;
        public int DamageValue;
        public bool IsCritical;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<DamageHealthMessage> DamageHealthMessages = new ListMessagesChannel<DamageHealthMessage>();
    }
}
