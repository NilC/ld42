﻿namespace Brutime.LD42
{
    using BruCore;

    public struct AccumulatedDamageHealthMessage
    {
        public int EntityId;
        public float AccumulatedDamage;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<AccumulatedDamageHealthMessage> AccumulatedDamageHealthMessages = new ListMessagesChannel<AccumulatedDamageHealthMessage>();
    }
}
