﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class DamageSubsystem : Subsystem
    {
        public override void Update()
        {            
            MessageBus.HealthDamagedMessages.Clear();
            MessageBus.DeadMessages.Clear();

            ProcessDamageHealthMessages();
            ProcessAccumulatedDamageHealthMessages();
            ProcessInstantKillMessage();
        }

        private void ProcessDamageHealthMessages()
        {
            var messages = MessageBus.DamageHealthMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                var entityId = message.EntityId;
                var damageValue = message.DamageValue;

                HealthComponent component;
                var isExists = Components.Health.TryGet(entityId, out component);

                if (!isExists || component.IsDead || component.IsImmortal)
                {
                    continue;
                }

                component = ReduceHealth(entityId, component, damageValue);
                Components.Health[entityId] = component;

                PublishNewHealthDamagedMessage(entityId, damageValue, message.IsCritical);
            }

            MessageBus.DamageHealthMessages.Clear();
        }

        private void ProcessAccumulatedDamageHealthMessages()
        {
            var messages = MessageBus.AccumulatedDamageHealthMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                var entityId = message.EntityId;
                var accumulatedDamage = message.AccumulatedDamage;

                var component = Components.Health[entityId];

                if (component.IsDead || component.IsImmortal)
                {
                    continue;
                }

                component.CurrentAccumulatedDamage += accumulatedDamage;

                var intPartOfDamage = 0;
                if (component.CurrentAccumulatedDamage > 1.0f)
                {
                    intPartOfDamage = Mathf.FloorToInt(accumulatedDamage);

                    component = ReduceHealth(entityId, component, intPartOfDamage);
                    component.CurrentAccumulatedDamage -= intPartOfDamage;
                    Components.Health[entityId] = component;
                }

                PublishNewHealthDamagedMessage(entityId, intPartOfDamage, false);
            }

            MessageBus.AccumulatedDamageHealthMessages.Clear();
        }

        private void ProcessInstantKillMessage()
        {
            var messages = MessageBus.InstantKillMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;

                var component = Components.Health[entityId];
                if (component.IsDead || component.IsImmortal)
                {
                    continue;
                }
                component = ReduceHealth(entityId, component, component.Health);
                Components.Health[entityId] = component;
            }

            MessageBus.InstantKillMessages.Clear();
        }

        private void PublishNewHealthDamagedMessage(int entityId, int damagedValue, bool isCritical)
        {
            var healthDamagedMessage = new HealthDamagedMessage
            {
                EntityId = entityId,
                DamagedValue = damagedValue,
                IsCritical = isCritical,
                HealthFactor = Components.Health.GetHealthFactor(entityId)
            };

            MessageBus.HealthDamagedMessages.Publish(healthDamagedMessage);
        }

        private HealthComponent ReduceHealth(int entityId, HealthComponent component, int resultDamage)
        {
            component.Health -= resultDamage;

            if (component.Health <= 0)
            {
                component.Health = 0;
                component.IsDead = true;

                MessageBus.DeadMessages.Publish(entityId, new DeadMessage { EntityId = entityId });
            }

            return component;
        }
    }
}
