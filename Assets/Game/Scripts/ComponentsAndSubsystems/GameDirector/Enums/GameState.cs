﻿namespace Brutime.LD42
{
    public enum GameState
    {
        Intro,
        Gameplay,
        GameOver,
        WaitingForRestart,
        LevelComplete,
        WaitingForNextLevel,
        End
    }
}
