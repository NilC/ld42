﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class GameDirector : Subsystem
    {
        public float CurrentTimeToHold = 0.0f;

        //ToDo: To Settings
        private float waitingForGameOverMenuTime = 0.4f;

        private static int scenesOffset = 2;
        private static int currentLevelIndex = 0;
        private static int levelCount = 4;

        [SerializeField]
        public GameState GameState = GameState.Intro;

        private float introTime = 0.0f;
        private float gameOverTime = 0.0f;

        private bool gameCompletedMessageWasSent = false;

        public override void Start()
        {
            //Time.timeScale = 3.0f;
            GameState = GameState.Intro;

            CurrentTimeToHold = SettingsProviders.Level.TimeToHold;

            introTime = 0.0f;
            gameOverTime = 0.0f;

            gameCompletedMessageWasSent = false;

            MessageBus.StartLevelMessages.Publish(new StartLevelMessage { LevelNumber = currentLevelIndex + 1 });

            Subsystems.Input.IsEnabled = false;

            Application.targetFrameRate = 60;
        }

        public override void Update()
        {
            switch (GameState)
            {
                case GameState.Intro:
                    ProcessIntroState();
                    ProcessRestartLevelMessages();
                    break;

                case GameState.Gameplay:
                    ProcessGameplay();
                    ProcessGameOverMessages();
                    ProcessRestartLevelMessages();
                    break;

                case GameState.GameOver:
                    ProcessGameOver();
                    ProcessRestartLevelMessages();
                    break;

                case GameState.WaitingForRestart:
                    ProcessRestartLevelMessages();
                    break;

                case GameState.LevelComplete:
                    ProcessLevelComplete();
                    break;

                case GameState.WaitingForNextLevel:
                    ProcessNextLevelMessages();
                    break;

                case GameState.End:
                    ProcessGameCompletedMessages();
                    break;
            }
        }

        private void ProcessGameplay()
        {
            CurrentTimeToHold -= Time.deltaTime;
            if (CurrentTimeToHold <= 0.0 || (MessageBus.CreateGuysMessages.GetAll().Count == 0 && Components.Guys.GetEntitiesIds().Count == 0))
            //if (CurrentTimeToHold <= 0.0 || Components.Guys.GetEntitiesIds().Count == 0)
            {
                if (currentLevelIndex == levelCount - 1)
                {
                    if (!gameCompletedMessageWasSent)
                    {
                        Subsystems.Input.IsEnabled = false;
                        CurrentTimeToHold = 0.0f;
                        MessageBus.GameCompletedMessages.Publish(new GameCompletedMessage { });
                        gameCompletedMessageWasSent = true;
                        GameState = GameState.End;
                    }
                }
                else
                {
                    CurrentTimeToHold = 0.0f;
                    MessageBus.LevelCompletedMessage.Publish(new LevelCompletedMessage { });
                    CompleteLevel();
                }
            }
            else if (Components.Guys.GetEntitiesIds().Count >= SettingsProviders.Level.MaxGuysCountToLose)
            {
                MessageBus.GameOverMessages.Publish(new GameOverMessage { });
            }
        }

        private void ProcessIntroState()
        {
            introTime += Time.deltaTime;
            if (introTime > 1.4f)
            {
                var respawnsEntitiesIds = Components.Respawns.GetEntitiesIds();
                foreach (var respawnrEntityId in respawnsEntitiesIds)
                {
                    MessageBus.StartSpawningMessages.Publish(new StartSpawningMessage { });
                    Subsystems.Input.IsEnabled = true;
                }
                GameState = GameState.Gameplay;
            }
        }

        private void ProcessGameOver()
        {
            MessageBus.GameOverMessages.Clear();
            gameOverTime += Time.deltaTime;
            if (gameOverTime > waitingForGameOverMenuTime || Input.GetButtonDown("Submit") || Input.GetButtonDown("Cancel"))
            {
                GameState = GameState.WaitingForRestart;
            }
        }

        private void ProcessGameOverMessages()
        {
            if (!MessageBus.GameOverMessages.IsEmpty())
            {
                SwitchToGameOverState();
            }
        }

        private void ProcessRestartLevelMessages()
        {
            if (!MessageBus.RestartLevelMessages.IsEmpty())
            {
                Subsystems.Input.IsEnabled = true;
                MessageBus.RestartLevelMessages.Clear();
                RestartLevel();
            }
        }

        private void ProcessLevelComplete()
        {
            if (!MessageBus.LevelCompletedMessage.IsEmpty())
            {
                MessageBus.LevelCompletedMessage.Clear();
            }

            GameState = GameState.WaitingForNextLevel;
        }

        private void CompleteLevel()
        {
            Subsystems.Input.IsEnabled = false;
            GameState = GameState.LevelComplete;
        }

        private void ProcessNextLevelMessages()
        {
            if (!MessageBus.NextLevelMessages.IsEmpty())
            {
                MessageBus.NextLevelMessages.Clear();
                Subsystems.Input.IsEnabled = true;
                currentLevelIndex++;
                SceneManager.LoadScene(scenesOffset + currentLevelIndex);
            }
        }

        private void ProcessGameCompletedMessages()
        {
            if (!MessageBus.GameCompletedMessages.IsEmpty())
            {
                MessageBus.GameCompletedMessages.Clear();
            }
        }

        private void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void SwitchToGameOverState()
        {
            Subsystems.Input.IsEnabled = false;
            GameState = GameState.GameOver;
        }
    }
}
