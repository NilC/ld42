﻿namespace Brutime.LD42
{
    using BruCore;

    public struct GameCompletedMessage
    {
        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<GameCompletedMessage> GameCompletedMessages = new SingleMessageChannel<GameCompletedMessage>();
    }
}