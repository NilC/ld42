﻿namespace Brutime.LD42
{
    using BruCore;

    public struct StartLevelMessage
    {
        public int LevelNumber;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<StartLevelMessage> StartLevelMessages = new SingleMessageChannel<StartLevelMessage>();
    }
}