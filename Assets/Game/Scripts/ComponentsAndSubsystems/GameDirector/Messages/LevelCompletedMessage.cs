﻿namespace Brutime.LD42
{
    using BruCore;

    public struct LevelCompletedMessage
    {
        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<LevelCompletedMessage> LevelCompletedMessage = new SingleMessageChannel<LevelCompletedMessage>();
    }
}