﻿namespace Brutime.LD42
{
    using BruCore;

    public struct RestartLevelMessage
    {
        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<RestartLevelMessage> RestartLevelMessages = new SingleMessageChannel<RestartLevelMessage>();
    }
}