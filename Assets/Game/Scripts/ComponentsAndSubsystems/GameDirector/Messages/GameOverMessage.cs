﻿namespace Brutime.LD42
{
    using BruCore;

    public struct GameOverMessage
    {
        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<GameOverMessage> GameOverMessages = new SingleMessageChannel<GameOverMessage>();
    }
}