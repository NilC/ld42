﻿namespace Brutime.LD42
{
    using BruCore;

    public struct NextLevelMessage
    {
        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<NextLevelMessage> NextLevelMessages = new SingleMessageChannel<NextLevelMessage>();
    }
}