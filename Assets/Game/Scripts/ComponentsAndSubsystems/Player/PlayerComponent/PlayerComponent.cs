namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct PlayerComponent
    {
        public bool IsFacingRight;
        public float ScaleX;

        public Abilities ActiveAbility;
    }
}