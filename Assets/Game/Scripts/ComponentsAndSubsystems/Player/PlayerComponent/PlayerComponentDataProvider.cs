namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class PlayerComponentDataProvider : CommonComponentDataProvider<PlayerComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "ActiveAbility" };
        }
#endif
    }
}
