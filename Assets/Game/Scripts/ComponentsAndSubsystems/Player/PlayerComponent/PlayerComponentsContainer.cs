namespace Brutime.LD42
{
    using BruCore;

    public class PlayerComponentsContainer : ComponentsContainer<PlayerComponent, PlayerComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, PlayerComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.ScaleX = CoreComponents.Entities[entityId].GameObject.transform.localScale.x;
            AddComponent(entityId, component);
        }
    }
}
