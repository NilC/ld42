namespace Brutime.LD42
{
    using BruCore;

    public struct PlayerPlaceRadiationCloudMessage
    {
        public int EntityId;
        public int GuyId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<PlayerPlaceRadiationCloudMessage> PlayerPlaceRadiationCloudMessages = new ListMessagesChannel<PlayerPlaceRadiationCloudMessage>();
    }    
}