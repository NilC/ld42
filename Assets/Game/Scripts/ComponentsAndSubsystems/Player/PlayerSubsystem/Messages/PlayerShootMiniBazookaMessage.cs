namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct PlayerShootMiniBazookaMessage
    {
        public int EntityId;
        public Vector3 TargetPosition;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<PlayerShootMiniBazookaMessage> PlayerShootMiniBazookaMessages = new ListMessagesChannel<PlayerShootMiniBazookaMessage>();
    }    
}