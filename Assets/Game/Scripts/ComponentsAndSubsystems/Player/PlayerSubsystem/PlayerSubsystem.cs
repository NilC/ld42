namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class PlayerSubsystem : Subsystem
    {
        private Camera mainCamera;

        public override void Start()
        {
            mainCamera = Camera.main;

            var windowAspect = Screen.width / (float)Screen.height;
            var scaleHeight = windowAspect / 1.76f;

            if (scaleHeight < 1.0f)
            {
                mainCamera.orthographicSize = mainCamera.orthographicSize / scaleHeight;
            }

            foreach (var entityId in Components.Players.GetEntitiesIds())
            {
                var component = Components.Players[entityId];
                component.ActiveAbility = Abilities.Rocket;
                Components.Players[entityId] = component;
            }
        }

        public override void Update()
        {
            ProcessFlip();
            ProcessSetActiveAbilityMessages();
            ProcessPlayerShootMiniBazookaMessages();
            ProcessPlayerPlaceRepellerMessages();
            ProcessPlayerPlaceRadiationCloudMessages();
        }

        private void ProcessFlip()
        {
            foreach (var entityId in Components.Players.GetEntitiesIds())
            {
                var mousePositionInWorldPoints = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                var component = Components.Players[entityId];
                var transform = Components.Positioning[entityId].Transform;

                //Debug.Log(mousePositionInWorldPoints.x);
                //Debug.Log(transform.position.x);

                if (component.IsFacingRight && mousePositionInWorldPoints.x < transform.position.x)
                {
                    //Debug.Log("component.IsFacingRight = false");
                    component.IsFacingRight = false;
                    transform.localScale = new Vector3(
                        component.ScaleX,
                        transform.localScale.y,
                        transform.localScale.z);
                }

                if (!component.IsFacingRight && mousePositionInWorldPoints.x > transform.position.x)
                {
                    //Debug.Log("component.IsFacingRight = true");
                    component.IsFacingRight = true;
                    transform.localScale = new Vector3(
                        -component.ScaleX,
                        transform.localScale.y,
                        transform.localScale.z);
                }

                Components.Players[entityId] = component;
            }
        }

        private void ProcessPlayerShootMiniBazookaMessages()
        {
            var messages = MessageBus.PlayerPlaceRadiationCloudMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var component = Components.Players[entityId];
            }

            MessageBus.PlayerPlaceRadiationCloudMessages.Clear();
        }


        private void ProcessPlayerPlaceRepellerMessages()
        {
            var messages = MessageBus.PlayerPlaceRepellerMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var component = Components.Players[entityId];
            }

            MessageBus.PlayerPlaceRepellerMessages.Clear();
        }

        private void ProcessPlayerPlaceRadiationCloudMessages()
        {
            var messages = MessageBus.PlayerPlaceRadiationCloudMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var component = Components.Players[entityId];
            }

            MessageBus.PlayerPlaceRadiationCloudMessages.Clear();
        }

        private void ProcessSetActiveAbilityMessages()
        {
            var messages = MessageBus.SetActiveAbilityMessages.GetAll();

            var abilityManagerIds = Components.AbilityManagers.GetEntitiesIds();
            var playerIds = Components.Players.GetEntitiesIds();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                foreach (var abilityManagerId in abilityManagerIds)
                {
                    var abilityManager = Components.AbilityManagers[abilityManagerId];

                    if (abilityManager.ManegementAbilityDatabaseId == (int)message.AbilityToSet && abilityManager.IsAbleToCast)
                    {
                        foreach (var entityId in playerIds)
                        {
                            var component = Components.Players[entityId];

                            var abilityChangedMessage = new ActiveAbilityChangedMessage
                            {
                                OldActiveAbility = component.ActiveAbility,
                                NewActiveAbility = message.AbilityToSet
                            };

                            component.ActiveAbility = message.AbilityToSet;
                            Components.Players[entityId] = component;

                            MessageBus.ActiveAbilityChangedMessages.Publish(abilityChangedMessage);
                        }
                    }
                    else
                    {
                        // todo: show message "Ability reloading" or do nothing
                    }
                }
            }

            MessageBus.SetActiveAbilityMessages.Clear();
        }
    }
}