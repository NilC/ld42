﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct UpdatePointMessage
    {
        public int EntityId;
        public Vector3 Point;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static DictionaryMessagesChannel<UpdatePointMessage> UpdatePointMessages = new DictionaryMessagesChannel<UpdatePointMessage>();
    }
}