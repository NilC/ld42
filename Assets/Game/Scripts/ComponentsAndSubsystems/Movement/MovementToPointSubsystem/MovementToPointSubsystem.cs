﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class MovementToPointSubsystem : Subsystem        
    {
        public override void Update()
        {
            ProcessUpdatePointMessages();
            ProcessMovement();
        }

        private void ProcessUpdatePointMessages()
        {
            var messagesPairs = MessageBus.UpdatePointMessages.GetAll();

            foreach (var messagePair in messagesPairs)
            {
                var message = messagePair.Value;
                var entityId = message.EntityId;

                if (MessageBus.PointReachedMessages.Contains(entityId))
                {
                    continue;
                }

                var component = Components.MovementToPoint[entityId];
                component.Point = message.Point;
                component.IsPointReached = false;
                Components.MovementToPoint[entityId] = component;
            }

            MessageBus.UpdatePointMessages.Clear();
        }

        private void ProcessMovement()
        {
            foreach (var entityId in Components.MovementToPoint.GetEntitiesIds())
            {
                var component = Components.MovementToPoint[entityId];
                if (!component.IsEnabled || component.IsPointReached)
                {
                    continue;
                }

                var currentPositionPoint = Components.Positioning[entityId].Transform.position;

                var directionVector = component.Point - currentPositionPoint;
                directionVector.Normalize();

                var nextPosition = currentPositionPoint + directionVector * GetDeltaSpeed(component);

                //учитывается "перелет" позиции из-за больших скоростей, или большого RealtimeDeltaTime
                if ((currentPositionPoint.x <= component.Point.x && component.Point.x <= nextPosition.x ||
                     nextPosition.x <= component.Point.x && component.Point.x <= currentPositionPoint.x) &&

                    (currentPositionPoint.y <= component.Point.y && component.Point.y <= nextPosition.y ||
                     nextPosition.y <= component.Point.y && component.Point.y <= currentPositionPoint.y) &&

                    (currentPositionPoint.z <= component.Point.z && component.Point.z <= nextPosition.z ||
                     nextPosition.z <= component.Point.z && component.Point.z <= currentPositionPoint.z))
                {
                    nextPosition = component.Point;
                }

                var deltaPosition = nextPosition - currentPositionPoint;

                Components.Positioning[entityId].Transform.position += deltaPosition;

                var offset = component.Point - Components.Positioning[entityId].Transform.position;
                var isPointReached = offset.sqrMagnitude < component.SqrAccuracy;

                if (isPointReached)
                {
                    component.IsPointReached = true;
                    Components.MovementToPoint[entityId] = component;

                    MessageBus.PointReachedMessages.Publish(entityId, new PointReachedMessage
                    {
                        EntityId = entityId,
                        Point = component.Point
                    });
                }
            }
        }

        private float GetDeltaSpeed(MovementToPointComponent component)
        {
            return component.Speed * component.SpeedModificator * Time.deltaTime;
        }
    }
}
