﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class LinearMovementComponentDataProvider : CommonComponentDataProvider<LinearMovementComponent>
    {
#if UNITY_EDITOR
        public override void ApplyDefaultValues()
        {
            Component = new LinearMovementComponent
            {
                IsEnabled = true,
                InitialSpeedModificator = 1.0f
            };
        }

        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "Speed", "SpeedModificator" };
        }

        public override GizmoPropertiesProvider[] GetGizmoPropertiesProviders()
        {
            return new GizmoPropertiesProvider[] { new MovementToPointPropertiesProvider() };
        }
#endif
    }
}
