﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class LinearMovementComponentsContainer : ComponentsContainer<LinearMovementComponent, LinearMovementComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, LinearMovementComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Speed = component.InitialSpeed;
            component.SpeedModificator = component.InitialSpeedModificator;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];

            component.IsEnabled = true;
            component.Speed = component.InitialSpeed;
            component.SpeedModificator = component.InitialSpeedModificator;

            Components[entityId] = component;
        }
    }
}

