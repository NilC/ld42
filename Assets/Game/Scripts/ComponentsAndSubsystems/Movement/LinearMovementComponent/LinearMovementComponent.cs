﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct LinearMovementComponent
    {
        public bool IsEnabled;
        public float Speed;
        public float InitialSpeed;
        public float SpeedModificator;
        public float InitialSpeedModificator;
        public Vector3 DirectionVector;
    }
}