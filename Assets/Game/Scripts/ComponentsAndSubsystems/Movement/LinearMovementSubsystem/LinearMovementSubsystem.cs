﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class LinearMovementSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessUpdateDirectionMessages();
            ProcessMovement();
        }

        private void ProcessUpdateDirectionMessages()
        {
            var messages = MessageBus.UpdateDirectionMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var component = Components.LinearMovement[entityId];
                component.DirectionVector = message.DirectionVector;
                component.DirectionVector.Normalize();
                Components.LinearMovement[entityId] = component;
            }

            MessageBus.UpdateDirectionMessages.Clear();
        }

        private void ProcessMovement()
        {
            foreach (var entityId in Components.LinearMovement.GetEntitiesIds())
            {
                var component = Components.LinearMovement[entityId];

                if (component.IsEnabled)
                {
                    Components.Positioning[entityId].Transform.position += component.DirectionVector * GetDeltaSpeed(component);
                }
            }
        }

        private float GetDeltaSpeed(LinearMovementComponent component)
        {
            return component.Speed * component.SpeedModificator * Time.deltaTime;
        }
    }
}
