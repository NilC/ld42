﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct UpdateDirectionMessage
    {
        public int EntityId;
        public Vector3 DirectionVector;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<UpdateDirectionMessage> UpdateDirectionMessages = new ListMessagesChannel<UpdateDirectionMessage>();
    }
}