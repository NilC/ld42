﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class MovementToPointComponentDataProvider : CommonComponentDataProvider<MovementToPointComponent>
    {
#if UNITY_EDITOR
        public override void ApplyDefaultValues()
        {
            Component = new MovementToPointComponent
            {
                IsEnabled = true,
                InitialSpeedModificator = 1.0f,
                Accuracy = 0.0001f
            };
        }

        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Speed", "SpeedModificator", "SqrAccuracy" };
        }

        public override GizmoPropertiesProvider[] GetGizmoPropertiesProviders()
        {
            return new GizmoPropertiesProvider[] { new MovementToPointPropertiesProvider() };
        }
#endif
    }
}
