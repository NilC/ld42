﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct MovementToPointComponent
    {
        public bool IsEnabled;
        public float InitialSpeed;
        public float Speed;
        public float InitialSpeedModificator;
        public float SpeedModificator;
        public Vector3 Point;
        public bool IsPointReached;
        public float Accuracy;
        public float SqrAccuracy;
    }
}
