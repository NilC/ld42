﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class MovementToPointComponentsContainer : ComponentsContainer<MovementToPointComponent, MovementToPointComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, MovementToPointComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Speed = component.InitialSpeed;
            component.SpeedModificator = component.InitialSpeedModificator;
            component.SqrAccuracy = component.Accuracy * component.Accuracy;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];

            component.IsEnabled = true;
            component.Speed = component.InitialSpeed;
            component.SpeedModificator = component.InitialSpeedModificator;
            component.Point = new Vector3();
            component.IsPointReached = false;
            component.SqrAccuracy = component.Accuracy * component.Accuracy;

            Components[entityId] = component;
        }
    }
}

