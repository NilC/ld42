namespace Brutime.LD42
{
    using BruCore;
    using System.Collections.Generic;

    public class MovementToPointPropertiesProvider : GizmoPropertiesProvider
    {
        public override List<GizmoProperty> GetProperties(int entityId)
        {
            var properties = new List<GizmoProperty>();

            var pointText = string.Empty;

            MovementToPointComponent component;
            if (Components.MovementToPoint.TryGet(entityId, out component))
            {               
                pointText = component.Point.ToString("G4");
            }

            properties.Add(new GizmoProperty("Point", pointText));

            return properties;
        }
    }
}
