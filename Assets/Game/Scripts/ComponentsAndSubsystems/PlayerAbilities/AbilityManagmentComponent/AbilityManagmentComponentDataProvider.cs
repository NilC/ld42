namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class AbilityManagmentComponentDataProvider : CommonComponentDataProvider<AbilityManagmentComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "TimeInCooldown" };
        }
#endif
    }
}
