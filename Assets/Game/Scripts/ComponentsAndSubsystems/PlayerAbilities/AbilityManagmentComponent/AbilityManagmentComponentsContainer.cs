namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public class AbilityManagmentComponentsContainer : ComponentsContainer<AbilityManagmentComponent, AbilityManagmentComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, AbilityManagmentComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
