namespace Brutime.LD42
{
    using System;

	[Serializable]
    public struct AbilityManagmentComponent
	{
	    public int ManegementAbilityDatabaseId;

	    public float CooldownTime;
	    public float TimeInCooldown;
	    public bool IsAbleToCast;
	}
}