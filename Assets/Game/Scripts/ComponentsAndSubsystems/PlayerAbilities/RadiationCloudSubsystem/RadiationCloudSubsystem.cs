﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class RadiationCloudSubsystem : Subsystem
    {
        

        public override void Update()
        {
            ProcessCreateMessages();

            ProcessClouds();
        }

        private void ProcessClouds()
        {
            var guys = Components.Guys.GetEntitiesIds();

            foreach (var cloudId in Components.RadiationClounds.GetEntitiesIds())
            {
                var cloud = Components.RadiationClounds[cloudId];
                InitEntity(cloudId);
                //var position = SetPosition(cloudId, cloud);
                var position = Components.Positioning[cloudId].Transform.position;
                if (cloud.IsFloating)
                    ProcessFloating(cloud, cloudId);
                 
                foreach (var dude in guys)
                {
                    if (Vector3.Distance(Components.Positioning[dude].Transform.position, position) <= cloud.AreaRadius)
                        Attack(dude, cloud.AreaDamage);
                }
            }
        }

        public void InitEntity(int entityId)
        {
            var c = Components.LinearMovement[entityId];
            c.IsEnabled = Components.RadiationClounds[entityId].IsFloating;
            Components.LinearMovement[entityId] = c;
        }

        private void Attack(int dudeId, float targetDamage)
        {
            MessageBus.AttackMessages.Publish(new AttackMessage
            {
                //ApplyId = dudeId,
                CriticalChance = 0,
                CriticalModifier = 0,
                DamageType = DamageType.Pure,
                DamageValueFrom = (int)targetDamage,
                DamageValueTo = (int)targetDamage,
                TargetEntityId = dudeId
            });
        }

        private void ProcessFloating(RadiationCloudComponent cloud, int cloudId)
        {
            if (RollProbability(cloud.ChangeDirectionTendencyProbability))
                cloud.Tendency = (DirectionTendency)Random.Range(0, 3);

            if (cloud.Tendency == DirectionTendency.None)
                return;

            var dir = Components.LinearMovement[cloudId].DirectionVector;
            var step = cloud.Tendency == DirectionTendency.Increasing ? cloud.RotationAngle : -cloud.RotationAngle;

            dir = Quaternion.Euler(0, 0, step) * dir;
            MessageBus.UpdateDirectionMessages.Publish(new UpdateDirectionMessage { DirectionVector = dir, EntityId = cloudId });
        }


        private static bool RollProbability(float prob)
        {
            return Random.value < prob;
        }

        //private Vector3 SetPosition(int cloudId, RadiationCloudComponent cloud)
        //{
        //    PositioningComponent positioningComponent;
        //    if (Components.Positioning.TryGet(cloud.TargetId, out positioningComponent))
        //        Components.Positioning[cloudId] = positioningComponent;
        //    else
        //        positioningComponent = Components.Positioning[cloudId];

        //    return positioningComponent.Transform.position;
        //}

        private void ProcessCreateMessages()
        {
            var messages = MessageBus.CreateRadiationCloudMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var id = Factories.Entities.CreateEntity(messages[i].EntityId, messages[i].Position);

                MessageBus.PlayRadiationCloudMessages.Publish(new PlayRadiationCloudMessage
                {
                    Position = messages[i].Position
                });

                RandomizeInitialDirection(id);
            }

            MessageBus.CreateRadiationCloudMessages.Clear();
        }

        private void RandomizeInitialDirection(int id)
        {
            var angle = Random.Range(-180f, 180f);

            var movement = Components.LinearMovement[id];
            movement.DirectionVector = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);

            Components.LinearMovement[id] = movement;

        }
    }
}
