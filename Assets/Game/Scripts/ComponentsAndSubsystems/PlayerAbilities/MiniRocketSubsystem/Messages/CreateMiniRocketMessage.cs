namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct CreateMiniRocketMessage
    {
        public int EntityId;
        public Vector3 Position;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreateMiniRocketMessage> CreateMiniRocketMessages = new ListMessagesChannel<CreateMiniRocketMessage>();
    }    
}