namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class RadiationCloudComponentDataProvider : CommonComponentDataProvider<RadiationCloudComponent>
    {
#if UNITY_EDITOR
        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new[] { new CloudRadiusGizmosDrawer()};
        }

#endif
    }
}
