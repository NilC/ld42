﻿using Brutime.BruCore;
using UnityEngine;

namespace Brutime.LD42
{
    public class CloudRadiusGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            if (!Components.Positioning.Contains(entityId) || !Components.RadiationClounds.Contains(entityId))
                return;

            var position = Components.Positioning[entityId].Transform.position;
            var radius = Components.RadiationClounds[entityId].AreaRadius;

            
            DrawEllipse(position, radius, radius, Color.red);
        }
    }


}