namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public class RadiationCloudComponentsContainer : ComponentsContainer<RadiationCloudComponent, RadiationCloudComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, RadiationCloudComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }

        public override void InitializeComponent(int entityId)
        {
            base.InitializeComponent(entityId);

            if (this.Components[entityId].IsFloating)
            {
                Subsystems.RadiationClouds.InitEntity(entityId);
            }
        }
    }
}
