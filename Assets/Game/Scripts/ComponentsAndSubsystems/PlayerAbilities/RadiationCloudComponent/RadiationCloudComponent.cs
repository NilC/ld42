namespace Brutime.LD42
{
    using System;
    using UnityEngine;

	[Serializable]
    public struct RadiationCloudComponent
    {
        public float AreaRadius;
        //public int TargetId;
        public float TargetDamage;
        public float AreaDamage;
        public bool IsFloating;
        public float ChangeDirectionTendencyProbability;
        public DirectionTendency Tendency;
        public float RotationAngle;
    }

    public enum DirectionTendency
    {
        None = 0,
        Increasing = 1,
        Decreasing = 2
    }

}