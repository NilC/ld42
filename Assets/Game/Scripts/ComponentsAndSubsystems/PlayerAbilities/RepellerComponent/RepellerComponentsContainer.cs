namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public class RepellerComponentsContainer : ComponentsContainer<RepellerComponent, RepellerComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, RepellerComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
