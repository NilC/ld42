﻿namespace Brutime.LD42
{
    // Pay attention, underlying values of ability - id ability in the database
    public enum Abilities
    {
        MiniRocket = 2000,
        RadiationCloud = 3000,
        Repeller = 4000,
        Rocket = 5000
    }
}