namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct SetActiveAbilityMessage
    {
        public Abilities AbilityToSet;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<SetActiveAbilityMessage> SetActiveAbilityMessages = new ListMessagesChannel<SetActiveAbilityMessage>();
    }    
}