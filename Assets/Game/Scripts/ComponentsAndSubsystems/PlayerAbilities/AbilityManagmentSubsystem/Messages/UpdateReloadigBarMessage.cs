namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct UpdateReloadigBarMessage
    {
        public float Progress;
        public Abilities AbilityType;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<UpdateReloadigBarMessage> UpdateReloadigBarMessages = new ListMessagesChannel<UpdateReloadigBarMessage>();
    }    
}