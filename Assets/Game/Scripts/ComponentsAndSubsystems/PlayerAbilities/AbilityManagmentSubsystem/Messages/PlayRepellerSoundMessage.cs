namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct PlayRepellerSoundMessage
    {
        public Vector3 Position;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<PlayRepellerSoundMessage> PlayRepellerSoundMessages = new ListMessagesChannel<PlayRepellerSoundMessage>();
    }    
}