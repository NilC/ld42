namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct ActiveAbilityChangedMessage
    {
        public Abilities NewActiveAbility;
        public Abilities OldActiveAbility;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<ActiveAbilityChangedMessage> ActiveAbilityChangedMessages = new ListMessagesChannel<ActiveAbilityChangedMessage>();
    }    
}