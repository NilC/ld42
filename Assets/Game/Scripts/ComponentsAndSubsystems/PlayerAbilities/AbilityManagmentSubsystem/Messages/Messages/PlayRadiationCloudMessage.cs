namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct PlayRadiationCloudMessage
    {
        public Vector3 Position;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<PlayRadiationCloudMessage> PlayRadiationCloudMessages = new ListMessagesChannel<PlayRadiationCloudMessage>();
    }    
}