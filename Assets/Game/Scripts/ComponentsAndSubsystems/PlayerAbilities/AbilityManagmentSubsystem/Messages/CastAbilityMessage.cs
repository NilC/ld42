namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public struct CastAbilityMessage
    {
        public Vector3 TargetPosition;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CastAbilityMessage> CastAbilityMessages = new ListMessagesChannel<CastAbilityMessage>();
    }    
}