﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class AbilityManagmentSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessAbilityCooldown();
            ProcessCastAbilityMessages();
        }

        private void ProcessAbilityCooldown()
        {
            foreach (var entityId in Components.AbilityManagers.GetEntitiesIds())
            {
                var component = Components.AbilityManagers[entityId];

                if (component.IsAbleToCast == false)
                {
                    component.TimeInCooldown += Time.deltaTime;
                }
                if (component.TimeInCooldown >= component.CooldownTime)
                {
                    component.IsAbleToCast = true;
                    component.TimeInCooldown = 0;
                }

                var updateReloadigBarMessage = new UpdateReloadigBarMessage { AbilityType = (Abilities)component.ManegementAbilityDatabaseId };
                if (!component.IsAbleToCast)
                {
                    updateReloadigBarMessage.Progress = component.TimeInCooldown / component.CooldownTime;
                }
                else
                {
                    updateReloadigBarMessage.Progress = 1;
                }
                MessageBus.UpdateReloadigBarMessages.Publish(updateReloadigBarMessage);
                

                Components.AbilityManagers[entityId] = component;
            }
        }

        private void ProcessCastAbilityMessages()
        {
            var messages = MessageBus.CastAbilityMessages.GetAll();
            var playerIds = Components.Players.GetEntitiesIds();
            var abilityManagersIds = Components.AbilityManagers.GetEntitiesIds();

            for (int i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                foreach (var playerId in playerIds)
                {
                    var activeAbility = Components.Players[playerId].ActiveAbility;

                    foreach (var abilityManagerId in abilityManagersIds)
                    {
                        var abilityManager = Components.AbilityManagers[abilityManagerId];

                        if (abilityManager.ManegementAbilityDatabaseId == (int)activeAbility && abilityManager.IsAbleToCast)
                        {
                            switch (activeAbility)
                            {
                                case Abilities.Rocket:
                                    MessageBus.CreatePositionProjectileMessages.Publish(new CreatePositionProjectileMessage
                                    {
                                        ProjectileDatabaseId = SettingsProviders.Level.RocketProjectileDatabaseId,
                                        Position = Components.ProjectileSources[playerId].SourceTransform.position,
                                        OwnerEntityId = playerId,
                                        TargetPosition = message.TargetPosition
                                    });
                                    MessageBus.PlayRocketShootSoundMessages.Publish(new PlayRocketShootSoundMessage
                                    {
                                        Position = Components.ProjectileSources[playerId].SourceTransform.position
                                    });
                                    break;

                                case Abilities.RadiationCloud:
                                    MessageBus.CreateRadiationCloudMessages.Publish(new CreateRadiationCloudMessage
                                    {
                                        EntityId = (int)activeAbility,
                                        Position = message.TargetPosition
                                    });
                                    break;

                                case Abilities.Repeller:
                                    MessageBus.CreateRepellerMessages.Publish(new CreateRepellerMessage
                                    {
                                        EntityId = (int)activeAbility,
                                        Position = message.TargetPosition
                                    });
                                    break;
                            }

                            abilityManager.IsAbleToCast = false;
                            Components.AbilityManagers[abilityManagerId] = abilityManager;
                        }
                    }
                }
            }

            MessageBus.CastAbilityMessages.Clear();
        }
    }
}