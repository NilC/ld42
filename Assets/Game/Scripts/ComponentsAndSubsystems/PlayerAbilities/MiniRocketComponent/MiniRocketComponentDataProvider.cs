namespace Brutime.LD42
{
    using BruCore;

    public class MiniRocketComponentDataProvider : CommonComponentDataProvider<MiniRocketComponent>
    {
#if UNITY_EDITOR
        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[]
            {
                new MiniRocketDamageAreaGizmoDrawer()
            };
        }
#endif
    }
}
