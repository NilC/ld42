namespace Brutime.LD42
{
    using System;

	[Serializable]
    public struct MiniRocketComponent
    {
        public float Radius;
        public int Damage;
    }
}