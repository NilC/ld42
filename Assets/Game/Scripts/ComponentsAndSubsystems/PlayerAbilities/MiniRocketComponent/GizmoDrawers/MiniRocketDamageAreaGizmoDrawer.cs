﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class MiniRocketDamageAreaGizmoDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            if (!Components.Positioning.Contains(entityId) ||
                !Components.MiniRockets.Contains(entityId))
            {
                return;
            }

            var position = Components.Positioning[entityId].Transform.position;
            var damageRadius = Components.MiniRockets[entityId].Radius;

            var color = Color.red;
            DrawEllipse(position, damageRadius, damageRadius, color);
        }
    }
}