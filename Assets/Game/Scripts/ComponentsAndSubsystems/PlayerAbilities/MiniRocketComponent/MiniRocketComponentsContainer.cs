namespace Brutime.LD42
{
    using BruCore;

    public class MiniRocketComponentsContainer : ComponentsContainer<MiniRocketComponent, MiniRocketComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, MiniRocketComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
