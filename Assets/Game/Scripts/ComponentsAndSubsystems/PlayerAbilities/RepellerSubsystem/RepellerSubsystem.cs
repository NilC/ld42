﻿namespace Brutime.LD42
{
    using System.Collections.Generic;
    using BruCore;
    using UnityEngine;

    public class RepellerSubsystem : Subsystem
    {
        private HashSet<int> repellers;

        public override void Update()
        {
            ProcessCreateMessages();
            ProcessRepellers();
        }

        private void ProcessCreateMessages()
        {
            var messages = MessageBus.CreateRepellerMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                Factories.Entities.CreateEntity(messages[i].EntityId, messages[i].Position);
                MessageBus.PlayRepellerSoundMessages.Publish(new PlayRepellerSoundMessage
                {
                    Position = messages[i].Position
                });
            }
            MessageBus.CreateRepellerMessages.Clear();
        }

        private void ProcessRepellers()
        {
            repellers = Components.Repellers.GetEntitiesIds();

            foreach (var id in Components.Guys.GetEntitiesIds())
            {
                if (IsCloseEnough(id))
                {
                    var c = Components.LinearMovement[id];
                    c.DirectionVector.Scale(new Vector3(-1, -1, 1));
                    Components.LinearMovement[id] = c;
                }
            }
        }

        private bool IsCloseEnough(int dudeId)
        {
            foreach (var repellerId in repellers)
            {
                var repellingDistance = Components.Repellers[repellerId].RepellingDistance;

                var vec = GetPosition(repellerId) - GetPosition(dudeId);
                var dir = Components.LinearMovement[dudeId].DirectionVector;

                if (vec.sqrMagnitude <= repellingDistance * repellingDistance && Vector3.Dot(vec, dir) > 0) //if close enough and heading to repeller
                    return true;
            }

            return false;
        }

        private Vector3 GetPosition(int id)
        {
            return Components.Positioning[id].Transform.position;
        }
    }
}
