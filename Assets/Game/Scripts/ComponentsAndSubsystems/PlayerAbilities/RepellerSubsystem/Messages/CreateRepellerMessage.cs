﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct CreateRepellerMessage
    {
        public int EntityId;
        public Vector3 Position;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreateRepellerMessage> CreateRepellerMessages = new ListMessagesChannel<CreateRepellerMessage>();
    }
}