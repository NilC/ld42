﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class RenderOrderByYSubsystem : Subsystem
    {
        public const int RangePerUnit = 1000;

        public override void Start()
        {
            UpdateRenderOrder();
        }

        public override void Update()
        {
            UpdateRenderOrder();
        }

        private void UpdateRenderOrder()
        {
            var messages = MessageBus.PositionChangedMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                RenderOrderByYComponent component;
                var isExists = Components.RenderOrderByY.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.SpriteRenderer.sortingOrder = (int)(-message.NewPosition.y * RangePerUnit + component.Priority);
                    Components.RenderOrderByY[message.EntityId] = component;
                }
            }
        }
    }
}
