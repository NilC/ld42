﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class FlippingComponentsContainer : ComponentsContainer<FlippingComponent, FlippingComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, FlippingComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Transform = CoreComponents.Entities[entityId].GameObject.transform;
            component.ScaleX = component.Transform.localScale.x;
            AddComponent(entityId, component);
        }
    }
}

