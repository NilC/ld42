﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct FlippingComponent
    {
        public bool IsFacingRight;
        public Transform Transform;
        public float ScaleX;
    }
}