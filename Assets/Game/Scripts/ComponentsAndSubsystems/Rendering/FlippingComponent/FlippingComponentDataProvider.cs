﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class FlippingComponentDataProvider : CommonComponentDataProvider<FlippingComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Transform", "ScaleX" };
        }
#endif
    }
}
