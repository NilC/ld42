﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class AnimationComponentDataProvider : CommonComponentDataProvider<AnimationComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Animator" };
        }
#endif
    }
}
