﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class AnimationComponentsContainer : ComponentsContainer<AnimationComponent, AnimationComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, AnimationComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Animator = CoreComponents.Entities[entityId].GameObject.GetComponent<Animator>();
            AddComponent(entityId, component);
        }

        public override void PushComponentToPool(int entityId)
        {
            var component = Components[entityId];
            if (component.IsNeedToReset)
            {
                component.Animator.CrossFade("Reset", 0.0f);
                component.Animator.Update(0.0f);
            }
            base.PushComponentToPool(entityId);
        }
    }
}

