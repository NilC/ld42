﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct AnimationComponent
    {
        public bool IsNeedToReset;
        public Animator Animator;
    }
}
