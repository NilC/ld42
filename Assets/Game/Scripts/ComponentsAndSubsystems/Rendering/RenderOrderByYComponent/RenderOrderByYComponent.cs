﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct RenderOrderByYComponent
    {
        public int Priority;
        public SpriteRenderer SpriteRenderer;
    }
}