﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class RenderOrderByYComponentsContainer : ComponentsContainer<RenderOrderByYComponent, RenderOrderByYComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, RenderOrderByYComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.SpriteRenderer = CoreComponents.Entities[entityId].GameObject.GetComponentInChildren<SpriteRenderer>();
            AddComponent(entityId, component);
        }
    }
}

