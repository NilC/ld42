﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class RenderOrderByYComponentDataProvider : CommonComponentDataProvider<RenderOrderByYComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "SortingGroup" };
        }
#endif
    }
}
