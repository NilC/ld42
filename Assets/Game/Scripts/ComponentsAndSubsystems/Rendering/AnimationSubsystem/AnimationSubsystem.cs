﻿namespace Brutime.LD42
{
    using BruCore;

    public class AnimationSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessTriggerAnimationMessages();
            ProcessIntegerValueAnimationMessages();
            ProcessFloatValueAnimationMessages();
            ProcessBoolValueAnimationMessages();
            ProcessResetTriggerAnimationMessages();
        }

        private void ProcessTriggerAnimationMessages()
        {
            var messages = MessageBus.TriggerAnimationMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                AnimationComponent component;
                var isExists = Components.Animation.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.Animator.SetTrigger(message.AnimationId);                    
                }
            }

            MessageBus.TriggerAnimationMessages.Clear();
        }

        private void ProcessIntegerValueAnimationMessages()
        {
            var messages = MessageBus.IntegerValueAnimationMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                AnimationComponent component;
                var isExists = Components.Animation.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.Animator.SetInteger(message.AnimationId, message.Value);
                }
            }

            MessageBus.IntegerValueAnimationMessages.Clear();
        }

        private void ProcessFloatValueAnimationMessages()
        {
            var messages = MessageBus.FloatValueAnimationMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                AnimationComponent component;
                var isExists = Components.Animation.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.Animator.SetFloat(message.AnimationId, message.Value);
                }
            }

            MessageBus.FloatValueAnimationMessages.Clear();
        }

        private void ProcessBoolValueAnimationMessages()
        {
            var messages = MessageBus.BoolValueAnimationMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                AnimationComponent component;
                var isExists = Components.Animation.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.Animator.SetBool(message.AnimationId, message.Value);
                }
            }

            MessageBus.BoolValueAnimationMessages.Clear();
        }

        private void ProcessResetTriggerAnimationMessages()
        {
            var messages = MessageBus.ResetTriggerAnimationMessages.GetAll();

            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];

                AnimationComponent component;
                var isExists = Components.Animation.TryGet(message.EntityId, out component);
                if (isExists)
                {
                    component.Animator.ResetTrigger(message.AnimationId);
                }
            }

            MessageBus.ResetTriggerAnimationMessages.Clear();
        }
    }
}
