﻿namespace Brutime.LD42
{
    using BruCore;

    public struct BoolValueAnimationMessage
    {
        public int EntityId;
        public int AnimationId;
        public bool Value;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<BoolValueAnimationMessage> BoolValueAnimationMessages = new ListMessagesChannel<BoolValueAnimationMessage>();
    }
}
