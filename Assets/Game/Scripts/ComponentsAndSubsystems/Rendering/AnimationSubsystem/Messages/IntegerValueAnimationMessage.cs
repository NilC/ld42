﻿namespace Brutime.LD42
{
    using BruCore;

    public struct IntegerValueAnimationMessage
    {
        public int EntityId;
        public int AnimationId;
        public int Value;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<IntegerValueAnimationMessage> IntegerValueAnimationMessages = new ListMessagesChannel<IntegerValueAnimationMessage>();
    }
}
