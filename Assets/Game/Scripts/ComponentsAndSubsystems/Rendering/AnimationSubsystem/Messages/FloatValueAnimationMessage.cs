﻿namespace Brutime.LD42
{
    using BruCore;

    public struct FloatValueAnimationMessage
    {
        public int EntityId;
        public int AnimationId;
        public float Value;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<FloatValueAnimationMessage> FloatValueAnimationMessages = new ListMessagesChannel<FloatValueAnimationMessage>();
    }
}
