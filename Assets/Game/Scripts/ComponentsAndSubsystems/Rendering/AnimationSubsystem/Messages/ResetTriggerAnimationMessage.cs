﻿namespace Brutime.LD42
{
    using BruCore;

    public struct ResetTriggerAnimationMessage
    {
        public int EntityId;
        public int AnimationId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<ResetTriggerAnimationMessage> ResetTriggerAnimationMessages = new ListMessagesChannel<ResetTriggerAnimationMessage>();
    }
}
