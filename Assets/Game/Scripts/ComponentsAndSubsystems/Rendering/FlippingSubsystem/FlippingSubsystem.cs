namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class FlippingSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessPositionChangedMessages();
        }

        private void ProcessPositionChangedMessages()
        {
            var messages = MessageBus.PositionChangedMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;

                FlippingComponent component;
                var isExists = Components.Flipping.TryGet(entityId, out component);
                if (isExists)
                {
                    if (component.IsFacingRight && message.NewPosition.x < message.OldPosition.x)
                    {
                        component.IsFacingRight = false;
                        component.Transform.localScale = new Vector3(
                            component.ScaleX,
                            component.Transform.localScale.y,
                            component.Transform.localScale.z);

                    }

                    if (!component.IsFacingRight && message.NewPosition.x > message.OldPosition.x)
                    {
                        component.IsFacingRight = true;
                        component.Transform.localScale = new Vector3(
                            -component.ScaleX,
                            component.Transform.localScale.y,
                            component.Transform.localScale.z);

                    }

                    Components.Flipping[entityId] = component;
                }
            }
        }
    }
}
