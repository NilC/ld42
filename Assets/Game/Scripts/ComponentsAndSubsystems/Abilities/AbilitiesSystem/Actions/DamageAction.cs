﻿namespace Brutime.LD42
{
#if UNITY_EDITOR
    using UnityEditor;
#endif

    public class DamageAction : AbilityAction
    {
        public DamageType DamageType;
        public int DamageValueFrom;
        public int DamageValueTo;
        public float CriticalChance;
        public float CriticalModifier;

        public override void Apply(int entityId)
        {
            var damagaMessage = new AttackMessage
            {
                TargetEntityId = entityId,
                //ApplyId = AbilityApplyIdGenerator.GetId(),
                DamageType = DamageType,
                DamageValueFrom = DamageValueFrom,
                DamageValueTo = DamageValueTo,
                CriticalChance = CriticalChance,
                CriticalModifier = CriticalModifier
            };

            MessageBus.AttackMessages.Publish(damagaMessage);
        }

#if UNITY_EDITOR
        public override string GetCaption()
        {
            return "Damage Action";
        }

        public override void DoLayout()
        {
            DamageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", DamageType);
            DamageValueFrom = EditorGUILayout.IntField("Damage Value From", DamageValueFrom);
            DamageValueTo = EditorGUILayout.IntField("Damage Value To", DamageValueTo);
            CriticalChance = EditorGUILayout.FloatField("Critical Chance", CriticalChance);
            CriticalModifier = EditorGUILayout.FloatField("Critical Modifier", CriticalModifier);
        }
#endif
    }
}