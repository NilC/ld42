﻿namespace Brutime.LD42
{
    using UnityEngine;

    public abstract class AbilityAction : ScriptableObject
    {
        public abstract void Apply(int entityId);

#if UNITY_EDITOR
        public abstract string GetCaption();
        public abstract void DoLayout();
#endif
    }
}