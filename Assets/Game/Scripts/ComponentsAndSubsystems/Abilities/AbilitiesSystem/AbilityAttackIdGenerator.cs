namespace Brutime.LD42
{
    public static class AbilityApplyIdGenerator
    {
        public static int currentId = 0;

        public static int GetId()
        {
            currentId++;
            return currentId;
        }
    }
}