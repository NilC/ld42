﻿namespace Brutime.LD42
{
    using UnityEngine;

    public class Ability : ScriptableObject
    {
        public Sprite Icon;
        public float CooldownTime;
        public AbilityAction[] Actions = new AbilityAction[0];

        public void Apply(int targetEntityId)
        {
            for (var i = 0; i < Actions.Length; i++)
            {
                Actions[i].Apply(targetEntityId);
            }
        }
    }
}