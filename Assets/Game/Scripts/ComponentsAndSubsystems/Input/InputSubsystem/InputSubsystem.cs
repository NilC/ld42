namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class InputSubsystem : Subsystem
    {
        private Camera mainCamera;
        private bool isNeedToResetToRocket;

        public override void Start()
        {
            mainCamera = Camera.main;
            isNeedToResetToRocket = false;
        }

        public override void Update()
        {
            ProcessPlayerMovement();
            ProcessAbilities();
        }

        private static void ProcessPlayerMovement()
        {
            var speed = 7.0f;

            foreach (var entityId in Components.Players.GetEntitiesIds())
            {
                var transform = Components.Positioning[entityId].Transform;

                if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                {
                    transform.Translate(-speed * Time.deltaTime, 0.0f, 0.0f);
                }

                if (Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.D))
                {
                    transform.Translate(speed * Time.deltaTime, 0.0f, 0.0f);
                }

                if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                {
                    transform.Translate(0.0f, speed * Time.deltaTime, 0.0f);
                }

                if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                {
                    transform.Translate(0.0f, -speed * Time.deltaTime, 0.0f);
                }

                if (transform.position.x < SettingsProviders.Level.MinX)
                {
                    transform.position = new Vector3(SettingsProviders.Level.MinX, transform.position.y, transform.position.z);
                }

                if (transform.position.x > SettingsProviders.Level.MaxX)
                {
                    transform.position = new Vector3(SettingsProviders.Level.MaxX, transform.position.y, transform.position.z);
                }

                if (transform.position.y < SettingsProviders.Level.MinY)
                {
                    transform.position = new Vector3(transform.position.x, SettingsProviders.Level.MinY, transform.position.z);
                }

                if (transform.position.y > SettingsProviders.Level.MaxY)
                {
                    transform.position = new Vector3(transform.position.x, SettingsProviders.Level.MaxY, transform.position.z);
                }
            }
        }

        private void ProcessAbilities()
        {
            foreach (var entityId in Components.Players.GetEntitiesIds())
            {
                if (isNeedToResetToRocket)
                {
                    var component = Components.Players[entityId];
                    component.ActiveAbility = Abilities.Rocket;
                    Components.Players[entityId] = component;
                    isNeedToResetToRocket = false;
                }

                if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
                {
                    MessageBus.SetActiveAbilityMessages.Publish(new SetActiveAbilityMessage
                    {
                        AbilityToSet = Abilities.Rocket
                    });
                }

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    var transform = Components.Positioning[entityId].Transform;

                    MessageBus.SetActiveAbilityMessages.Publish(new SetActiveAbilityMessage
                    {
                        AbilityToSet = Abilities.RadiationCloud
                    });

                    MessageBus.CastAbilityMessages.Publish(new CastAbilityMessage
                    {
                        TargetPosition = transform.position
                    });

                    isNeedToResetToRocket = true;
                }

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    var mousePositionInWorldPoints = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                    MessageBus.CastAbilityMessages.Publish(new CastAbilityMessage
                    {
                        TargetPosition = new Vector3(mousePositionInWorldPoints.x, mousePositionInWorldPoints.y, 0)
                    });
                }
            }
        }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        public static bool IsTapped()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began && !IsPointerOverUI();
        }

        public static bool IsStartedMoving()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began;
        }

        public static bool IsMoving()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved;
        }

        public Vector3 GetInputPosition()
        {
            Vector3 mousePos = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, -mainCamera.transform.position.z);
            return mainCamera.ScreenToWorldPoint(mousePos);
        }

        public static Vector3 GetInputScreenPosition()
        {
            return Input.GetTouch(0).position;
        }
#else
        public static bool IsStartedMoving()
        {
            return Input.GetMouseButtonDown(0);
        }

        public static bool IsMoving()
        {
            return Input.GetMouseButton(0);
        }

        public static bool IsTapped()
        {
            return Input.GetMouseButtonUp(0) && !IsPointerOverUI();
        }

        public Vector3 GetInputPosition()
        {
            var mousePos = Input.mousePosition;
            mousePos.z = -mainCamera.transform.position.z; // select distance = 10 units from the camera
            return mainCamera.ScreenToWorldPoint(mousePos);
            //return Dependences.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        public static Vector3 GetInputScreenPosition()
        {
            return Input.mousePosition;
        }
#endif

        private static bool IsPointerOverUI()
        {
            if (!EventSystem.current)
            {
                return false;
            }

            return EventSystem.current.currentSelectedGameObject;
            //return EventSystem.current.IsPointerOverGameObject();
        }
    }
}