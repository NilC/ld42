﻿namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct PositioningComponent
    {
        public Transform Transform;
        public Vector3 PreviousPosition;
    }
}