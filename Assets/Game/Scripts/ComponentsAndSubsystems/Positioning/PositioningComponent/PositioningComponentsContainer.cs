﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class PositioningComponentsContainer : ComponentsContainer<PositioningComponent, PositioningComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, PositioningComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.Transform = CoreComponents.Entities[entityId].GameObject.transform;
            component.PreviousPosition = component.Transform.position;
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.PreviousPosition = component.Transform.position;
            Components[entityId] = component;
        }

        public override void PopComponentFromPool(int currentEntityId, int newEntityId)
        {
            base.PopComponentFromPool(currentEntityId, newEntityId);

            var component = Components[newEntityId];
            var positionChangedMessage = new PositionChangedMessage
            {
                EntityId = newEntityId,
                NewPosition = component.Transform.position
            };
            MessageBus.PositionChangedMessages.Publish(positionChangedMessage);
        }
    }
}

