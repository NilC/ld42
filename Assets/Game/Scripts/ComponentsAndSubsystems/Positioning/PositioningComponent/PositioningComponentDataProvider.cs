﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class PositioningComponentDataProvider : CommonComponentDataProvider<PositioningComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new [] { "Transform", "PreviousPosition" };
        }

        public override bool IsRequired()
        {
            return true;
        }
#endif
    }
}
