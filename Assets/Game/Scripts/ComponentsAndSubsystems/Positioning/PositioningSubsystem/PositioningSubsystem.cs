﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class PositioningSubsystem : Subsystem
    {
        public override void Update()
        {
            MessageBus.PositionChangedMessages.Clear();

            foreach (var entityId in Components.Positioning.GetEntitiesIds())
            {
                var component = Components.Positioning[entityId];
                if (component.Transform != null && component.Transform.hasChanged)
                {
                    var positionChangedMessage = new PositionChangedMessage
                    {
                        EntityId = entityId,
                        OldPosition = component.PreviousPosition,
                        NewPosition = component.Transform.position
                    };
                    MessageBus.PositionChangedMessages.Publish(positionChangedMessage);

                    component.Transform.hasChanged = false;
                    component.PreviousPosition = component.Transform.position;

                    Components.Positioning[entityId] = component;
                }
            }
        }
    }
}