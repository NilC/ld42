﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct PositionChangedMessage
    {
        public int EntityId;
        public Vector3 OldPosition;
        public Vector3 NewPosition;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<PositionChangedMessage> PositionChangedMessages = new ListMessagesChannel<PositionChangedMessage>();
    }
}