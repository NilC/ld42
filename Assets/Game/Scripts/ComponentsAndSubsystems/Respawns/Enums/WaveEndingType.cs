﻿namespace Brutime.LD42
{
    public enum WaveEndingType
    {
        AllDeadThenEndingPauseTime,
        OnlyEndingPauseTime
    }
}