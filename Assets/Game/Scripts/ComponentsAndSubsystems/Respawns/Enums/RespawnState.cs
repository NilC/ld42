﻿namespace Brutime.LD42
{
    public enum RespawnState
    {
        None,
        InProgress,
        WaitingForNextWave,
        NoMoreWaves
    }
}