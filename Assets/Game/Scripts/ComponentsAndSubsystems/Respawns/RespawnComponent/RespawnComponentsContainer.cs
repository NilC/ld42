﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class RespawnComponentsContainer : ComponentsContainer<RespawnComponent, RespawnComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, RespawnComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}

