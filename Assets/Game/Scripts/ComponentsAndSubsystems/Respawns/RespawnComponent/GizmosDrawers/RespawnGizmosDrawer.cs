namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class RespawnGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            var transform = CoreComponents.Entities[entityId].GameObject.transform;

            Gizmos.color = new Color(0.0f, 0.6f, 0.2f, 0.25f);
            Gizmos.DrawCube(transform.position, transform.localScale);
        }
    }
}
