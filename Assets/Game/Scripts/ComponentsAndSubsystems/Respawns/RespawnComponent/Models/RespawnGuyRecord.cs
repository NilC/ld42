namespace Brutime.LD42
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct RespawnGuyRecord
    {
        public int EntityDatabaseId;
        [Range(1, 100)]
        public int Chance;

        //public RespawnEnemyItem(long id, float chance)
        //{
        //    Id = id;
        //    Chance = chance;
        //}
    }
}