﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct RespawnData
    {
        public int EnemiesCount;
        public RespawnGuyRecord[] RespawnEnemyRecords;
    }
}