﻿namespace Brutime.LD42
{
    using System;
    using Brutime.BruCore;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Collections.Generic;
    using UnityEditor;
    using System.Linq;
#endif

    public class RespawnComponentDataProvider : ComponentDataProvider
    {
        public override Type GetComponentType()
        {
            return typeof(RespawnComponent);
        }

        public RespawnComponent GetComponent()
        {
            return new RespawnComponent
            {
                RespawnData = respawnData
            };
        }

        [SerializeField]
        private RespawnData[] respawnData;

#if UNITY_EDITOR
        private EntitiesDatabase entitiesDatabase;
        private string[] guysNames;
        private int[] guysIds;
        private Dictionary<int, Texture2D> guysIcons;

        public override string GetCaption()
        {
            return "Respawn Component";
        }

        public override bool IsUsingSerializedObject()
        {
            return true;
        }

        public override void InitializeLayout(SerializedObject serializedObject)
        {
            base.InitializeLayout();

            var respawnDataProperty = serializedObject.FindProperty("respawnData");

            entitiesDatabase = Resources
                .FindObjectsOfTypeAll<EntitiesDatabase>()
                .First();

            var allEnemies = entitiesDatabase
                .EntityRecordGroups
                .Where(x => x.Name.Contains("Guys"))
                .SelectMany(x => x.EntitiesRecords)
                .ToArray();

            guysNames = new string[allEnemies.Length];
            guysIds = new int[allEnemies.Length];
            guysIcons = new Dictionary<int, Texture2D>(allEnemies.Length);
            for (var i = 0; i < allEnemies.Length; i++)
            {
                var guyItem = allEnemies[i];
                guysNames[i] = string.Format("{0} {1}", guyItem.EntityDatabaseId, guyItem.GameObject.name);
                guysIds[i] = guyItem.EntityDatabaseId;
                guysIcons[guyItem.EntityDatabaseId] = AssetPreview.GetAssetPreview(allEnemies[i].GameObject);
            }
        }

        public override void DrawLayout(SerializedObject serializedObject)
        {
            DrawRespawnData(serializedObject);
            FixPercents(serializedObject);
        }

        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[] { new RespawnGizmosDrawer() };
        }

        private void DrawRespawnData(SerializedObject serializedObject)
        {
            EditorGUILayout.Space();

            var respawnDataProperty = serializedObject.FindProperty("respawnData");

            if (respawnDataProperty.arraySize == 0)
            {
                respawnDataProperty.InsertArrayElementAtIndex(respawnDataProperty.arraySize);
                var lastWaveProperty = respawnDataProperty.GetArrayElementAtIndex(respawnDataProperty.arraySize - 1);
                lastWaveProperty.FindPropertyRelative("EnemiesCount").intValue = 0;
                lastWaveProperty.FindPropertyRelative("RespawnEnemyRecords").ClearArray();
            }

            EditorGUI.indentLevel++;

            for (var i = 0; i < respawnDataProperty.arraySize; i++)
            {
                EditorGUI.indentLevel++;

                var enemiesCountProperty = respawnDataProperty.GetArrayElementAtIndex(i).FindPropertyRelative("EnemiesCount");
                EditorGUILayout.PropertyField(enemiesCountProperty);

                EditorGUILayout.Space();

                RespawnEnemyItemsLayout(respawnDataProperty.GetArrayElementAtIndex(i));

                EditorGUILayout.Space();

                EditorGUI.indentLevel--;
            }

            EditorGUI.indentLevel--;
        }

        private void RespawnEnemyItemsLayout(SerializedProperty waveProperty)
        {
            var respawnEnemyItemsProperty = waveProperty.FindPropertyRelative("RespawnEnemyRecords");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Enemies", EditorStyles.boldLabel, GUILayout.MinWidth(50));
            if (GUILayout.Button("+", GUILayout.Width(88)))
            {
                respawnEnemyItemsProperty.InsertArrayElementAtIndex(respawnEnemyItemsProperty.arraySize);
                var lastRespawnEnemyItemProperty = respawnEnemyItemsProperty.GetArrayElementAtIndex(respawnEnemyItemsProperty.arraySize - 1);
                lastRespawnEnemyItemProperty.FindPropertyRelative("EntityDatabaseId").longValue = guysIds[0];
                lastRespawnEnemyItemProperty.FindPropertyRelative("Chance").intValue = 1;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel++;
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Record Id", GUILayout.MinWidth(80), GUILayout.MaxWidth(120));
                    GUILayout.Label("Icon", GUILayout.MaxWidth(48));
                    GUILayout.Space(-30);
                    EditorGUILayout.LabelField("Chance, %");
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();

                for (var i = 0; i < respawnEnemyItemsProperty.arraySize; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        var idProperty = respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("EntityDatabaseId");
                        var selectedIndex = ArrayUtility.IndexOf(guysIds, idProperty.intValue);
                        selectedIndex = EditorGUILayout.Popup(selectedIndex, guysNames, GUILayout.MinWidth(80), GUILayout.MaxWidth(120));
                        idProperty.intValue = selectedIndex != -1 ? guysIds[selectedIndex] : 0;

                        GUILayout.Label(new GUIContent { image = guysIcons[idProperty.intValue] }, GUILayout.MaxWidth(48), GUILayout.MinHeight(48), GUILayout.MaxHeight(48));
                        GUILayout.Space(-30);

                        var chanceProperty = respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance");
                        EditorGUILayout.PropertyField(chanceProperty, GUIContent.none);

                        if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                        {
                            respawnEnemyItemsProperty.MoveArrayElement(i, i - 1);
                        }
                        if (GUILayout.Button("↓", GUILayout.Width(30)) && i < respawnEnemyItemsProperty.arraySize - 1)
                        {
                            respawnEnemyItemsProperty.MoveArrayElement(i, i + 1);
                        }
                        if (GUILayout.Button("X", GUILayout.Width(20)))
                        {
                            respawnEnemyItemsProperty.DeleteArrayElementAtIndex(i);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUI.indentLevel--;
        }

        private void FixPercents(SerializedObject serializedObject)
        {
            var respawnDataProperty = serializedObject.FindProperty("respawnData");

            for (var respawnDataIndex = 0; respawnDataIndex < respawnDataProperty.arraySize; respawnDataIndex++)
            {
                var respawnEnemyItemsProperty = respawnDataProperty.GetArrayElementAtIndex(respawnDataIndex).FindPropertyRelative("RespawnEnemyRecords");

                if (respawnEnemyItemsProperty.arraySize == 0)
                {
                    continue;
                }

                var sum = 0.0f;
                for (var i = 0; i < respawnEnemyItemsProperty.arraySize; i++)
                {
                    sum += respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue;
                }

                for (var i = 0; i < respawnEnemyItemsProperty.arraySize; i++)
                {
                    respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue =
                        Mathf.RoundToInt(respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue / sum * 100.0f);
                }

                var newSum = 0;

                for (var i = 0; i < respawnEnemyItemsProperty.arraySize; i++)
                {
                    newSum += respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue;
                }

                if (newSum == 100)
                {
                    continue;
                }

                var maxValue = 0;
                var maxIndex = 0;
                for (var i = 0; i < respawnEnemyItemsProperty.arraySize; i++)
                {
                    if (respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue > maxValue)
                    {
                        maxValue = respawnEnemyItemsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Chance").intValue;
                        maxIndex = i;
                    }
                }

                if (newSum != 100)
                {
                    respawnEnemyItemsProperty.GetArrayElementAtIndex(maxIndex).FindPropertyRelative("Chance").intValue += 100 - newSum;
                }
            }
        }
#endif
    }
}
