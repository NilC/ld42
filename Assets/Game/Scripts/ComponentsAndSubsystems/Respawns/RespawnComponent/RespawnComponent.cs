﻿namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct RespawnComponent
    {
        public RespawnData[] RespawnData;
    }
}