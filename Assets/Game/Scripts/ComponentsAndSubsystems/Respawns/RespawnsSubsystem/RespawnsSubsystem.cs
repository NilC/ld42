namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class RespawnsSubsystem : Subsystem
    {
        public int TotalEnemiesCount { get; private set; }

        private bool isAlreadySpawned;
        private bool isLevelCompletedMessageAlreadySent;

        public override void Start()
        {
            isAlreadySpawned = false;
        }

        public override void Update()
        {
            ProcessStartSpawningMessages();
        }

        private void ProcessStartSpawningMessages()
        {
            if (!MessageBus.StartSpawningMessages.IsEmpty())
            {
                foreach (var entityId in Components.Players.GetEntitiesIds())
                {
                    Components.Positioning[entityId].Transform.position = new Vector3(-7.4f, -3.77f, 0.0f);
                }
                SpawnGuys();
                MessageBus.StartSpawningMessages.Clear();
            }
        }

        private void SpawnGuys()
        {
            if (isAlreadySpawned)
            {
                return;
            }

            foreach (var entityId in Components.Respawns.GetEntitiesIds())
            {
                var respawnData = Components.Respawns[entityId].RespawnData[0];
                for (var i = 0; i < respawnData.EnemiesCount; i++)
                {
                    var randomEntityDatabaseId = -1;
                    var randomPercent = Random.Range(1, 101);

                    var lowLimit = 0;
                    for (var j = 0; j < respawnData.RespawnEnemyRecords.Length; j++)
                    {
                        var hiLimit = lowLimit + respawnData.RespawnEnemyRecords[j].Chance;
                        if (lowLimit < randomPercent && randomPercent <= hiLimit)
                        {
                            randomEntityDatabaseId = respawnData.RespawnEnemyRecords[j].EntityDatabaseId;
                            break;
                        }
                        lowLimit = hiLimit;
                    }

                    SpawnGuy(entityId, randomEntityDatabaseId);
                }
            }
            isAlreadySpawned = true;
        }

        private void SpawnGuy(int entityId, int entityRecordId)
        {
            var respawnTransform = Components.Positioning[entityId].Transform;

            var respawnPosition = respawnTransform.position;
            var respawnHalfSize = respawnTransform.localScale * 0.5f;

            Vector3 randomPosition;
            do
            {
                randomPosition = new Vector3(
                    Random.Range(respawnPosition.x - respawnHalfSize.x, respawnPosition.x + respawnHalfSize.x),
                    Random.Range(respawnPosition.y - respawnHalfSize.y, respawnPosition.y + respawnHalfSize.y),
                    Random.Range(respawnPosition.z - respawnHalfSize.z, respawnPosition.z + respawnHalfSize.z));
            }
            while (IsInHouse(randomPosition));

            MessageBus.CreateGuysMessages.Publish(new CreateGuyMessage
            {
                GuyDatabaseId = entityRecordId,
                Position = randomPosition
            });
        }


        private bool IsInHouse(Vector3 position)
        {
            var housesEntitiesIds = Components.Houses.GetEntitiesIds();

            foreach (var houseEntityId in housesEntitiesIds)
            {
                var houseComponent = Components.Houses[houseEntityId];
                if (position.x > houseComponent.LeftBottomCorner.position.x - 0.3f &&
                    position.x < houseComponent.RightUpperCorner.position.x + 0.3f &&
                    position.y > houseComponent.LeftBottomCorner.position.y - 0.3f &&
                    position.y < houseComponent.RightUpperCorner.position.y + 0.3f)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
