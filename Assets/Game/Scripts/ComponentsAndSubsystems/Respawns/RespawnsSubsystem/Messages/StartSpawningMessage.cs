namespace Brutime.LD42
{
    using BruCore;

    public struct StartSpawningMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static SingleMessageChannel<StartSpawningMessage> StartSpawningMessages = new SingleMessageChannel<StartSpawningMessage>();
    }    
}