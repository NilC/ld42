namespace Brutime.LD42
{
    using BruCore;

    public struct DestroyEntityMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<DestroyEntityMessage> DestroyEntityMessages = new ListMessagesChannel<DestroyEntityMessage>();
    }    
}