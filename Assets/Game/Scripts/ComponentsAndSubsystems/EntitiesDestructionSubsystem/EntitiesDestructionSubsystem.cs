namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class EntitiesDestructionSubsystem : Subsystem
    {
        public override void Update()
        {
            ProcessDestroyEntityMessages();
        }

        private void ProcessDestroyEntityMessages()
        {
            var messages = MessageBus.DestroyEntityMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var entityDatabaseId = CoreComponents.Entities[entityId].EntityDatabaseId;
                Factories.Entities.DestroyEntity(entityDatabaseId, entityId);
                
            }
            MessageBus.DestroyEntityMessages.Clear();
        }
    }
}
