namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public class BadGuyComponentsContainer : ComponentsContainer<BadGuyComponent, BadGuyComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, BadGuyComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
