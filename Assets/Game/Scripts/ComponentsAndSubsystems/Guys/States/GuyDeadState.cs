﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class GuyDeadState : BehaviorState
    {
        public override int StateId { get { return (int)GuyStateId.Dead; } }

        public override void OnEnter(BehaviorState previousState, int entityId)
        {
            MessageBus.IntegerValueAnimationMessages.Publish(new IntegerValueAnimationMessage
            {
                EntityId = entityId,
                AnimationId = GuysSubsystem.StateAnimationId,
                Value = StateId
            });
        }

        public override void Update(int entityId)
        {
            var component = Components.Guys[entityId];
            if (component.CurrentDeadTime <= component.DeadTime)
            {
                component.CurrentDeadTime += Time.deltaTime;
                Components.Guys[entityId] = component;
            }
            else
            {
                MessageBus.DestroyEntityMessages.Publish(new DestroyEntityMessage { EntityId = entityId });
            }
        }

        public override int GetNextStateId(int entityId)
        {
            return (int)GuyStateId.Dead;
        }
    }
}
