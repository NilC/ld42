﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class GuyReproductionState : BehaviorState
    {
        public override int StateId { get { return (int)GuyStateId.Reproduction; } }

        public override void OnEnter(BehaviorState previousState, int entityId)
        {
            MessageBus.IntegerValueAnimationMessages.Publish(new IntegerValueAnimationMessage
            {
                EntityId = entityId,
                AnimationId = GuysSubsystem.StateAnimationId,
                Value = StateId
            });
        }

        public override void Update(int entityId)
        {
            var component = Components.Guys[entityId];

            if (component.CurrentReproductionTime < component.ReproductionTime)
            {
                component.CurrentReproductionTime += Time.deltaTime;
            }

            if (component.CurrentReproductionTime >= component.ReproductionTime)
            {
                DisablePartnerPassiveReproductionAndApplyReproductionSlowtimeIfPossible(component.ReproductionPartnerId);
                component.ReproductionPartnerId = 0;

                component.IgnoreReproductionTime += component.ReproductionSlowtime;

                MessageBus.CreateGuysMessages.Publish(new CreateGuyMessage
                {
                    GuyDatabaseId = CoreComponents.Entities[entityId].EntityDatabaseId,
                    Position = Components.Positioning[entityId].Transform.position
                });
            }

            Components.Guys[entityId] = component;
        }

        public override void OnLeave(int entityId)
        {
            var component = Components.Guys[entityId];
            component.CurrentReproductionTime = 0.0f;
            component.CurrentIgnoreReproductionTime = 0.0f;
            Components.Guys[entityId] = component;
        }

        public override int GetNextStateId(int entityId)
        {
            var component = Components.Guys[entityId];

            if (MessageBus.DeadMessages.Contains(entityId))
            {
                DisablePartnerPassiveReproductionAndApplyReproductionSlowtimeIfPossible(component.ReproductionPartnerId);
                component.ReproductionPartnerId = 0;
                Components.Guys[entityId] = component;

                return (int)GuyStateId.Dead;
            }

            if (MessageBus.DeadMessages.Contains(component.ReproductionPartnerId))
            {
                component.ReproductionPartnerId = 0;
                Components.Guys[entityId] = component;

                return (int)GuyStateId.Movement;
            }

            if (component.CurrentReproductionTime >= component.ReproductionTime)
            {
                return (int)GuyStateId.Movement;
            }

            return (int)GuyStateId.Reproduction;
        }

        private void DisablePartnerPassiveReproductionAndApplyReproductionSlowtimeIfPossible(int reproductionPartnerId)
        {
            GuyComponent partnerComponent;
            if (Components.Guys.TryGet(reproductionPartnerId, out partnerComponent))
            {
                partnerComponent.IsPassiveReproduction = false;
                partnerComponent.IgnoreReproductionTime += partnerComponent.ReproductionSlowtime;
                Components.Guys[reproductionPartnerId] = partnerComponent;
            }
        }
    }
}
