﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class GuyPassiveReproductionState : BehaviorState
    {
        public override int StateId { get { return (int)GuyStateId.PassiveReproduction; } }

        public override void OnEnter(BehaviorState previousState, int entityId)
        {
            MessageBus.IntegerValueAnimationMessages.Publish(new IntegerValueAnimationMessage
            {
                EntityId = entityId,
                AnimationId = GuysSubsystem.StateAnimationId,
                Value = StateId
            });
        }

        public override void OnLeave(int entityId)
        {
            var component = Components.Guys[entityId];
            component.CurrentIgnoreReproductionTime = 0.0f;
            Components.Guys[entityId] = component;
        }

        public override int GetNextStateId(int entityId)
        {
            if (MessageBus.DeadMessages.Contains(entityId))
            {
                return (int)GuyStateId.Dead;
            }

            var component = Components.Guys[entityId];
            if (!component.IsPassiveReproduction)
            {
                return (int)GuyStateId.Movement;
            }

            return (int)GuyStateId.PassiveReproduction;
        }
    }
}
