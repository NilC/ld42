﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class GuyMovementState : BehaviorState
    {
        public override int StateId { get { return (int)GuyStateId.Movement; } }

        public override void OnEnter(BehaviorState previousState, int entityId)
        {
            MessageBus.IntegerValueAnimationMessages.Publish(new IntegerValueAnimationMessage
            {
                EntityId = entityId,
                AnimationId = GuysSubsystem.StateAnimationId,
                Value = StateId
            });

            var x = Random.Range(-1f, 1f);
            var y = Random.Range(-1f, 1f);
            var direction = new Vector3(x, y);

            direction.Normalize();

            MessageBus.UpdateDirectionMessages.Publish(new UpdateDirectionMessage
            {
                EntityId = entityId,
                DirectionVector = direction
            });

            var component = Components.LinearMovement[entityId];
            component.IsEnabled = true;
            Components.LinearMovement[entityId] = component;
        }

        public override void Update(int entityId)
        {
            ProcessDirection(entityId);
            ProcessTimeIgnoreReproduction(entityId);
        }

        public override void OnLeave(int entityId)
        {
            var component = Components.LinearMovement[entityId];
            component.IsEnabled = false;
            Components.LinearMovement[entityId] = component;
        }

        public override int GetNextStateId(int entityId)
        {
            if (MessageBus.DeadMessages.Contains(entityId))
            {
                return (int)GuyStateId.Dead;
            }

            var component = Components.Guys[entityId];
            if (component.IsPassiveReproduction)
            {
                return (int)GuyStateId.PassiveReproduction;
            }

            if (component.CurrentIgnoreReproductionTime >= component.IgnoreReproductionTime)
            {
                var guysEntitiesIds = Components.Guys[entityId].GuyKind == GuyKind.Bad
                    ? Components.BadGuys.GetEntitiesIds()
                    : Components.GoodGuys.GetEntitiesIds();

                foreach (var guyEntityId in guysEntitiesIds)
                {
                    if (Subsystems.GameDirector.GameState == GameState.Gameplay && IsCloseEnough(entityId, guyEntityId))
                    {
                        component = Components.Guys[entityId];
                        component.ReproductionPartnerId = guyEntityId;
                        Components.Guys[entityId] = component;

                        component = Components.Guys[guyEntityId];
                        component.IsPassiveReproduction = true;
                        Components.Guys[guyEntityId] = component;

                        return (int)GuyStateId.Reproduction;
                    }
                }
            }

            return (int)GuyStateId.Movement;
        }

        private void ProcessDirection(int entityId)
        {
            var position = GetPosition(entityId);
            var watchVectorMagnitude = Components.Guys[entityId].WatchVectorMagnitude;
            var direction = Components.LinearMovement[entityId].DirectionVector;
            var maxWatchPosition = position + direction * watchVectorMagnitude;

            if (maxWatchPosition.x < SettingsProviders.Level.MinX ||
                maxWatchPosition.x > SettingsProviders.Level.MaxX ||
                maxWatchPosition.y < SettingsProviders.Level.MinY ||
                maxWatchPosition.y > SettingsProviders.Level.MaxY)
            {
                ChangeDirection(entityId);
            }

            var housesEntitiesIds = Components.Houses.GetEntitiesIds();

            foreach (var houseEntityId in housesEntitiesIds)
            {
                var houseComponent = Components.Houses[houseEntityId];
                if (maxWatchPosition.x > houseComponent.LeftBottomCorner.position.x &&
                    maxWatchPosition.x < houseComponent.RightUpperCorner.position.x &&
                    maxWatchPosition.y > houseComponent.LeftBottomCorner.position.y &&
                    maxWatchPosition.y < houseComponent.RightUpperCorner.position.y)
                {
                    ChangeDirection(entityId);
                }
            }
        }

        private void ProcessTimeIgnoreReproduction(int entityId)
        {
            var component = Components.Guys[entityId];
            if (component.CurrentIgnoreReproductionTime < component.IgnoreReproductionTime)
            {
                component.CurrentIgnoreReproductionTime += Time.deltaTime;
                Components.Guys[entityId] = component;
            }
        }   

        private void ChangeDirection(int entityId)
        {
            var component = Components.LinearMovement[entityId];

            var randomAgnle = Random.Range(-90.0f, -180.0f);

            MessageBus.UpdateDirectionMessages.Publish(new UpdateDirectionMessage
            {
                EntityId = entityId,
                DirectionVector = Quaternion.Euler(0, 0, randomAgnle) * component.DirectionVector
            });
        }

        private bool IsCloseEnough(int entityId, int guyEntityId)
        {
            if (entityId == guyEntityId || Components.Guys[guyEntityId].StateId != GuyStateId.Movement)
            {
                return false;
            }

            var reproductionDistance = Components.Guys[entityId].ReproductionDistance;

            var vectorToTarget = GetPosition(entityId) - GetPosition(guyEntityId);
            //var direction = Components.LinearMovement[guyEntityId].DirectionVector;

            return vectorToTarget.sqrMagnitude <= reproductionDistance * reproductionDistance;
        }

        private Vector3 GetPosition(int entityId)
        {
            return Components.Positioning[entityId].Transform.position;
        }
    }
}
