﻿namespace Brutime.LD42
{
    public enum GuyStateId
    {
        Movement,
        Reproduction,
        PassiveReproduction,
        Dead
    }
}
