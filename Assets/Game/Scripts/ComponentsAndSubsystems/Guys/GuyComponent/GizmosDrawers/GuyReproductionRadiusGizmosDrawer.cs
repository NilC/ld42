namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class GuyReproductionRadiusGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            if (!Components.Positioning.Contains(entityId) ||
                !Components.Guys.Contains(entityId))
            {
                return;
            }

            var position = Components.Positioning[entityId].Transform.position;
            var reproductionDistance = Components.Guys[entityId].ReproductionDistance;

            var color = Color.green;
            DrawEllipse(position, reproductionDistance, reproductionDistance, color);
        }
    }
}