namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class GuyWatchVectorGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            if (!Components.Positioning.Contains(entityId) ||
                !Components.LinearMovement.Contains(entityId) ||
                !Components.Guys.Contains(entityId))
            {
                return;
            }

            var position = Components.Positioning[entityId].Transform.position;
            var direction = Components.LinearMovement[entityId].DirectionVector;
            var watchVectorMagnitude = Components.Guys[entityId].WatchVectorMagnitude;

            Gizmos.color = Color.black;
            Gizmos.DrawLine(position, position + direction * watchVectorMagnitude);
        }
    }
}