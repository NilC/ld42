namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class GuyComponentsContainer : ComponentsContainer<GuyComponent, GuyComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, GuyComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.IgnoreReproductionTime = Random.Range(component.IgnoreReproductionTimeFrom, component.IgnoreReproductionTimeTo);
            component.ReproductionTime = Random.Range(component.ReproductionTimeFrom, component.ReproductionTimeTo);            
            AddComponent(entityId, component);
        }

        public override void InitializeComponent(int entityId)
        {
            base.InitializeComponent(entityId);
            var component = Components[entityId];
            component.StateId = GuyStateId.Movement;
            Subsystems.Guys.OnEnter(entityId);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.StateId = GuyStateId.Movement;
            component.IgnoreReproductionTime = Random.Range(component.IgnoreReproductionTimeFrom, component.IgnoreReproductionTimeTo);
            component.CurrentIgnoreReproductionTime = 0.0f;
            component.ReproductionTime = Random.Range(component.ReproductionTimeFrom, component.ReproductionTimeTo);
            component.CurrentReproductionTime = 0.0f;
            component.ReproductionPartnerId = 0;
            component.IsPassiveReproduction = false;
            component.CurrentDeadTime = 0.0f;
            Components[entityId] = component;
            Subsystems.Guys.OnEnter(entityId);
        }
    }
}
