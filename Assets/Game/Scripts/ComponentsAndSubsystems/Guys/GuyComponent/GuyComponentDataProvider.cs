namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class GuyComponentDataProvider : CommonComponentDataProvider<GuyComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new[]
            {
                "StateId",
                "CurrentIgnoreReproductionTime",
                "IgnoreReproductionTime",
                "ReproductionTime",
                "CurrentReproductionTime",
                "ReproductionPartnerId",
                "IsPassiveReproduction",
                "CurrentDeadTime"
            };
        }

        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[]
            {
                new GuyWatchVectorGizmosDrawer(),
                new GuyReproductionRadiusGizmosDrawer()
            };
        }
#endif
    }
}
