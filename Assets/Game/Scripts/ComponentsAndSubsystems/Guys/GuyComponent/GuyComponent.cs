namespace Brutime.LD42
{
    using System;

    [Serializable]
    public struct GuyComponent
    {
        public GuyStateId StateId;

        public GuyKind GuyKind;

        public float WatchVectorMagnitude;

        public float IgnoreReproductionTimeFrom;
        public float IgnoreReproductionTimeTo;
        public float IgnoreReproductionTime;
        public float ReproductionSlowtime;
        public float CurrentIgnoreReproductionTime;

        public float ReproductionDistance;
        public float ReproductionTimeFrom;
        public float ReproductionTimeTo;
        public float ReproductionTime;
        public float CurrentReproductionTime;
        public int ReproductionPartnerId;
        public bool IsPassiveReproduction;

        public float DeadTime;
        public float CurrentDeadTime;
    }
}