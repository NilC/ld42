﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class GuysSubsystem : Subsystem
    {
        public static int StateAnimationId = Animator.StringToHash("State");

        BehaviorStateMachine stateMachine;

        public GuysSubsystem() : base()
        {
            stateMachine = new BehaviorStateMachine();

            stateMachine.AddState(new GuyMovementState());
            stateMachine.AddState(new GuyReproductionState());
            stateMachine.AddState(new GuyPassiveReproductionState());
            stateMachine.AddState(new GuyDeadState());
        }

        public override void Update()
        {
            ProcessCreateGuysMessages();
            ProcessStates();
        }

        private void ProcessStates()
        {
            foreach (var entityId in Components.Guys.GetEntitiesIds())
            {
                var currentStateId = Components.Guys[entityId].StateId;
                var stateId = (GuyStateId)stateMachine.UpdateStates(entityId, (int)currentStateId);
                var component = Components.Guys[entityId];
                component.StateId = stateId;
                Components.Guys[entityId] = component;
            }
        }

        public void OnEnter(int entityId)
        {
            var component = Components.Guys[entityId];
            stateMachine.GetState((int)component.StateId).OnEnter(null, entityId);
        }

        private void ProcessCreateGuysMessages()
        {
            var messages = MessageBus.CreateGuysMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                Factories.Entities.CreateEntity(messages[i].GuyDatabaseId, messages[i].Position);
            }
            MessageBus.CreateGuysMessages.Clear();
        }
    }
}
