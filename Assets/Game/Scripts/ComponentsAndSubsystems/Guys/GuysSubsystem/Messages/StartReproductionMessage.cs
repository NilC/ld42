namespace Brutime.LD42
{
    using BruCore;

    public struct StartReproductionMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static DictionaryMessagesChannel<StartReproductionMessage> StartReproductionMessages = new DictionaryMessagesChannel<StartReproductionMessage>();
    }    
}