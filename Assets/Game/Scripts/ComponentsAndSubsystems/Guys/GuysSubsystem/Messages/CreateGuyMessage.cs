﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public struct CreateGuyMessage
    {
        public int GuyDatabaseId;
        public Vector3 Position;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static ListMessagesChannel<CreateGuyMessage> CreateGuysMessages = new ListMessagesChannel<CreateGuyMessage>();
    }
}