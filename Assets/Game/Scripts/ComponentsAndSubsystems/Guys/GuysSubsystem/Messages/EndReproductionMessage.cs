namespace Brutime.LD42
{
    using BruCore;

    public struct EndReproductionMessage
    {
        public int EntityId;

        public override string ToString() { return MessageDebugHelper.GetString(this); }
    }

    public partial class MessageBus : CoreMessageBus
    {
        public static DictionaryMessagesChannel<EndReproductionMessage> EndReproductionMessages = new DictionaryMessagesChannel<EndReproductionMessage>();
    }    
}