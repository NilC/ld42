namespace Brutime.LD42
{
    using UnityEngine;
    using BruCore;

    public class GoodGuyComponentsContainer : ComponentsContainer<GoodGuyComponent, GoodGuyComponentDataProvider>
    {        
        public override void GetOrCreateNewComponentAndAdd(int entityId, GoodGuyComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
