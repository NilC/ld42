namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class HouseComponentDataProvider : CommonComponentDataProvider<HouseComponent>
    {
#if UNITY_EDITOR
        public override string[] GetHiddenFieldsNames()
        {
            return new[] { "LeftBottomCorner", "RightUpperCorner" };
        }

        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[] { new HouseComponentGizmosDrawer() };
        }
#endif
    }
}
