namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    public class HouseComponentGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            if (!Components.Houses.Contains(entityId))
            {
                return;
            }

            var component = Components.Houses[entityId];

            var color = Color.red;

            DrawRectangle(
                component.LeftBottomCorner.position.x,
                component.LeftBottomCorner.position.y,
                component.RightUpperCorner.position.x,
                component.RightUpperCorner.position.y,
                color);
        }
    }
}