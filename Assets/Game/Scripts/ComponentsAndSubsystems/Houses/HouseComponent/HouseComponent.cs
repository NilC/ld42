namespace Brutime.LD42
{
    using System;
    using UnityEngine;

	[Serializable]
    public struct HouseComponent
    {
        public Transform LeftBottomCorner;
        public Transform RightUpperCorner;
    }
}   