namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class HouseComponentsContainer : ComponentsContainer<HouseComponent, HouseComponentDataProvider>
    {
        public static string LeftBottomCornerTag = "LeftBottomCorner";
        public static string RightUpperCornerTag = "RightUpperCorner";

        public override void GetOrCreateNewComponentAndAdd(int entityId, HouseComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.LeftBottomCorner = CoreComponents.Entities[entityId].GameObject.GetTransformWithTag(LeftBottomCornerTag);
            component.RightUpperCorner = CoreComponents.Entities[entityId].GameObject.GetTransformWithTag(RightUpperCornerTag);
            AddComponent(entityId, component);
        }
    }
}
