﻿namespace Brutime.LD42.Editor
{
    using Brutime.BruCore;
    using Brutime.BruCore.EditorUtils;
    using UnityEditor;

    public static class MainMenu
    {
        [MenuItem("Template/Create/Ability")]
        public static void CreateAbility()
        {
            ScriptableObjectUtility.CreateAsset<Ability>("Ability");
        }

        [MenuItem("Template/Create/Actions/Damage")]
        public static void CreateDamageAction()
        {
            ScriptableObjectUtility.CreateAsset<DamageAction>("DamageAction");
        }

        [MenuItem("Template/Create/Entities Database")]
        public static void CreateEntitiesDatabase()
        {
            ScriptableObjectUtility.CreateAsset<EntitiesDatabase>("EntitiesDatabase");
        }

        [MenuItem("Template/Create/Entity Specification")]
        public static void CreateEntitySpecification()
        {
            ScriptableObjectUtility.CreateAsset<EntitySpecification>("EntitySpecification");
        }

        //[MenuItem("Template/Create/Enemies/States/Enemy Idle State")]
        //public static void CreateEnemyIdleState()
        //{
        //    ScriptableObjectUtility.CreateAsset<EnemyIdleState>("EnemyIdleState");
        //}

        //[MenuItem("Template/Create/Enemies/States/Enemy Approach State")]
        //public static void CreateEnemyApproachState()
        //{
        //    ScriptableObjectUtility.CreateAsset<EnemyApproachState>("EnemyApproachState");
        //}

        //[MenuItem("Template/Create/Enemies/States/Enemy Melee Attack State")]
        //public static void CreateEnemyAttackState()
        //{
        //    ScriptableObjectUtility.CreateAsset<EnemyMeleeAttackState>("EnemyAttackState");
        //}

        //[MenuItem("Template/Create/Enemies/States/Enemy Dead State")]
        //public static void CreateEnemyDeadState()
        //{
        //    ScriptableObjectUtility.CreateAsset<EnemyDeadState>("EnemyDeadState");
        //}

        //[MenuItem("Template/Create/Common Decisions/Always True")]
        //public static void CreateAlwaysTrueDecision()
        //{
        //    ScriptableObjectUtility.CreateAsset<AlwaysTrueDecision>("AlwaysTrueDecision");
        //}

        //[MenuItem("Template/Create/Common Decisions/Update Point")]
        //public static void CreateUpdatePointDecision()
        //{
        //    ScriptableObjectUtility.CreateAsset<UpdatePointDecision>("UpdatePointDecision");
        //}

        //[MenuItem("Template/Create/Common Decisions/Point Reached")]
        //public static void CreateReachedPointDecision()
        //{
        //    ScriptableObjectUtility.CreateAsset<PointReachedDecision>("PointReachedDecision");
        //}

        //[MenuItem("Template/Create/Common Decisions/Attack Finished Decision")]
        //public static void CreateAttackFinishedDecision()
        //{
        //    ScriptableObjectUtility.CreateAsset<AttackFinishedDecision>("AttackFinishedDecision");
        //}

        //[MenuItem("Template/Create/Common Decisions/Dead Decision")]
        //public static void CreateDeadDecision()
        //{
        //    ScriptableObjectUtility.CreateAsset<DeadDecision>("DeadDecision");
        //}
    }
}