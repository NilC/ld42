﻿namespace Brutime.LD42.Editor
{
    using System.Linq;
    using System.Reflection;
    using Brutime.BruCore.Editor;
    using Brutime.BruCore.EditorUtils;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Ability))]
    public class AbilityEditor : CustomInspectorEditor
    {
        //private enum ToggleEnum 
        //{
        //    False = 0,
        //    True = 1
        //}

        private readonly string[] abilityTargetingOptions =
        {
            "None",
            "TargetOrPoint",
            "Target",
            "Area",
            "Everybody"
        };

        private readonly string[] abilityCastingOptions =
        {
            "Animation",
            "Projectile"
        };

        private int abilityCastingIndex;
        private int previousAbilityCastingIndex;

        private int abilityTargetingIndex;
        private int previousAbilityTargetingIndex;

        private string[] actionTypeNames = new string[0];
        private int actionTypeIndex = -1;

        private int buffEfectActionTypeIndex = -1;

        private void OnEnable()
        {
            var ability = (Ability)target;

            //if (ability.Casting == null)
            //{
            //    UpdateCasting(ability, 0);
            //}

            //abilityCastingIndex = GetAbilityCastingIndex(ability);
            //previousAbilityCastingIndex = abilityCastingIndex;

            //if (ability.Behavior == null)
            //{
            //    UpdateTargeting(ability, 0);
            //}

            //abilityTargetingIndex = GetAbilityTargetingIndex(ability);
            //previousAbilityTargetingIndex = abilityTargetingIndex;

            UpdateActionsTypeNames();
        }

        public override void OnInspectorGUI()
        {
            var ability = (Ability)target;

            EditorGUILayout.LabelField(ability.name, EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            ability.Icon = (Sprite)EditorGUILayout.ObjectField("Icon", ability.Icon, typeof(Sprite), allowSceneObjects: true);
            EditorGUILayout.EndHorizontal();

            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Casting");
            //abilityCastingIndex = EditorGUILayout.Popup(abilityCastingIndex, abilityCastingOptions);
            //if (abilityCastingIndex != previousAbilityCastingIndex)
            //{
            //    UpdateCasting(ability, abilityCastingIndex);
            //    previousAbilityCastingIndex = abilityCastingIndex;
            //}
            //EditorGUILayout.EndHorizontal();

            //if (abilityCastingIndex == 1)
            //{
            //    EditorGUILayout.BeginHorizontal();
            //    EditorGUILayout.LabelField("Speed");
            //    (ability.Casting as AbilityCastingProjectile).Speed = EditorGUILayout.FloatField((ability.Casting as AbilityCastingProjectile).Speed);
            //    EditorGUILayout.EndHorizontal();
            //}

            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Behavior");
            //abilityTargetingIndex = EditorGUILayout.Popup(abilityTargetingIndex, abilityTargetingOptions);
            //if (abilityTargetingIndex != previousAbilityTargetingIndex)
            //{
            //    UpdateTargeting(ability, abilityTargetingIndex);
            //    previousAbilityTargetingIndex = abilityTargetingIndex;
            //}
            //EditorGUILayout.EndHorizontal();

            //if (abilityTargetingIndex == 1)
            //{
            //    EditorGUILayout.BeginHorizontal();
            //    EditorGUILayout.LabelField("Radius");
            //    (ability.Behavior as AbilityBehaviorTargetOrPoint).Radius = EditorGUILayout.FloatField((ability.Behavior as AbilityBehaviorTargetOrPoint).Radius);
            //    EditorGUILayout.EndHorizontal();
            //}

            //if (abilityTargetingIndex == 3)
            //{
            //    EditorGUILayout.BeginHorizontal();
            //    EditorGUILayout.LabelField("Radius");
            //    (ability.Behavior as AbilityBehaviorArea).Radius = EditorGUILayout.FloatField((ability.Behavior as AbilityBehaviorArea).Radius);
            //    EditorGUILayout.EndHorizontal();
            //}

            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Target Type");
            //ability.Behavior.TargetTeam = (AbilityTargetTeam)EditorGUILayout.EnumPopup(ability.Behavior.TargetTeam);
            //EditorGUILayout.EndHorizontal();

            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Mana Cost");
            //ability.ManaCost = EditorGUILayout.IntField(ability.ManaCost);
            //EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cooldown Time");
            ability.CooldownTime = EditorGUILayout.FloatField(ability.CooldownTime);
            EditorGUILayout.EndHorizontal();

            ActionsLayout(ability);
            //BuffEfectsLayout(ability);
            //EditorLayout.Array("Actions", ref ability.Actions);
            //EditorLayout.Array("Buff Effects", ref ability.BuffEffects);

            MarkForSaveIfChanged();
        }

        //private void UpdateCasting(Ability ability, int castingIndex)
        //{
        //    if (ability.Casting != null)
        //    {
        //        DestroyImmediate(ability.Casting, true);
        //    }

        //    AbilityCasting casting;

        //    switch (castingIndex)
        //    {
        //        case 0:
        //            casting = CreateInstance<AbilityCastingAnimation>();
        //            casting.name = "CastingAnimation";
        //            break;

        //        case 1:
        //            casting = CreateInstance<AbilityCastingProjectile>();
        //            casting.name = "CastingProjectile";
        //            break;

        //        default:
        //            throw new UnityException(string.Format("Unknown castingIndex: {0}", castingIndex));
        //    }

        //    AssetDatabase.AddObjectToAsset(casting, ability);

        //    ability.Casting = casting;

        //    MarkForSaveIfChanged();
        //    SaveAndRefreshAssets();
        //}

        //private int GetAbilityCastingIndex(Ability ability)
        //{
        //    if (ability.Casting is AbilityCastingAnimation)
        //    {
        //        return 0;
        //    }

        //    if (ability.Casting is AbilityCastingProjectile)
        //    {
        //        return 1;
        //    }

        //    throw new UnityException("Unknown AbilityCasting");
        //}

        //private void UpdateTargeting(Ability ability, int targetingIndex)
        //{
        //    if (ability.Behavior != null)
        //    {
        //        DestroyImmediate(ability.Behavior, true);
        //    }

        //    AbilityBehavior behavior;

        //    switch (targetingIndex)
        //    {
        //        case 0:
        //            behavior = CreateInstance<AbilityBehaviorNone>();
        //            behavior.name = "TargetingNone";
        //            break;

        //        case 1:
        //            behavior = CreateInstance<AbilityBehaviorTargetOrPoint>();
        //            behavior.name = "TargetingTargetOrPoint";
        //            break;

        //        case 2:
        //            behavior = CreateInstance<AbilityBehaviorTarget>();
        //            behavior.name = "TargetingTarget";
        //            break;

        //        case 3:
        //            behavior = CreateInstance<AbilityBehaviorArea>();
        //            behavior.name = "TargetingArea";
        //            break;

        //        case 4:
        //            behavior = CreateInstance<AbilityBehaviorEverybody>();
        //            behavior.name = "TargetingEverybody";
        //            break;

        //        default:
        //            throw new UnityException(string.Format("Unknown targetingIndex: {0}", targetingIndex));
        //    }

        //    AssetDatabase.AddObjectToAsset(behavior, ability);

        //    ability.Behavior = behavior;

        //    MarkForSaveIfChanged();
        //    SaveAndRefreshAssets();
        //}

        //private int GetAbilityTargetingIndex(Ability ability)
        //{
        //    if (ability.Behavior is AbilityBehaviorNone)
        //    {
        //        return 0;
        //    }

        //    if (ability.Behavior is AbilityBehaviorTargetOrPoint)
        //    {
        //        return 1;
        //    }

        //    if (ability.Behavior is AbilityBehaviorTarget)
        //    {
        //        return 2;
        //    }

        //    if (ability.Behavior is AbilityBehaviorArea)
        //    {
        //        return 3;
        //    }

        //    if (ability.Behavior is AbilityBehaviorEverybody)
        //    {
        //        return 4;
        //    }

        //    throw new UnityException("Unknown AbilityBehavior");
        //}

        private void ActionsLayout(Ability ability)
        {
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Actions");
            actionTypeIndex = EditorGUILayout.Popup(actionTypeIndex, actionTypeNames);
            if (actionTypeIndex != -1)
            {
                var newAction = CreateInstance(actionTypeNames[actionTypeIndex]) as AbilityAction;
                newAction.name = actionTypeNames[actionTypeIndex];
                AssetDatabase.AddObjectToAsset(newAction, ability);
                SaveAndRefreshAssets();
                ArrayUtility.Add(ref ability.Actions, newAction);

                actionTypeIndex = -1;
            }
            EditorGUILayout.EndHorizontal();

            var indexToDelete = -1;
            for (var i = 0; i < ability.Actions.Length; i++)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);

                EditorGUILayout.BeginHorizontal();
                if (ability.Actions[i] != null)
                {
                    EditorGUILayout.LabelField(ability.Actions[i].GetCaption(), EditorStyles.boldLabel);
                }
                else
                {
                    EditorGUILayout.LabelField("Null Action", EditorStyles.boldLabel);
                }
                if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                {
                    var tmp = ability.Actions[i];
                    ability.Actions[i] = ability.Actions[i - 1];
                    ability.Actions[i - 1] = tmp;
                }
                if (GUILayout.Button("↓", GUILayout.Width(30)) && i < ability.Actions.Length - 1)
                {
                    var tmp = ability.Actions[i];
                    ability.Actions[i] = ability.Actions[i + 1];
                    ability.Actions[i + 1] = tmp;
                }
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    indexToDelete = i;
                }
                EditorGUILayout.EndHorizontal();

                if (ability.Actions[i] != null)
                {
                    EditorGUILayout.Space();
                    ability.Actions[i].DoLayout();
                    EditorGUILayout.Space();
                }

                EditorGUILayout.EndVertical();
            }

            if (indexToDelete > -1)
            {
                DestroyImmediate(ability.Actions[indexToDelete], true);
                ArrayUtility.RemoveAt(ref ability.Actions, indexToDelete);
                SaveAndRefreshAssets();
            }
        }

        private void UpdateActionsTypeNames()
        {
            var types = Assembly.GetAssembly(typeof(AbilityAction)).GetTypes();

            actionTypeNames = types
                .Where(x => x.IsSubclassOf(typeof(AbilityAction)))
                .Select(x => ScriptableObjectUtility.GetNameForUi(x.FullName))
                .ToArray();
        }

        //private void BuffEfectsLayout(Ability ability)
        //{
        //    EditorGUILayout.Space();
        //    EditorGUILayout.BeginHorizontal();
        //    EditorGUILayout.LabelField("Buff Effects");
        //    if (GUILayout.Button("+", GUILayout.Width(88)))
        //    {
        //        var newBuffEfect = CreateInstance<AbilityBuffEffect>();
        //        newBuffEfect.name = "BuffEffect";
        //        AssetDatabase.AddObjectToAsset(newBuffEfect, ability);
        //        SaveAndRefreshAssets();
        //        ArrayUtility.Add(ref ability.BuffEffects, newBuffEfect);
        //    }
        //    EditorGUILayout.EndHorizontal();

        //    var indexToDelete = -1;
        //    for (var i = 0; i < ability.BuffEffects.Length; i++)
        //    {
        //        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        //        EditorGUILayout.BeginHorizontal();
        //        EditorGUILayout.LabelField("Buff Effect", EditorStyles.boldLabel);
        //        if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
        //        {
        //            var tmp = ability.BuffEffects[i];
        //            ability.BuffEffects[i] = ability.BuffEffects[i - 1];
        //            ability.BuffEffects[i - 1] = tmp;
        //        }
        //        if (GUILayout.Button("↓", GUILayout.Width(30)) && i < ability.BuffEffects.Length - 1)
        //        {
        //            var tmp = ability.BuffEffects[i];
        //            ability.BuffEffects[i] = ability.BuffEffects[i + 1];
        //            ability.BuffEffects[i + 1] = tmp;
        //        }
        //        if (GUILayout.Button("X", GUILayout.Width(20)))
        //        {
        //            indexToDelete = i;
        //        }
        //        EditorGUILayout.EndHorizontal();

        //        EditorGUILayout.Space();

        //        if (ability.BuffEffects[i] == null)
        //        {
        //            EditorGUILayout.EndVertical();
        //            continue;
        //        }

        //        ability.BuffEffects[i].Duration = EditorGUILayout.FloatField("Duration", ability.BuffEffects[i].Duration);
        //        ability.BuffEffects[i].ApplyFrequency = EditorGUILayout.FloatField("Apply Frequency", ability.BuffEffects[i].ApplyFrequency);

        //        EditorGUILayout.Space();

        //        var isNeedToRemoveAction = false;
        //        if (ability.BuffEffects[i].Action != null)
        //        {
        //            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        //            EditorGUILayout.BeginHorizontal();
        //            EditorGUILayout.LabelField(ability.BuffEffects[i].Action.GetCaption(), EditorStyles.boldLabel);
        //            if (GUILayout.Button("X", GUILayout.Width(20)))
        //            {
        //                isNeedToRemoveAction = true;
        //            }
        //            EditorGUILayout.EndHorizontal();

        //            ability.BuffEffects[i].Action.DoLayout();
        //            EditorGUILayout.EndVertical();
        //        }
        //        else
        //        {
        //            EditorGUILayout.BeginVertical();
        //            EditorGUILayout.LabelField("Action");
        //            buffEfectActionTypeIndex = EditorGUILayout.Popup(buffEfectActionTypeIndex, actionTypeNames);
        //            if (buffEfectActionTypeIndex != -1)
        //            {
        //                var newAction = CreateInstance(actionTypeNames[buffEfectActionTypeIndex]) as AbilityAction;
        //                newAction.name = "BuffEffect" + actionTypeNames[buffEfectActionTypeIndex];
        //                AssetDatabase.AddObjectToAsset(newAction, ability);
        //                SaveAndRefreshAssets();
        //                ability.BuffEffects[i].Action = newAction;

        //                buffEfectActionTypeIndex = -1;
        //            }
        //            EditorGUILayout.EndVertical();
        //        }

        //        EditorGUILayout.Space();

        //        if (isNeedToRemoveAction)
        //        {
        //            DestroyImmediate(ability.BuffEffects[i].Action, true);
        //            ability.BuffEffects[i].Action = null;
        //            SaveAndRefreshAssets();
        //        }

        //        EditorGUILayout.EndVertical();
        //    }

        //    if (indexToDelete > -1)
        //    {
        //        if (ability.BuffEffects[indexToDelete] != null)
        //        {
        //            DestroyImmediate(ability.BuffEffects[indexToDelete].Action, true);
        //        }
        //        DestroyImmediate(ability.BuffEffects[indexToDelete], true);
        //        ArrayUtility.RemoveAt(ref ability.BuffEffects, indexToDelete);
        //        SaveAndRefreshAssets();
        //    }
        //}
    }
}