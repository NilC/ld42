﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;
    using UnityEngine.UI;

    public class HeroParametersController : Controller
    {
        [SerializeField]
        private Image heroHealthImage;

        //[SerializeField]
        //private Image heroManaImage;

        private void Update()
        {
            var messages = MessageBus.HealthDamagedMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                
                if (Components.Players.Contains(entityId))
                {
                    heroHealthImage.transform.localScale = new Vector3(message.HealthFactor, heroHealthImage.transform.localScale.y, heroHealthImage.transform.localScale.z);
                    //heroManaImage.transform.localScale = new Vector3(heroSpellCasterController.GetManaFactor(), heroManaImage.transform.localScale.y, heroManaImage.transform.localScale.z);
                }
            }           
        }
    }
}
