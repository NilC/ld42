﻿namespace Brutime.LD42
{
    using UnityEngine;

    public struct HealthBarComponent
    {
        public int ShowHealthBarAnimationHash;
        public int HideHealthBarAnimationHash;

        public float VisibleTime;
        public Transform ForegroundTransform;
        public bool IsVisible;
        public float CurrentVisibleTime;
        public Vector3 FullHealthScale;
    }
}
