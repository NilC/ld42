﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public class HealthBarComponentsContainer : ComponentsContainer<HealthBarComponent, HealthBarComponentDataProvider>
    {
        public static string ComponentTag = "HealthBarForeground";

        public override void GetOrCreateNewComponentAndAdd(int entityId, HealthBarComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.ForegroundTransform = CoreComponents.Entities[entityId].GameObject.GetTransformWithTag(ComponentTag);
            AddComponent(entityId, component);
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];

            component.IsVisible = false;
            component.CurrentVisibleTime = 0.0f;
            component.ForegroundTransform.localScale = component.FullHealthScale;

            Components[entityId] = component;
        }
    }
}

