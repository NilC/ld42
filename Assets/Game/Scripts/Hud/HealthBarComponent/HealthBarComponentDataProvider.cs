﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using System;
    using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    public class HealthBarComponentDataProvider : ComponentDataProvider
    {
        public string ShowHealthBarAnimationName;
        public int ShowHealthBarAnimationHash;

        public string HideHealthBarAnimationName;
        public int HideHealthBarAnimationHash;

        public float VisibleTime;

        public override Type GetComponentType()
        {
            return typeof(HealthBarComponent);
        }

        public HealthBarComponent GetComponent()
        {
            var component = new HealthBarComponent
            {
                ShowHealthBarAnimationHash = Animator.StringToHash(ShowHealthBarAnimationName),
                HideHealthBarAnimationHash = Animator.StringToHash(HideHealthBarAnimationName),
                VisibleTime = VisibleTime
            };

            return component;
        }

#if UNITY_EDITOR
        public override void DrawLayout()
        {
            ShowHealthBarAnimationName = EditorGUILayout.TextField("Show Health Bar Animation Name", ShowHealthBarAnimationName);
            ShowHealthBarAnimationHash = Animator.StringToHash(ShowHealthBarAnimationName);

            HideHealthBarAnimationName = EditorGUILayout.TextField("Hide Health Bar Animation Name", HideHealthBarAnimationName);
            HideHealthBarAnimationHash = Animator.StringToHash(HideHealthBarAnimationName);

            VisibleTime = EditorGUILayout.FloatField("Visible Time", VisibleTime);
        }
#endif
    }
}
