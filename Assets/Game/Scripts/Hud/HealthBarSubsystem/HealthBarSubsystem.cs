﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class HealthBarSubsystem : Subsystem        
    {
        public override void Update()
        {
            ProcessHealthHealedMessages();
            ProcessHealthDamagedMessages();
            UpdateVisibility();
        }

        private void ProcessHealthHealedMessages()
        {
            var messages = MessageBus.HealthHealedMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var healthFactor = message.HealthFactor;

                ShowHealthBarIfExists(entityId, healthFactor);
            }
        }

        private void ProcessHealthDamagedMessages()
        {
            var messages = MessageBus.HealthDamagedMessages.GetAll();
            for (var i = 0; i < messages.Count; i++)
            {
                var message = messages[i];
                var entityId = message.EntityId;
                var healthFactor = message.HealthFactor;

                ShowHealthBarIfExists(entityId, healthFactor);
            }
        }

        private void ShowHealthBarIfExists(int entityId, float healthFactor)
        {
            HealthBarComponent component;
            var isExists = Components.HealthBars.TryGet(entityId, out component);
            if (!isExists)
            {
                return;
            }

            component.ForegroundTransform.localScale = new Vector3
            (
                component.FullHealthScale.x * healthFactor,
                component.FullHealthScale.y,
                component.FullHealthScale.z
            );

            if (!component.IsVisible)
            {
                component.IsVisible = true;
                CreateTriggerAnimationMessage(entityId, component.ShowHealthBarAnimationHash);
            }

            component.CurrentVisibleTime = 0.0f;

            Components.HealthBars[entityId] = component;
        }

        private void UpdateVisibility()
        {
            foreach (var entityId in Components.HealthBars.GetEntitiesIds())
            {
                var component = Components.HealthBars[entityId];
                if (component.IsVisible)
                {
                    component.CurrentVisibleTime += Time.deltaTime;

                    if (component.CurrentVisibleTime > component.VisibleTime)
                    {
                        component.CurrentVisibleTime = 0.0f;
                        component.IsVisible = false;

                        CreateTriggerAnimationMessage(entityId, component.HideHealthBarAnimationHash);
                    }

                    Components.HealthBars[entityId] = component;
                }
            }
        }

        private void CreateTriggerAnimationMessage(int entityId, int animationId)
        {
            MessageBus.TriggerAnimationMessages.Publish(new TriggerAnimationMessage
            {
                EntityId = entityId,
                AnimationId = animationId
            });
        }
    }
}
