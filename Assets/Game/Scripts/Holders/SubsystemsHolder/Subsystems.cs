﻿namespace Brutime.LD42
{
    using BruCore;
    using System.Collections.Generic;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Reflection;
#endif

    [ExecuteInEditMode]
    public class Subsystems : CoreSubsystems
    {
        //For tests
        public static TestSubsystem Test = new TestSubsystem();

        //Collision Events
        //public static CollisionEventsSubsystem CollisionEvents = new CollisionEventsSubsystem();

        //InputAndHero
        public static InputSubsystem Input = new InputSubsystem();
        public static PlayerSubsystem Players = new PlayerSubsystem();

        //Enemies       
        public static GuysSubsystem Guys = new GuysSubsystem();

        //Respawns
        public static RespawnsSubsystem Respawns = new RespawnsSubsystem();

        //Projectiles
        public static TargetProjectilesSubsystem TargetProjectiles = new TargetProjectilesSubsystem();
        public static PositionProjectileSubsystem PositionProjectiles = new PositionProjectileSubsystem();
        public static ProjectileDebrisesSubsystem ProjectileDebrises = new ProjectileDebrisesSubsystem();

        //Player abilities
        public static AbilityManagmentSubsystem AbilityManagment = new AbilityManagmentSubsystem();
        //public static MiniRocketSubsystem MiniRocket = new MiniRocketSubsystem();
        public static RepellerSubsystem Repellers = new RepellerSubsystem();
        public static RadiationCloudSubsystem RadiationClouds = new RadiationCloudSubsystem();

        //Lifetime
        public static LifetimeSubsystem Lifetimes = new LifetimeSubsystem();

        //Attack and Health
        public static AttackSubsystem Attack = new AttackSubsystem();
        public static DamageSubsystem Damage = new DamageSubsystem();
        public static HealSubsystem Heal = new HealSubsystem();

        //Animation Events
        //public static AnimationEventsSubsystem AnimationEvents = new AnimationEventsSubsystem();

        //Movement
        public static LinearMovementSubsystem LinearMovement = new LinearMovementSubsystem();
        public static MovementToPointSubsystem MovementToPoint = new MovementToPointSubsystem();

        //Positioning
        public static PositioningSubsystem Positioning = new PositioningSubsystem();

        //Rendering
        public static AnimationSubsystem Animation = new AnimationSubsystem();
        //public static FlippingSubsystem Flipping = new FlippingSubsystem();
        public static RenderOrderByYSubsystem RenderOrderByY = new RenderOrderByYSubsystem();

        //Hud
        public static HealthBarSubsystem HealthBar = new HealthBarSubsystem();

        //GameDirector
        public static GameDirector GameDirector = new GameDirector();

        public static EntitiesDestructionSubsystem EntitiesDestruction = new EntitiesDestructionSubsystem();

        protected override List<ISubsystem> GetGameSubsystems()
        {
            var gameSubsystems = new List<ISubsystem>
            {
                //For tests
                Test,

                ////Collision Events
                //CollisionEvents,

                //InputAndHero
                Input,
                Players,
                //HeroIdleState,
                //HeroDeadState,

                //Enemies
                Guys,
                //EnemyAnimation,
                //EnemyIdleState,
                //EnemyApproachState,
                //EnemyMeleeAttackState,
                //EnemyDeadState,               

                Respawns,

                //Projectiles
                TargetProjectiles,
                PositionProjectiles,
                ProjectileDebrises,

                // Player abilities
                AbilityManagment,
                //MiniRocket,
                Repellers,
                RadiationClouds,

                //Health And Damage
                Attack,
                Damage,
                Heal,

                ////Animation Events
                //AnimationEvents,

                //Lifetimes
                Lifetimes,

                //Movement
                LinearMovement,
                MovementToPoint,

                //Positioning
                Positioning,

                //Rendering
                Animation,
                //Flipping,
                RenderOrderByY,

                //Hud
                HealthBar,

                GameDirector,

                EntitiesDestruction
            };

#if UNITY_EDITOR
            var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.Static);
            if (fields.Length != gameSubsystems.Count)
            {
                throw new UnityException("Count of declared subsystems is not equal to count of utilized systems");
            }
#endif
            return gameSubsystems;
        }
    }
}
