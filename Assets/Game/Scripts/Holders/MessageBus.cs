﻿namespace Brutime.LD42
{
    using Brutime.BruCore;

    public partial class MessageBus : CoreMessageBus
    {
        public void Awake()
        {
            Clear();
        }
    }
}
