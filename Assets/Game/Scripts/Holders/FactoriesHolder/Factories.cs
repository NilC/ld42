namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class Factories : Controller
    {
        public static EntitiesFactory Entities;

        private void Awake()
        {
            Entities = FindComponentWithTagOnScene<EntitiesFactory>(EntitiesFactory.Tag);
        }
    }
}
