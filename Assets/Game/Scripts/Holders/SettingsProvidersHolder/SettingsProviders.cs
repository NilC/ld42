namespace Brutime.LD42
{
    using Brutime.BruCore;
    using UnityEngine;

    [ExecuteInEditMode]
    public class SettingsProviders : Controller
    {
        public static LevelSettingsProvider Level;

        private void Awake()
        {            
            Level = FindComponentWithTagOnScene<LevelSettingsProvider>(LevelSettingsProvider.Tag);         
        }
    }
}
