﻿namespace Brutime.LD42
{
    using Brutime.BruCore;
    using System.Collections.Generic;

#if UNITY_EDITOR
    using System.Reflection;
    using UnityEngine;
#endif

    public class Components : CoreComponents
    {
        //Input And Hero
        public static InputComponentContainer Input = new InputComponentContainer();
        public static PlayerComponentsContainer Players = new PlayerComponentsContainer();

        //Guys
        public static GuyComponentsContainer Guys = new GuyComponentsContainer();
        public static BadGuyComponentsContainer BadGuys = new BadGuyComponentsContainer();
        public static GoodGuyComponentsContainer GoodGuys = new GoodGuyComponentsContainer();

        //Respawns
        public static HouseComponentsContainer Houses = new HouseComponentsContainer();

        //Respawns
        public static RespawnComponentsContainer Respawns = new RespawnComponentsContainer();
       
        //Projectiles
        public static TargetComponentsContainer Targets = new TargetComponentsContainer();
        public static ProjectileSourceComponentsContainer ProjectileSources = new ProjectileSourceComponentsContainer();
        public static TargetProjectileComponentsContainer TargetProjectiles = new TargetProjectileComponentsContainer();
        public static PositionProjectileComponentsContainer PositionProjectiles = new PositionProjectileComponentsContainer();
        public static ProjectileDebrisComponentsContainer ProjectileDebrises = new ProjectileDebrisComponentsContainer();

        //Lifetime
        public static LifetimeComponentsContainer Lifetimes = new LifetimeComponentsContainer();

        //Player ablities
        public static AbilityManagmentComponentsContainer AbilityManagers = new AbilityManagmentComponentsContainer();
        public static RepellerComponentsContainer Repellers = new RepellerComponentsContainer();
        public static RadiationCloudComponentsContainer RadiationClounds = new RadiationCloudComponentsContainer();
        public static MiniRocketComponentsContainer MiniRockets = new MiniRocketComponentsContainer();

        //Attack and Health
        public static HealthComponentsContainer Health = new HealthComponentsContainer();
        public static DefenceComponentsContainer Defence = new DefenceComponentsContainer();

        //Movement
        public static LinearMovementComponentsContainer LinearMovement = new LinearMovementComponentsContainer();
        public static MovementToPointComponentsContainer MovementToPoint = new MovementToPointComponentsContainer();

        //Positioning
        public static PositioningComponentsContainer Positioning = new PositioningComponentsContainer();

        //Rendering
        public static AnimationComponentsContainer Animation = new AnimationComponentsContainer();
        public static FlippingComponentsContainer Flipping = new FlippingComponentsContainer();
        public static RenderOrderByYComponentsContainer RenderOrderByY = new RenderOrderByYComponentsContainer();

        //Hud
        public static HealthBarComponentsContainer HealthBars = new HealthBarComponentsContainer();


        protected override List<IComponentsContainer> GetGameComponentsContainers()
        {
            var gameComponentsContainers = new List<IComponentsContainer>
            {
                Input,
                Players,

                Guys,
                BadGuys,
                GoodGuys,

                Houses,

                Respawns,

                Lifetimes,

                AbilityManagers,
                Repellers,
                RadiationClounds,
                MiniRockets,

                Targets,
                ProjectileSources,
                TargetProjectiles,
                PositionProjectiles,
                ProjectileDebrises,

                Health,
                Defence,                                

                LinearMovement,
                MovementToPoint,

                Positioning,

                Animation,
                Flipping,
                RenderOrderByY,

                HealthBars
            };

#if UNITY_EDITOR
            var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.Static);
            if (fields.Length != gameComponentsContainers.Count)
            {
                throw new UnityException("Count of declared components is not equal to count of utilized components");
            }
#endif

            return gameComponentsContainers;
        }
    }
}
