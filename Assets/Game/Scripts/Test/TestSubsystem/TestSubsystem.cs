﻿namespace Brutime.LD42
{
    using BruCore;
    using UnityEngine;

    public class TestSubsystem : Subsystem
    {
        private Vector3 playerPosition;

        private int playerId;
        private int enemyId;

        public override void Start()
        {
            base.Start();

            //playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
            //playerPosition += new Vector3(1.3f, 0.8f, 0.0f);

            //playerId = GameObject.FindGameObjectWithTag("Player").GetInstanceID();
            //enemyId = GameObject.FindGameObjectWithTag("Enemy").GetInstanceID();
        }

        public override void Update()
        {
            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    var createTargetProjectileMessage = new CreateTargetProjectileMessage
            //    {
            //        OwnerEntityId = playerId,
            //        OwnerTeam = Team.Player,
            //        Position = playerPosition,
            //        SpellId = 1000,
            //        TargetEntityId = enemyId
            //    };

            //    MessageBus.AddCreateTargetProjectileMessage(createTargetProjectileMessage);
            //}

            if (Input.GetKeyDown(KeyCode.Space))
            {
                var damageHealthMessage = new DamageHealthMessage
                {
                    EntityId = enemyId,
                    DamageValue = 5,
                    IsCritical = false
                };

                MessageBus.DamageHealthMessages.Publish(damageHealthMessage);
            }
        }
    }
}