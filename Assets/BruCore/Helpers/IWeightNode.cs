﻿namespace Brutime.BruCore
{
    public interface IWeightNode
    {
        float GetWeight();
    }
}