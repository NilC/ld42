﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public struct Segment
    {
        public Segment(Vector2 start, Vector2 end)
        {
            Start = start;
            End = end;
        }

        public Vector2 Start;
        public Vector2 End;
    }
}