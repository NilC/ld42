﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public static class MathHelper
    {
        public static int GetWeightedRandomIndex(float[] weights)
        {
            float sumOfWeights = 0.0f;
            for (int i = 0; i < weights.Length; i++)
            {
                sumOfWeights += weights[i];
            }

            float random = Random.Range(0.0f, sumOfWeights);
            for (int i = 0; i < weights.Length; i++)
            {
                if (random < weights[i])
                {
                    return i;
                }
                random -= weights[i];
            }

            throw new UnityException("Should never get here");
        }

        public static int GetWeightedRandomIndex(IWeightNode[] weightNodes)
        {
            float sumOfWeights = 0.0f;
            for (int i = 0; i < weightNodes.Length; i++)
            {
                sumOfWeights += weightNodes[i].GetWeight();
            }

            float random = Random.Range(0.0f, sumOfWeights);
            for (int i = 0; i < weightNodes.Length; i++)
            {
                if (random < weightNodes[i].GetWeight())
                {
                    return i;
                }
                random -= weightNodes[i].GetWeight();
            }

            throw new UnityException("Should never get here");
        }

        public static void ReverseXScale(Transform transform)
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }

        public static bool RandomFlag()
        {
            return (Random.Range(0, 100) < 50);
        }

        public static bool Approximately(float a, float b, float tolerance)
        {
            return (Mathf.Abs(a - b) < tolerance);
        }

        public static bool AreLineSegmentsIntersect(Segment firstSegment, Segment secondSegment)
        {
            return AreLineSegmentsIntersect(firstSegment.Start, firstSegment.End, secondSegment.Start, secondSegment.End);
        }

        public static bool AreLineSegmentsIntersect(Vector2 firstPointStart, Vector2 firstPointEnd, Vector2 secondPointStart, Vector2 secondPointEnd)
        {
            Vector2 a = firstPointEnd - firstPointStart;
            Vector2 b = secondPointStart - secondPointEnd;
            Vector2 c = firstPointStart - secondPointStart;

            float alphaNumerator = b.y * c.x - b.x * c.y;
            float alphaDenominator = a.y * b.x - a.x * b.y;
            float betaNumerator = a.x * c.y - a.y * c.x;
            float betaDenominator = alphaDenominator;

            bool doIntersect = true;

            if (Mathf.Approximately(alphaDenominator, 0.0f) || Mathf.Approximately(betaDenominator, 0))
            {
                doIntersect = false;
            }
            else
            {
                if (alphaDenominator > 0)
                {
                    if (alphaNumerator < 0 || alphaNumerator > alphaDenominator)
                    {
                        doIntersect = false;
                    }
                }
                else if (alphaNumerator > 0 || alphaNumerator < alphaDenominator)
                {
                    doIntersect = false;
                }

                if (doIntersect && betaDenominator > 0)
                {
                    if (betaNumerator < 0 || betaNumerator > betaDenominator)
                    {
                        doIntersect = false;
                    }
                }
                else if (betaNumerator > 0 || betaNumerator < betaDenominator)
                {
                    doIntersect = false;
                }
            }

            return doIntersect;
        }

    }
}
