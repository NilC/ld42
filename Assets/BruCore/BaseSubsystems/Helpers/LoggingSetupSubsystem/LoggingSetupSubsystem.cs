﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class LoggingSetupSubsystem : Subsystem
    {
        public override void Start()
        {
            Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        }
    }
}
