﻿namespace Brutime.BruCore
{
    using UnityEngine;

    [ExecuteInEditMode]
    public class BeginUpdateSubsystem : Subsystem
    {
        public override void Update()
        {
#if UNITY_EDITOR
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                CoreMessageBus.StartFrame();
            }
#endif
        }
    }
}
