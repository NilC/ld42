﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Linq;
    using UnityEditor;
#endif

    [ExecuteInEditMode]
    public class EntitiesSubsystem : Subsystem
    {
        private const int EntitiesCapacity = 1000;

        private int currentEntityId = 0;
        private Dictionary<int, int> entitiesIdsMap = new Dictionary<int, int>();

#if UNITY_EDITOR
        private Dictionary<int, GizmosDrawer[]> gizmosDrawers = new Dictionary<int, GizmosDrawer[]>();
        private Dictionary<int, GizmoPropertiesProvider[]> gizmoPropertiesProviders = new Dictionary<int, GizmoPropertiesProvider[]>();
        private GizmoPropertiesDrawer gizmoPropertiesDrawer = new GizmoPropertiesDrawer();
#endif

        public EntitiesSubsystem()
        {
            entitiesIdsMap = new Dictionary<int, int>(EntitiesCapacity);

#if UNITY_EDITOR
            gizmosDrawers = new Dictionary<int, GizmosDrawer[]>(EntitiesCapacity);
            gizmoPropertiesProviders = new Dictionary<int, GizmoPropertiesProvider[]>(EntitiesCapacity);
#endif
        }

        public override void Start()
        {
#if UNITY_EDITOR
            CheckComponentsPerSubsystemUniqueness();
            //CheckComponentAttributesPerSubsystemUniqueness();
#endif
        }

        public int Add(EntityComponentDataProvider entityComponentDataProvider, AddingType addingType)
        {
            var entityId = CreateEntityId(entityComponentDataProvider.gameObject.GetEntityId());
            CoreComponents.Entities.CreateNewComponentAndAddWithCast(entityId, entityComponentDataProvider);
            var entityComponent = CoreComponents.Entities[entityId];
            entityComponent.ComponentsContainers.Add(CoreComponents.Entities);

            var entitySpecification = entityComponentDataProvider.EntitySpecification;

            if (entitySpecification == null || entitySpecification.ComponentDataProviders == null)
            {
                throw new UnityException(string.Format("Entity Specification of game object \"{0}\" is null or have no component data prodivers", entityComponentDataProvider.gameObject.name));
            }

            foreach (var componentDataProvider in entitySpecification.ComponentDataProviders)
            {
                foreach (var componentsContainer in CoreComponents.AllComponentsContainers)
                {
                    if (componentDataProvider.GetComponentType() == componentsContainer.GetComponentType())
                    {
#if UNITY_EDITOR
                        if ((componentsContainer.IsExecutingInEditMode() || Application.isPlaying))
#endif
                        {
                            componentsContainer.CreateNewComponentAndAddWithCast(entityId, componentDataProvider);
                            entityComponent.ComponentsContainers.Add(componentsContainer);
                        }
                    }
                }
            }

            if (addingType == AddingType.WithInitialization)
            {
                InitializeComponent(entityComponent);
            }

            CoreComponents.Entities[entityId] = entityComponent;            

#if UNITY_EDITOR
            gizmosDrawers.Add(entityId, GetGizmosDrawers(entityComponentDataProvider));
            gizmoPropertiesProviders.Add(entityId, GetGizmoPropertiesProviders(entityComponentDataProvider));

            CheckCustomEntiesIdsUniqueness();
#endif
            return entityId;
        }

        public void InitializeComponent(EntityComponent entityComponent)
        {
            for (var i = 0; i < entityComponent.ComponentsContainers.Count; i++)
            {
                var componentsContainers = entityComponent.ComponentsContainers[i];
                componentsContainers.InitializeComponent(entityComponent.EntityId);
            }
        }

        public void RemoveComponent(int entityId)
        {
#if UNITY_EDITOR
            gizmosDrawers.Remove(entityId);
            gizmoPropertiesProviders.Remove(entityId);
#endif

            var componentsContainers = CoreComponents.Entities[entityId].ComponentsContainers;

            for (var i = 0; i < componentsContainers.Count; i++)
            {
                componentsContainers[i].RemoveComponent(entityId);
            }
        }

        public int PopComponentFromPool(int entityId)
        {
            var cachedComponent = CoreComponents.Entities.GetCachedComponent(entityId);
            var newEntityId = CreateEntityId(cachedComponent.GameObject.GetEntityId());

            for (var i = 0; i < cachedComponent.ComponentsContainers.Count; i++)
            {
                cachedComponent.ComponentsContainers[i].PopComponentFromPool(entityId, newEntityId);
            }

            for (var i = 0; i < cachedComponent.ComponentsContainers.Count; i++)
            {
                cachedComponent.ComponentsContainers[i].ResetComponent(newEntityId);
            }

#if UNITY_EDITOR
            gizmosDrawers.Add(newEntityId, GetGizmosDrawers(CoreComponents.Entities[newEntityId].GameObject.GetComponent<EntityComponentDataProvider>()));
            gizmoPropertiesProviders.Add(newEntityId, GetGizmoPropertiesProviders(CoreComponents.Entities[newEntityId].GameObject.GetComponent<EntityComponentDataProvider>()));

            //CheckCustomEntiesIdsUniqueness();
#endif

            return newEntityId;
        }

        public void PushComponentToPool(int entityId)
        {
#if UNITY_EDITOR
            gizmosDrawers.Remove(entityId);
            gizmoPropertiesProviders.Remove(entityId);
#endif

            var componentsContainers = CoreComponents.Entities[entityId].ComponentsContainers;

            for (var i = 0; i < componentsContainers.Count; i++)
            {
                componentsContainers[i].PushComponentToPool(entityId);
            }
        }

        public EntityComponent Get(int entityId)
        {
            return new EntityComponent();
        }

        public Dictionary<int, int> GetEntitiesIdsMap()
        {
            return entitiesIdsMap;
        }

        public int GetEntityIdByGameObjectEntityId(int gameObjectEntityId)
        {
            return entitiesIdsMap[gameObjectEntityId];
        }

        private int CreateEntityId(int gameObjectEntityId)
        {
            var entityId = GetNextEntityId();
            entitiesIdsMap[gameObjectEntityId] = entityId;
            return entityId;
        }

        private EntityComponent UpdateEntityId(EntityComponent entityComponent)
        {
            entityComponent.EntityId = GetNextEntityId();
            entitiesIdsMap[entityComponent.GameObject.GetEntityId()] = entityComponent.EntityId;
            return entityComponent;
        }

        private int GetNextEntityId()
        {
            currentEntityId++;
            return currentEntityId;
        }

#if UNITY_EDITOR
        private void CheckComponentsPerSubsystemUniqueness()
        {
            var componentsContainersWithDuplicatedComponents = CoreComponents
                .AllComponentsContainers
                .GroupBy(y => y.GetComponentType())
                .Where(x => x.Count() > 1)
                .ToList();

            if (componentsContainersWithDuplicatedComponents.Count > 0)
            {
                throw new UnityException(string.Format("Components duplicated in components containers: \"{0}\"",
                    string.Join(", ", componentsContainersWithDuplicatedComponents.SelectMany(x => x.Select(y => y.GetType().Name)).ToArray())));
            }
        }

        //private void CheckComponentAttributesPerSubsystemUniqueness()
        //{
        //    var subsystemsWithDuplicatedAttributes = CoreSubsystems
        //        .AllComponentsSubsystems
        //        .GroupBy(y => y.GetComponentAttributesType())
        //        .Where(x => x.Count() > 1)
        //        .ToList();

        //    if (subsystemsWithDuplicatedAttributes.Count > 0)
        //    {
        //        throw new UnityException(string.Format("Component attributes duplicated in subsystems: \"{0}\"",
        //            string.Join(", ", subsystemsWithDuplicatedAttributes.SelectMany(x => x.Select(y => y.GetType().Name)).ToArray())));
        //    }
        //}

        private void CheckCustomEntiesIdsUniqueness()
        {
            var customEntitiesIds = new Dictionary<int, string>();

            foreach (var entityId in CoreComponents.Entities.GetEntitiesIds())
            {
                var component = CoreComponents.Entities[entityId];
                var customEntityId = component.CustomEntityId;
                if (customEntityId != 0)
                {
                    if (customEntitiesIds.ContainsKey(customEntityId))
                    {
                        var existingComponentName = customEntitiesIds[customEntityId];

                        var errorMessage = string.Format(
                            "Not unique custom entities ids. Id: {0}. Names: \"{1}\", \"{2}\"",
                            component.CustomEntityId, existingComponentName, component.Name);

                        throw new UnityException(errorMessage);
                    }
                    customEntitiesIds.Add(customEntityId, component.Name);
                }
            }
        }

        public override void OnDrawGizmos()
        {
            foreach (var gizmosDrawerPair in gizmosDrawers)
            {
                for (var i = 0; i < gizmosDrawerPair.Value.Length; i++)
                {
                    var entityId = gizmosDrawerPair.Key;
                    var gizmosDrawer = gizmosDrawerPair.Value[i];
                    gizmosDrawer.Draw(entityId);
                }
            }

            foreach (var gizmoPropertiesProviderPair in gizmoPropertiesProviders)
            {
                var entityId = gizmoPropertiesProviderPair.Key;
                if (Selection.activeGameObject == CoreComponents.Entities[entityId].GameObject)
                {
                    gizmoPropertiesDrawer.Draw(entityId, gizmoPropertiesProviderPair.Value);
                }
            }
        }

        private GizmosDrawer[] GetGizmosDrawers(EntityComponentDataProvider entityComponentDataProvider)
        {
            var gizmosProviders = entityComponentDataProvider.GetComponents<IGizmosProvider>();

            var gizmosDrawers = new List<GizmosDrawer>();

            foreach (var gizmosProvider in gizmosProviders)
            {
                gizmosDrawers.AddRange(gizmosProvider.GetGizmosDrawers());
            }

            var entitySpecification = entityComponentDataProvider.EntitySpecification;
            if (entitySpecification != null && entitySpecification.ComponentDataProviders != null)
            {
                foreach (var componentDataProvider in entitySpecification.ComponentDataProviders)
                {
                    gizmosDrawers.AddRange(componentDataProvider.GetGizmosDrawers());
                }
            }

            return gizmosDrawers.ToArray();
        }

        private GizmoPropertiesProvider[] GetGizmoPropertiesProviders(EntityComponentDataProvider entityComponentDataProvider)
        {
            var gizmosProviders = entityComponentDataProvider.GetComponents<IGizmosProvider>();

            var gizmoPropertiesProviders = new List<GizmoPropertiesProvider>();

            foreach (var gizmosProvider in gizmosProviders)
            {
                gizmoPropertiesProviders.AddRange(gizmosProvider.GetGizmoPropertiesProviders());
            }

            var entitySpecification = entityComponentDataProvider.EntitySpecification;
            if (entitySpecification != null && entitySpecification.ComponentDataProviders != null)
            {
                foreach (var componentDataProvider in entitySpecification.ComponentDataProviders)
                {
                    gizmoPropertiesProviders.AddRange(componentDataProvider.GetGizmoPropertiesProviders());
                }
            }

            return gizmoPropertiesProviders.ToArray();
        }
#endif
    }
}