﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class EntitySpecification : ScriptableObject
    {
        public ComponentDataProvider[] ComponentDataProviders = new ComponentDataProvider[0];

        ////Use only in initializating code and editors
        //public T GetAttribute<T>() where T : ComponentDataProvider
        //{
        //    for (var i = 0; i < ComponentAttributes.Length; i++)
        //    {
        //        if (ComponentAttributes[i].GetType() == typeof (T))
        //        {
        //            return (T)ComponentAttributes[i];
        //        }
        //    }

        //    return null;
        //}
    }
}