﻿namespace Brutime.BruCore
{
    public enum AddingType
    {
        WithInitialization,
        WithoutInitialization
    }
}