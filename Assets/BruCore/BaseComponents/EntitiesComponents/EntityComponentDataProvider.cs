﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

#if UNITY_EDITOR
#endif

    [ExecuteInEditMode]
    public class EntityComponentDataProvider : ExternalComponentDataProvider
    {
        public int EntityDatabaseId;
        public int CustomEntityId;
        public bool MustBeInPool;
        public EntitySpecification EntitySpecification;

        public EntityComponent GetComponent()
        {
            var component = new EntityComponent
            {
                EntityDatabaseId = EntityDatabaseId,
                CustomEntityId = CustomEntityId,
                GameObject = gameObject,
                IsInPool = false,
                Pool = null,
                ComponentsContainers = new List<IComponentsContainer>(30),

#if UNITY_EDITOR
                Name = name
#endif
            };

            return component;
        }

#if UNITY_EDITOR
        public override GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[] { new PlaceHolderGizmosDrawer() };
        }

        public override GizmoPropertiesProvider[] GetGizmoPropertiesProviders()
        {
            return new GizmoPropertiesProvider[] { new IdsGizmoPropertiesProvider() };
        }
#endif
    }
}

