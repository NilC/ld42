﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public struct EntityComponent
    {
        public int EntityId;
        public int EntityDatabaseId;
        public int CustomEntityId;
        public GameObject GameObject;
        public bool IsInPool;
        public Pool Pool;
        public List<IComponentsContainer> ComponentsContainers;

#if UNITY_EDITOR
        public string Name;
#endif
    }
}

