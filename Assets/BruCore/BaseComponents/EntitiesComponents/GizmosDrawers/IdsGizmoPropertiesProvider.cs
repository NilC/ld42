namespace Brutime.BruCore
{
    using System.Collections.Generic;

    public class IdsGizmoPropertiesProvider : GizmoPropertiesProvider
    {
        public override List<GizmoProperty> GetProperties(int entityId)
        {
            return new List<GizmoProperty>
            {
                new GizmoProperty("Id", entityId.ToString()),
                new GizmoProperty("Custom Id", CoreComponents.Entities[entityId].CustomEntityId.ToString())
            };
        }
    }
}