namespace Brutime.BruCore
{
    using UnityEngine;

    public class PlaceHolderGizmosDrawer : GizmosDrawer
    {
        public override void Draw(int entityId)
        {
            var transform = CoreComponents.Entities[entityId].GameObject.transform;

            Gizmos.color = new Color(0.945f, 0.769f, 0.059f);
            Gizmos.DrawLine(
                transform.position + new Vector3(-0.25f, 0.0f, 0.0f),
                transform.position + new Vector3(0.25f, 0.0f, 0.0f));

            Gizmos.DrawLine(
                transform.position + new Vector3(0.0f, 0.0f, 0.0f),
                transform.position + new Vector3(0.0f, 0.5f, 0.0f));
        }
    }
}