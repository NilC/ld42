﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public class EntitiesComponentsContainer : ComponentsContainer<EntityComponent, EntityComponentDataProvider>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, EntityComponentDataProvider componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            component.EntityId = entityId;
            AddComponent(entityId, component);
        }

        public override void PopComponentFromPool(int currentEntityId, int newEntityId)
        {
            base.PopComponentFromPool(currentEntityId, newEntityId);
            var component = Components[newEntityId];
            component.EntityId = newEntityId;
            Components[newEntityId] = component;
        }

        public int GetEntityIdByCustomEntityId(int customEntityId)
        {
            foreach (var entityId in EntitiesIds)
            {
                if (Components[entityId].CustomEntityId == customEntityId)
                {
                    return entityId;
                }
            }

            throw new UnityException(string.Format("No entity with custom entity id: {0}", customEntityId));
        }

        public EntityComponent GetCachedComponent(int entityId)
        {
            return CachedComponents[entityId];
        }
    }
}

