﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    [ExecuteInEditMode]
    public abstract class CoreSubsystems : Controller
    {
        public static CoreSubsystems Instance;

        public static EntitiesSubsystem Entities;

        public static List<ISubsystem> AllSubsystems = new List<ISubsystem>(50);

        protected virtual void Awake()
        {
            Instance = this;

            AllSubsystems.Clear();

            AllSubsystems.Add(new BeginUpdateSubsystem());
            AllSubsystems.Add(new LoggingSetupSubsystem());

            Entities = new EntitiesSubsystem();
            AllSubsystems.Add(Entities);

            var gameSubsystems = GetGameSubsystems();
            AllSubsystems.AddRange(gameSubsystems);

            AllSubsystems.Add(new EndUpdateSubsystem());
        }

        private void Start()
        {
            for (var i = 0; i < AllSubsystems.Count; i++)
            {
#if UNITY_EDITOR
                if (AllSubsystems[i].IsEnabled && ((AllSubsystems[i].IsExecutingInEditMode() || Application.isPlaying)))
#else                    
                if (AllSubsystems[i].IsEnabled)
#endif
                {
                    AllSubsystems[i].Start();
                }
            }
        }

        private void Update()
        {
            for (var i = 0; i < AllSubsystems.Count; i++)
            {
#if UNITY_EDITOR
                if (AllSubsystems[i].IsEnabled && ((AllSubsystems[i].IsExecutingInEditMode() || Application.isPlaying)))
#else                    
                if (AllSubsystems[i].IsEnabled)
#endif
                {
                    AllSubsystems[i].Update();
                }
            }
        }

        private void FixedUpdate()
        {
            for (var i = 0; i < AllSubsystems.Count; i++)
            {
#if UNITY_EDITOR
                if (AllSubsystems[i].IsEnabled && ((AllSubsystems[i].IsExecutingInEditMode() || Application.isPlaying)))
#else                    
                if (AllSubsystems[i].IsEnabled)
#endif
                {
                    AllSubsystems[i].FixedUpdate();
                }
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            for (var i = 0; i < AllSubsystems.Count; i++)
            {

                if (AllSubsystems[i].IsEnabled && ((AllSubsystems[i].IsExecutingInEditMode() || Application.isPlaying)))
                {
                    AllSubsystems[i].OnDrawGizmos();
                }
            }
        }
#endif

        protected abstract List<ISubsystem> GetGameSubsystems();
    }
}
