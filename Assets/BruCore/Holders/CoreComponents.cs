﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    [ExecuteInEditMode]
    public abstract class CoreComponents : Controller
    {
        public static EntitiesComponentsContainer Entities = new EntitiesComponentsContainer();

        public static List<IComponentsContainer> AllComponentsContainers = new List<IComponentsContainer>(50);

        protected virtual void Awake()
        {
            for (var i = 0; i < AllComponentsContainers.Count; i++)
            {
                AllComponentsContainers[i].Clear();
            }

            AllComponentsContainers.Clear();

            AllComponentsContainers.Add(Entities);

            var gameComponentsContainers = GetGameComponentsContainers();
            AllComponentsContainers.AddRange(gameComponentsContainers);
        }

        protected abstract List<IComponentsContainer> GetGameComponentsContainers();
    }
}
