﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System;
    using UnityEditor;
    using UnityEngine;

    public static class CustomEditorGUILayout
    {
        public static int[] ArrayField(string label, int[] array)
        {
            return ArrayField(label, array, (index) => 
            {
                return index < array.Length ? (int)(object)(EditorGUILayout.IntField(array[index])) : default(int);
            });
        }

        public static float[] ArrayField(string label, float[] array)
        {
            return ArrayField(label, array, (index) => 
            {
                return index < array.Length ? (float)(object)(EditorGUILayout.FloatField(array[index])) : default(float);
            });
        }

        public static Vector3[] ArrayField(string label, Vector3[] array)
        {
            return ArrayField(label, array, (index) => 
            {
                return index < array.Length ? (Vector3)(object)(EditorGUILayout.Vector3Field(string.Empty, array[index])) : default(Vector3);
            });
        }

        public static T[] ArrayField<T>(string label, T[] array, Func<int, T> getArrayValue) where T : new()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel, GUILayout.MinWidth(50));
            if (GUILayout.Button("+", GUILayout.Width(88)))
            {
                ArrayUtility.Add(ref array, new T());
            }
            EditorGUILayout.EndHorizontal();
            if (array == null)
            {
                array = new T[0];
            }
            for (var i = 0; i < array.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                array[i] = getArrayValue(i);
                //if (array[i] is int)
                //{
                //    array[i] = (T)(object)(EditorGUILayout.IntField(Convert.ToInt32(array[i])));
                //}

                //if (array[i] is float)
                //{
                //    array[i] = (T)(object)(EditorGUILayout.FloatField(Convert.ToSingle(array[i])));
                //}

                if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                {
                    ArrayHelper.SwapElements(ref array, i, i - 1);
                }

                if (GUILayout.Button("↓", GUILayout.Width(30)) && i < array.Length - 1)
                {
                    ArrayHelper.SwapElements(ref array, i, i + 1);
                }

                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    ArrayUtility.RemoveAt(ref array, i);
                    EditorGUILayout.EndHorizontal();
                    continue;
                }

                EditorGUILayout.EndHorizontal();
            }

            return array;
        }
    }
}

#endif
