﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public static class DatabaseSelector
    {
        public static DatabaseEntitiesSelection GetEntities(string nameFilter)
        {
            var entitiesDatabase = Resources
                .FindObjectsOfTypeAll<EntitiesDatabase>()
                .First();

            var entitiesRecords = entitiesDatabase
                .EntityRecordGroups
                .Where(x => x.Name.Contains(nameFilter))
                .SelectMany(x => x.EntitiesRecords)
                .ToArray();

            var selection = new DatabaseEntitiesSelection
            {
                Ids = new int[entitiesRecords.Length],
                Names = new string[entitiesRecords.Length],
                Icons = new Dictionary<int, Texture2D>(entitiesRecords.Length),
                Entities = new Dictionary<int, GameObject>(entitiesRecords.Length)
            };

            for (var i = 0; i < entitiesRecords.Length; i++)
            {
                var entityRecord = entitiesRecords[i];
                selection.Ids[i] = entityRecord.EntityDatabaseId;
                selection.Names[i] = string.Format("{0} {1}", entityRecord.EntityDatabaseId, entityRecord.GameObject.name);
                selection.Icons[entityRecord.EntityDatabaseId] = AssetPreview.GetAssetPreview(entitiesRecords[i].GameObject);
                selection.Entities[entityRecord.EntityDatabaseId] = entityRecord.GameObject;
            }

            return selection;
        }

        public static DatabaseScriptableObjectsSelection GetScriptableObjects(string nameFilter)
        {
            var scriptableObjectsDatabase = Resources
                .FindObjectsOfTypeAll<ScriptableObjectsDatabase>()
                .Where(x => x.name == nameFilter)
                .First();

            var scriptableObjectsRecords = scriptableObjectsDatabase
                .ScriptableObjectRecordGroups
                .SelectMany(x => x.ScriptableObjectsRecords)
                .ToArray();

            var selection = new DatabaseScriptableObjectsSelection
            {
                Ids = new int[scriptableObjectsRecords.Length],
                Names = new string[scriptableObjectsRecords.Length],
                ScriptableObjects = new Dictionary<int, ScriptableObject>(scriptableObjectsRecords.Length)
            };

            for (var i = 0; i < scriptableObjectsRecords.Length; i++)
            {
                var scriptableObjectRecord = scriptableObjectsRecords[i];
                selection.Ids[i] = scriptableObjectRecord.Id;
                selection.Names[i] = string.Format("{0} {1}", scriptableObjectRecord.Id, scriptableObjectRecord.ScriptableObject.name);
                selection.ScriptableObjects[scriptableObjectRecord.Id] = scriptableObjectRecord.ScriptableObject;
            }

            return selection;
        }
    }
}

#endif
