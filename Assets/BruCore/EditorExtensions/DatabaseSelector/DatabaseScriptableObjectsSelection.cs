﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public class DatabaseScriptableObjectsSelection
    {
        public int[] Ids;
        public string[] Names;
        public Dictionary<int, ScriptableObject> ScriptableObjects;
    }
}

#endif
