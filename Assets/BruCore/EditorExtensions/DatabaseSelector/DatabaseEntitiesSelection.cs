﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public class DatabaseEntitiesSelection
    {
        public int[] Ids;
        public string[] Names;    
        public Dictionary<int, Texture2D> Icons;
        public Dictionary<int, GameObject> Entities;
    }
}

#endif
