﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class CustomInspectorEditor : Editor
    {
        protected void DrawScriptFieldFromScriptableObject()
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((ScriptableObject)target), typeof(ScriptableObject), false);
            GUI.enabled = true;
        }

        protected void DrawScriptFieldFromMonoBehaviour()
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((MonoBehaviour)target), typeof(MonoBehaviour), false);
            GUI.enabled = true;
        }

        protected void MarkForSaveIfChanged()
        {
            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
                //Debug.Log("SetDirty");
            }
        }

        protected void MarkForSaveIfChanged(List<GameObject> gameObjects)
        {
            if (GUI.changed)
            {
                MarkForSave(gameObjects);
            }
        }

        protected void MarkForSave(List<GameObject> gameObjects)
        {
            if (gameObjects.Count == 0)
            {
                return;
            }
            
            EditorUtility.SetDirty(target);

            foreach (var gameObject in gameObjects)
            {
                EditorUtility.SetDirty(gameObject);
            }
        }

        protected void SaveAndRefreshAssets()
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}

#endif