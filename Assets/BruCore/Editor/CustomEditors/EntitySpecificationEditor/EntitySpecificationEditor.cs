﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using EditorUtils;
    using System;
    using System.Linq;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(EntitySpecification))]
    public class EntitySpecificationEditor : CustomInspectorEditor
    {
        private string[] requiredDataProviderTypeNames = new string[0];
        private string[] dataProviderTypeNames = new string[0];
        private int dataProviderTypeIndex = -1;

        private void OnEnable()
        {
            UpdateRequiredDataProvidersTypeNames();
            UpdateDataProviderTypeNames();

            var specification = target as EntitySpecification;

            for (var i = 0; i < specification.ComponentDataProviders.Length; i++)
            {
                if (specification.ComponentDataProviders[i].IsUsingSerializedObject())
                {
                    var serializedDataProvider = new SerializedObject(specification.ComponentDataProviders[i]);
                    specification.ComponentDataProviders[i].InitializeLayout(serializedDataProvider);
                }
                else
                {
                    specification.ComponentDataProviders[i].InitializeLayout();
                }
            }
        }

        public override void OnInspectorGUI()
        {
            var specification = target as EntitySpecification;

            foreach (var requiredDataProviderTypeName in requiredDataProviderTypeNames)
            {
                if (specification.ComponentDataProviders.SingleOrDefault(x => x.GetType().Name == requiredDataProviderTypeName) == null)
                {
                    dataProviderTypeIndex = Array.IndexOf(dataProviderTypeNames, requiredDataProviderTypeName);
                    CreateDataProvider(specification, dataProviderTypeIndex);
                    UpdateDataProviderTypeNames();
                }
            }

            var indexToDelete = -1;
            for (var i = 0; i < specification.ComponentDataProviders.Length; i++)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);

                if (specification.ComponentDataProviders[i] != null)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(specification.ComponentDataProviders[i].GetCaption(), EditorStyles.boldLabel);
                    if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                    {
                        ArrayHelper.SwapElements(ref specification.ComponentDataProviders, i, i - 1);
                    }
                    if (GUILayout.Button("↓", GUILayout.Width(30)) && i < specification.ComponentDataProviders.Length - 1)
                    {
                        ArrayHelper.SwapElements(ref specification.ComponentDataProviders, i, i + 1);
                    }

                    if (!(specification.ComponentDataProviders[i].IsRequired()) && GUILayout.Button("X", GUILayout.Width(20)))
                    {
                        indexToDelete = i;
                    }
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.Space();

                    if (specification.ComponentDataProviders[i].IsUsingSerializedObject())
                    {
                        var serializedDataProvider = new SerializedObject(specification.ComponentDataProviders[i]);
                        serializedDataProvider.Update();
                        specification.ComponentDataProviders[i].DrawLayout(serializedDataProvider);
                        serializedDataProvider.ApplyModifiedProperties();
                    }
                    else
                    {
                        specification.ComponentDataProviders[i].DrawLayout();
                    }

                    EditorGUILayout.Space();
                }

                EditorGUILayout.EndVertical();
            }

            if (indexToDelete > -1)
            {
                DestroyImmediate(specification.ComponentDataProviders[indexToDelete], true);
                ArrayUtility.RemoveAt(ref specification.ComponentDataProviders, indexToDelete);
                SaveAndRefreshAssets();

                UpdateDataProviderTypeNames();
            }

            EditorGUILayout.BeginHorizontal();
            dataProviderTypeIndex = EditorGUILayout.Popup(dataProviderTypeIndex, dataProviderTypeNames);
            if (dataProviderTypeIndex != -1)
            {
                CreateDataProvider(specification, dataProviderTypeIndex);
                UpdateDataProviderTypeNames();
            }

            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Reorder Data Providers"))
            {
                if (CoreComponents.AllComponentsContainers.Count > 0)
                {
                    foreach (var componentDataProvider in specification.ComponentDataProviders)
                    {
                        if (CoreComponents
                            .AllComponentsContainers
                            .SingleOrDefault(y => y.GetComponentType() == componentDataProvider.GetComponentType()) == null)
                        {
                            throw new Exception(string.Format("Components not found for component data provider {0}", componentDataProvider));
                        }
                    }

                    specification.ComponentDataProviders = specification
                        .ComponentDataProviders
                        .OrderBy(x => x.IsRequired() ? -1 :
                            CoreComponents
                                .AllComponentsContainers
                                .IndexOf(CoreComponents
                                    .AllComponentsContainers
                                    .Single(y => y.GetComponentType() == x.GetComponentType())))
                        .ToArray();
                }
            }

            if (CoreSubsystems.AllSubsystems.Count == 0)
            {
                EditorGUILayout.LabelField("Subsystems not initialized", EditorStyles.boldLabel);
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(specification);
            }
        }

        private void CreateDataProvider(EntitySpecification specification, int currentDataProviderTypeIndex)
        {
            var newDataProvider = CreateInstance(dataProviderTypeNames[currentDataProviderTypeIndex]) as ComponentDataProvider;
            newDataProvider.name = dataProviderTypeNames[currentDataProviderTypeIndex];
            newDataProvider.ApplyDefaultValues();
            AssetDatabase.AddObjectToAsset(newDataProvider, specification);
            SaveAndRefreshAssets();
            ArrayUtility.Add(ref specification.ComponentDataProviders, newDataProvider);

            if (newDataProvider.IsUsingSerializedObject())
            {
                var serializedDataProvider = new SerializedObject(newDataProvider);
                newDataProvider.InitializeLayout(serializedDataProvider);
            }
            else
            {
                newDataProvider.InitializeLayout();
            }
        }

        private void UpdateRequiredDataProvidersTypeNames()
        {
            Type[] types = Assembly.GetAssembly(typeof(ComponentDataProvider)).GetTypes();

            requiredDataProviderTypeNames = types
                .Where(x => x.IsSubclassOf(typeof(ComponentDataProvider)) && !x.IsAbstract)
                .Select(x => CreateInstance(x.Name))
                .Where(x => ((ComponentDataProvider)x).IsRequired())
                .Select(x => ScriptableObjectUtility.GetNameForUi(x.GetType().FullName))
                .ToArray();
        }

        private void UpdateDataProviderTypeNames()
        {
            Type[] types = Assembly.GetAssembly(typeof(ComponentDataProvider)).GetTypes();

            dataProviderTypeNames = types
                .Where(x => x.IsSubclassOf(typeof(ComponentDataProvider)) && !x.IsAbstract)
                .Select(x => ScriptableObjectUtility.GetNameForUi(x.FullName))
                .ToArray();

            var specification = target as EntitySpecification;

            var alreadyAddedDataProviderTypeNames = specification
                .ComponentDataProviders
                .Select(x => x.GetType().Name);

            dataProviderTypeNames = dataProviderTypeNames
                .Except(alreadyAddedDataProviderTypeNames)
                .ToArray();

            dataProviderTypeIndex = -1;
        }
    }
}

#endif