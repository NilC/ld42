﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using BruCore;
    using System.Collections;
    using System.Linq;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [ExecuteInEditMode]
    [CustomEditor(typeof(EntityComponentDataProvider), true)]
    public class EntityComponentDataProviderEditor : CustomInspectorEditor
    {
        private bool showProperties = true;
        private bool viewAttributes = false;

        public override void OnInspectorGUI()
        {
            DrawScriptFieldFromMonoBehaviour();

            serializedObject.Update();

            DrawLayout();

            serializedObject.ApplyModifiedProperties();

            Repaint();
        }

        private void DrawLayout()
        {
            EditorGUILayout.BeginHorizontal();
            {
                var entityDatabaseIdProperty = serializedObject.FindProperty("EntityDatabaseId");

                GUI.enabled = false;
                EditorGUILayout.PropertyField(entityDatabaseIdProperty);
                GUI.enabled = true;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                var customEntityIdProperty = serializedObject.FindProperty("CustomEntityId");
                EditorGUILayout.PropertyField(customEntityIdProperty);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                var mustBeInPoolProperty = serializedObject.FindProperty("MustBeInPool");
                EditorGUILayout.PropertyField(mustBeInPoolProperty);
            }
            EditorGUILayout.EndHorizontal();

            var entitySpecificationProperty = serializedObject.FindProperty("EntitySpecification");

            EditorGUILayout.PropertyField(entitySpecificationProperty);

            showProperties = EditorGUILayout.Foldout(showProperties, "Properties");

            if (showProperties)
            {
                //    viewAttributes = EditorGUILayout.Toggle("View Attributes", viewAttributes);

                DrawComponentProperties();
            }
        }

        private void DrawComponentProperties()
        {
            if (CoreComponents.AllComponentsContainers.Count == 0)
            {
                return;
            }

            var entityComponentDataProvider = (EntityComponentDataProvider)target;

            if (!CoreSubsystems.Entities.GetEntitiesIdsMap().ContainsKey(entityComponentDataProvider.gameObject.GetEntityId()))
            {
                return;
            }

            var entityId = CoreSubsystems.Entities.GetEntityIdByGameObjectEntityId(entityComponentDataProvider.gameObject.GetEntityId());

            foreach (var componentsContainer in CoreComponents.AllComponentsContainers)
            {
                var componentsField = componentsContainer.GetType()
                    .GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Single(x => x.Name == "Components");

                var components = componentsField.GetValue(componentsContainer) as IDictionary;

                var cachedComponentsField = componentsContainer.GetType()
                    .GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Single(x => x.Name == "CachedComponents");

                var cachedComponents = cachedComponentsField.GetValue(componentsContainer) as IDictionary;

                var component = components[entityId];

                if (component == null)
                {
                    component = cachedComponents[entityId];
                }

                if (component == null)
                {
                    continue;
                }

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.TextField(component.GetType().Name, EditorStyles.boldLabel);
                }
                EditorGUILayout.EndHorizontal();

                DrawComponentProperties(component);
            }
        }

        private void DrawComponentProperties(object component)
        {
            var fields = component.GetType()
                .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);           

            var componentFields = fields
                .Where(x => !(x.FieldType.IsGenericType && x.FieldType.GetGenericArguments()[0].IsAssignableFrom(typeof(IComponentsContainer))))
                .ToArray();

            DrawComponentsFields(component, componentFields);
        }

        private void DrawComponentsFields(object component, FieldInfo[] componentFields)
        {
            EditorGUI.indentLevel++;
            {
                foreach (var field in componentFields)
                {
                    var value = field.GetValue(component);

                    if (value is IDictionary)
                    {
                        DrawComponentsDictionaryFields(component, field);
                    }
                    else if (value is IEnumerable && !(value is Transform) && !(value is string))
                    {
                        DrawComponentsEnumerableFields(component, field);
                    }
                    else
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            if (value is Vector2)
                            {
                                EditorGUILayout.LabelField(field.Name, ((Vector2)value).ToString("G4"));
                            }
                            else if (value is Vector3)
                            {
                                EditorGUILayout.LabelField(field.Name, ((Vector3)value).ToString("G4"));
                            }
                            else if (value is Vector4)
                            {
                                EditorGUILayout.LabelField(field.Name, ((Vector4)value).ToString("G4"));
                            }
                            else
                            {
                                EditorGUILayout.LabelField(field.Name, value != null ? value.ToString() : "null");
                            }                            
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            EditorGUI.indentLevel--;
        }

        private void DrawComponentsDictionaryFields(object component, FieldInfo field)
        {
            var dictionary = field.GetValue(component) as IDictionary;
            EditorGUILayout.LabelField(field.Name);
            EditorGUI.indentLevel++;
            {
                foreach (var key in dictionary.Keys)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(key.ToString(), dictionary[key].ToString());
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUI.indentLevel--;
        }

        private void DrawComponentsEnumerableFields(object component, FieldInfo field)
        {
            var enumerable = field.GetValue(component) as IEnumerable;
            EditorGUILayout.LabelField(field.Name);
            EditorGUI.indentLevel++;
            {
                foreach (var element in enumerable)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(element.ToString());
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUI.indentLevel--;
        }
    }
}

#endif