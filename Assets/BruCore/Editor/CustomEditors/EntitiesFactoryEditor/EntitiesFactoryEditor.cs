﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(EntitiesFactory), true)]
    public class EntitiesFactoryEditor : CustomInspectorEditor
    {
        private void OnEnable()
        {
            serializedObject.Update();

            UpdateIds();

            serializedObject.ApplyModifiedProperties();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            GUILayout.Space(2.0f);
            DrawScriptFieldFromMonoBehaviour();

            var entitiesDatabaseProperty = serializedObject.FindProperty("entitiesDatabase");
            EditorGUILayout.PropertyField(entitiesDatabaseProperty);

            var database = ((EntitiesDatabase)(entitiesDatabaseProperty.objectReferenceValue));

            if (database == null)
            {
                return;
            }

            var entitiesRecords = database
                .EntityRecordGroups
                .SelectMany(x => x.EntitiesRecords)
                .ToArray();

            var capacitiesRecordsProperty = serializedObject.FindProperty("capacitiesRecords");

            for (var i = 0; i < capacitiesRecordsProperty.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField(entitiesRecords[i].EntityDatabaseId.ToString(), GUILayout.Width(60.0f));
                    GUI.enabled = false;
                    EditorGUILayout.ObjectField(entitiesRecords[i].GameObject, typeof(GameObject), false, GUILayout.Width(120.0f));
                    GUI.enabled = true;

                    EditorGUILayout.LabelField(capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Id").intValue.ToString(), GUILayout.Width(60.0f));
                    capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Capacity").intValue =
                        EditorGUILayout.IntField(capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Capacity").intValue, GUILayout.Width(60.0f));
                }
                EditorGUILayout.EndHorizontal();
            }

            //if (GUILayout.Button("Update"))
            //{
            //    ClearCapacitiesRecords();
            //    UpdateIds();
            //}

            serializedObject.ApplyModifiedProperties();
        }

        private void ClearCapacitiesRecords()
        {
            serializedObject.Update();

            var capacitiesRecordsProperty = serializedObject.FindProperty("capacitiesRecords");
            capacitiesRecordsProperty.ClearArray();

            serializedObject.ApplyModifiedProperties();
        }

        private void UpdateIds()
        {
            var entitiesDatabaseProperty = serializedObject.FindProperty("entitiesDatabase");
            var database = ((EntitiesDatabase)entitiesDatabaseProperty.objectReferenceValue);

            var entitiesRecords = database
                .EntityRecordGroups
                .SelectMany(x => x.EntitiesRecords)
                .ToArray();

            var capacitiesRecordsProperty = serializedObject.FindProperty("capacitiesRecords");

            RemoveObsoleteIds(entitiesRecords, capacitiesRecordsProperty);
            AddNotExistingIds(entitiesRecords, capacitiesRecordsProperty);

            if (capacitiesRecordsProperty.arraySize != entitiesRecords.Length)
            {
                throw new UnityException("Something wrong with factory settings");
            }
        }

        private static void RemoveObsoleteIds(EntityRecord[] entitiesRecords, SerializedProperty capacitiesRecordsProperty)
        {
            int i = 0;
            while (i < capacitiesRecordsProperty.arraySize)
            {
                var sourceId = capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Id").intValue;
                if (!entitiesRecords.Any(x => x.EntityDatabaseId == sourceId))
                {
                    capacitiesRecordsProperty.DeleteArrayElementAtIndex(i);
                }
                else
                {
                    i++;
                }
            }
        }

        private static void AddNotExistingIds(EntityRecord[] entitiesRecords, SerializedProperty capacitiesRecordsProperty)
        {
            foreach (var entitiesRecord in entitiesRecords)
            {
                var found = false;

                for (var i = 0; i < capacitiesRecordsProperty.arraySize; i++)
                {
                    var sourceId = capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Id").intValue;
                    if (sourceId == entitiesRecord.EntityDatabaseId)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    var indexToInsert = 0;
                    for (var i = 0; i < capacitiesRecordsProperty.arraySize; i++)
                    {
                        var sourceId = capacitiesRecordsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Id").intValue;
                        if (sourceId > entitiesRecord.EntityDatabaseId)
                        {
                            break;
                        }
                        indexToInsert++;
                    }

                    capacitiesRecordsProperty.InsertArrayElementAtIndex(indexToInsert);
                    capacitiesRecordsProperty.GetArrayElementAtIndex(indexToInsert).FindPropertyRelative("Id").intValue = entitiesRecord.EntityDatabaseId;
                    capacitiesRecordsProperty.GetArrayElementAtIndex(indexToInsert).FindPropertyRelative("Capacity").intValue = 0;
                }
            }
        }
    }
}

#endif