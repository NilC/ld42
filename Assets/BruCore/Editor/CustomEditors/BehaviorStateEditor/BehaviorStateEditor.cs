﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(BehaviorState), true)]
    public class BehaviorStateEditor : CustomInspectorEditor
    {
        public override void OnInspectorGUI()
        {
            DrawScriptFieldFromScriptableObject();

            serializedObject.Update();

            DrawLayout();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawLayout()
        {
            var behaviorTransitionsProperty = serializedObject.FindProperty("BehaviorTransitions");

            for (var i = 0; i < behaviorTransitionsProperty.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(string.Format("Transaction {0}", i + 1), EditorStyles.boldLabel);

                if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                {
                    behaviorTransitionsProperty.MoveArrayElement(i, i - 1);
                }

                if (GUILayout.Button("↓", GUILayout.Width(30)) && i < behaviorTransitionsProperty.arraySize - 1)
                {
                    behaviorTransitionsProperty.MoveArrayElement(i, i + 1);
                }

                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    behaviorTransitionsProperty.DeleteArrayElementAtIndex(i);
                    EditorGUILayout.EndHorizontal();
                    continue;
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                var decisionProperty = behaviorTransitionsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("Decision");
                EditorGUILayout.PropertyField(decisionProperty, GUIContent.none);

                var nextStateProperty = behaviorTransitionsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("NextState");
                EditorGUILayout.PropertyField(nextStateProperty, GUIContent.none);

                EditorGUILayout.Space();
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+"))//, GUILayout.Width(80)))
            {
                behaviorTransitionsProperty.InsertArrayElementAtIndex(behaviorTransitionsProperty.arraySize);
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}

#endif