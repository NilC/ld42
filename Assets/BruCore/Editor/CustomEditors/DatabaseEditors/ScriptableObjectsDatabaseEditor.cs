﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ScriptableObjectsDatabase), true)]
    public class ScriptableObjectsDatabaseEditor : CustomInspectorEditor
    {
        private bool[] showScriptableObjectsRecords;
        string newScriptableObjectRecordGroupName;
        bool isInAddingGroupState;

        private void OnEnable()
        {
            var database = (ScriptableObjectsDatabase)target;
            showScriptableObjectsRecords = new bool[database.ScriptableObjectRecordGroups.Length];
            if (showScriptableObjectsRecords.Length > 0)
            {
                showScriptableObjectsRecords[0] = true;
            }
        }

        private void OnDisable()
        {
            if (target != null)
            {
                EditorUtility.SetDirty(target);
                SaveAndRefreshAssets();
            }
        }

        public override void OnInspectorGUI()
        {
            var database = (ScriptableObjectsDatabase)target;

            DrawEntitiesLayout(database);

            GUILayout.Space(2.0f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Force update", GUILayout.ExpandWidth(true), GUILayout.Width(250)))
                {
                    OnDisable();
                }
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        protected void DrawEntitiesLayout(ScriptableObjectsDatabase database)
        {
            for (var i = 0; i < database.ScriptableObjectRecordGroups.Length; i++)
            {
                GUILayout.BeginHorizontal();
                var groupText = string.Format("{0} ({1})", database.ScriptableObjectRecordGroups[i].Name, database.ScriptableObjectRecordGroups[i].ScriptableObjectsRecords.Length);
                showScriptableObjectsRecords[i] = EditorGUILayout.Foldout(showScriptableObjectsRecords[i], groupText);
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    ArrayUtility.RemoveAt(ref database.ScriptableObjectRecordGroups, i);
                    ArrayUtility.RemoveAt(ref showScriptableObjectsRecords, i);
                }
                GUILayout.EndHorizontal();

                if (i < showScriptableObjectsRecords.Length && showScriptableObjectsRecords[i])
                {
                    var startId = i == 0 ? 1 : i * 1000;
                    DrawEntitiesRecordsLayout(ref database.ScriptableObjectRecordGroups[i].ScriptableObjectsRecords, startId);
                }
            }

            GUILayout.Space(2.5f);

            if (isInAddingGroupState)
            {
                GUILayout.Space(1.5f);
                GUILayout.BeginHorizontal();
                {
                    newScriptableObjectRecordGroupName = EditorGUILayout.TextField("New Group Name: ", newScriptableObjectRecordGroupName);
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.Space(1.5f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Add Group", GUILayout.ExpandWidth(true), GUILayout.Width(150)))
                {
                    if (isInAddingGroupState)
                    {
                        ArrayUtility.Add(ref database.ScriptableObjectRecordGroups, new ScriptableObjectsRecordGroup(newScriptableObjectRecordGroupName));
                        ArrayUtility.Add(ref showScriptableObjectsRecords, true);
                    }

                    isInAddingGroupState = !isInAddingGroupState;
                }

                if (isInAddingGroupState)
                {
                    if (GUILayout.Button("Cancel", GUILayout.ExpandWidth(true), GUILayout.Width(150)))
                    {
                        isInAddingGroupState = !isInAddingGroupState;
                    }
                }

                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        private void DrawEntitiesRecordsLayout(ref ScriptableObjectsRecord[] items, int startId)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Id", EditorStyles.boldLabel, GUILayout.Width(60));
            EditorGUILayout.LabelField("GameObject", EditorStyles.boldLabel);
            if (GUILayout.Button("+", GUILayout.Width(88)))
            {
                var id = startId;
                if (items.Length > 0)
                {
                    id = Mathf.Max(items.Max(x => x.Id), startId) + 1;
                }

                ArrayUtility.Add(ref items, new ScriptableObjectsRecord(id, null));
            }
            EditorGUILayout.EndHorizontal();

            for (var i = 0; i < items.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                items[i].Id = EditorGUILayout.IntField(items[i].Id, GUILayout.Width(60));

                items[i].ScriptableObject = (ScriptableObject)EditorGUILayout.ObjectField(items[i].ScriptableObject, typeof(ScriptableObject), false);

                if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                {
                    var tmp = items[i];
                    items[i] = items[i - 1];
                    items[i - 1] = tmp;
                }

                if (GUILayout.Button("↓", GUILayout.Width(30)) && i < items.Length - 1)
                {
                    var tmp = items[i];
                    items[i] = items[i + 1];
                    items[i + 1] = tmp;
                }

                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    ArrayUtility.RemoveAt(ref items, i);
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUI.indentLevel--;
        }
    }
}

#endif