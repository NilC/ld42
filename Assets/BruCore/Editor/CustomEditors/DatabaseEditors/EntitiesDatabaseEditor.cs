﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(EntitiesDatabase), true)]
    public class EntitiesDatabaseEditor : CustomInspectorEditor
    {
        private bool[] showEntitiesRecords;
        string newEntityRecordGroupName;
        bool isInAddingGroupState;

        private void OnEnable()
        {
            var database = (EntitiesDatabase)target;

            if (database == null)
            {
                return;
            }

            showEntitiesRecords = new bool[database.EntityRecordGroups.Length];
            if (showEntitiesRecords.Length > 0)
            {
                showEntitiesRecords[0] = true;
            }

            var existingGameObjects = GetGameObjects(database);

            foreach (var existingGameObject in existingGameObjects)
            {
                ResetEntityDatabaseId(existingGameObject);
            }

            MarkForSave(existingGameObjects);
            EditorUtility.SetDirty(target);
            UpdateEntityDatabaseIds(database);
        }

        private void OnDisable()
        {
            var database = (EntitiesDatabase)target;

            if (database != null)
            {
                UpdateEntityDatabaseIds(database);
                EditorUtility.SetDirty(target);
                SaveAndRefreshAssets();
            }
        }

        //private override void OnLostFocus()
        //{
        //    var database = (EntitiesDatabase)target;

        //    UpdateEntityRecordIds(database);
        //    //EditorUtility.SetDirty(database);
        //    //SaveAndRefreshAssets();            
        //}

        public override void OnInspectorGUI()
        {
            var database = (EntitiesDatabase)target;

            DrawEntitiesLayout(database);
            //EditorGUIUtility.labelWidth = 30;
            //EditorGUIUtility.fieldWidth = 30;
            //ArrayLayout("Enemies", ref database.EntitiesRecords, 1);

            GUILayout.Space(2.0f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Force update", GUILayout.ExpandWidth(true), GUILayout.Width(250)))
                {
                    UpdateEntityDatabaseIds(database);
                    EditorUtility.SetDirty(target);
                    SaveAndRefreshAssets();
                }
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
            //{
            //    EditorUtility.SetDirty(target);
            //    UpdateEntityRecordIds(database);
            //    SaveAndRefreshAssets();
            //}
        }

        protected void DrawEntitiesLayout(EntitiesDatabase database)
        {
            for (var i = 0; i < database.EntityRecordGroups.Length; i++)
            {
                GUILayout.BeginHorizontal();
                var groupText = string.Format("{0} ({1})", database.EntityRecordGroups[i].Name, database.EntityRecordGroups[i].EntitiesRecords.Length);
                showEntitiesRecords[i] = EditorGUILayout.Foldout(showEntitiesRecords[i], groupText);
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    ResetEntityRecordsId(GetGameObjects(database.EntityRecordGroups[i].EntitiesRecords.ToList()));
                    ArrayUtility.RemoveAt(ref database.EntityRecordGroups, i);
                    ArrayUtility.RemoveAt(ref showEntitiesRecords, i);
                }
                GUILayout.EndHorizontal();

                if (i < showEntitiesRecords.Length && showEntitiesRecords[i])
                {
                    var startId = i == 0 ? 1 : i * 1000;
                    DrawEntitiesRecordsLayout(ref database.EntityRecordGroups[i].EntitiesRecords, startId);
                }
            }

            GUILayout.Space(2.5f);

            if (isInAddingGroupState)
            {
                GUILayout.Space(1.5f);
                GUILayout.BeginHorizontal();
                {
                    newEntityRecordGroupName = EditorGUILayout.TextField("New Group Name: ", newEntityRecordGroupName);
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.Space(1.5f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Add Group", GUILayout.ExpandWidth(true), GUILayout.Width(150)))
                {
                    if (isInAddingGroupState)
                    {
                        ArrayUtility.Add(ref database.EntityRecordGroups, new EntityRecordGroup(newEntityRecordGroupName));
                        ArrayUtility.Add(ref showEntitiesRecords, true);
                    }

                    isInAddingGroupState = !isInAddingGroupState;
                }

                if (isInAddingGroupState)
                {
                    if (GUILayout.Button("Cancel", GUILayout.ExpandWidth(true), GUILayout.Width(150)))
                    {
                        isInAddingGroupState = !isInAddingGroupState;
                    }
                }

                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        private void DrawEntitiesRecordsLayout(ref EntityRecord[] items, int startId)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Id", EditorStyles.boldLabel, GUILayout.Width(60));
            EditorGUILayout.LabelField("GameObject", EditorStyles.boldLabel);
            if (GUILayout.Button("+", GUILayout.Width(88)))
            {
                var id = startId;
                if (items.Length > 0)
                {
                    id = Mathf.Max(items.Max(x => x.EntityDatabaseId), startId) + 1;
                }

                ArrayUtility.Add(ref items, new EntityRecord(id, null));
            }
            EditorGUILayout.EndHorizontal();

            for (var i = 0; i < items.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                items[i].EntityDatabaseId = EditorGUILayout.IntField(items[i].EntityDatabaseId, GUILayout.Width(60));

                var previousGameObject = items[i].GameObject;
                items[i].GameObject = (GameObject)EditorGUILayout.ObjectField(items[i].GameObject, typeof(GameObject), false);

                if (items[i].GameObject != previousGameObject)
                {
                    ResetEntityDatabaseId(previousGameObject);
                }

                if (GUILayout.Button("↑", GUILayout.Width(30)) && i > 0)
                {
                    var tmp = items[i];
                    items[i] = items[i - 1];
                    items[i - 1] = tmp;
                }

                if (GUILayout.Button("↓", GUILayout.Width(30)) && i < items.Length - 1)
                {
                    var tmp = items[i];
                    items[i] = items[i + 1];
                    items[i + 1] = tmp;
                }

                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    ResetEntityDatabaseId(items[i].GameObject);
                    ArrayUtility.RemoveAt(ref items, i);
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUI.indentLevel--;
        }

        private void UpdateEntityDatabaseIds(EntitiesDatabase database)
        {
            var entitiesRecords = GetEntitiesRecords(database);

            foreach (var entityRecord in entitiesRecords)
            {
                if (entityRecord.GameObject != null && entityRecord.GameObject.GetComponent<EntityComponentDataProvider>() != null)
                {
                    entityRecord.GameObject.GetComponent<EntityComponentDataProvider>().EntityDatabaseId = entityRecord.EntityDatabaseId;
                }
            }

            var gameObjects = GetGameObjects(entitiesRecords);

            MarkForSaveIfChanged(gameObjects);
        }

        private void ResetEntityDatabaseId(GameObject gameObject)
        {
            if (gameObject != null && gameObject.GetComponent<EntityComponentDataProvider>() != null)
            {
                gameObject.GetComponent<EntityComponentDataProvider>().EntityDatabaseId = 0;
            }
        }

        private void ResetEntityRecordsId(List<GameObject> gameObjects)
        {
            foreach (var gameObject in gameObjects)
            {
                if (gameObject != null)
                {
                    gameObject.GetComponent<EntityComponentDataProvider>().EntityDatabaseId = 0;
                }
            }
        }

        private List<EntityRecord> GetEntitiesRecords(EntitiesDatabase database)
        {
            return database
                .EntityRecordGroups
                .SelectMany(x => x.EntitiesRecords)
                .Where(x => x.GameObject != null)
                .ToList();
        }

        private List<GameObject> GetGameObjects(EntitiesDatabase database)
        {
            return GetEntitiesRecords(database)
                .Select(x => x.GameObject)
                .ToList();
        }

        private List<GameObject> GetGameObjects(List<EntityRecord> entitiesRecords)
        {
            return entitiesRecords
                .Select(x => x.GameObject)
                .ToList();
        }
    }
}

#endif