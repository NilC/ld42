﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using BruCore;
    using UnityEditor;

    [CustomEditor(typeof(ComponentsContainer<,>), true)]
    public class ComponentsContainerEditor : CustomInspectorEditor
    {   
        public override void OnInspectorGUI()
        {
            DrawLayout();
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawLayout()
        {
            DrawScriptFieldFromMonoBehaviour();

            var componentsCapacityProperty = serializedObject.FindProperty("ComponentsCapacity");
            EditorGUILayout.PropertyField(componentsCapacityProperty);

            var subsystem = target as IComponentsContainer;
            var entitiesCount = subsystem.GetEntitiesIds().Count;

            EditorGUILayout.LabelField("Components Count", entitiesCount.ToString());

            //EditorGUILayout.Space();

            //var entitiesIdsString = string.Join(", ", subsystem.GetEntitiesIds().Select(x => x.ToString()).ToArray());

            //EditorGUILayout.LabelField("Entities ", entitiesIdsString);
        }
    }
}

#endif