﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using System;
    using System.Linq;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    ///     A helper class for instantiating ScriptableObjects in the editor.
    /// </summary>
    public class ScriptableObjectFactory
    {
        [MenuItem("Assets/Create/ScriptableObject")]
        public static void CreateScriptableObject()
        {
            var assembly = GetAssembly();

            // Get all classes derived from ScriptableObject
            var allScriptableObjects = GetAllScriptableObjects(assembly);

            // Show the selection window.
            ScriptableObjectWindow.Init(allScriptableObjects);
        }

        private static Type[] GetAllScriptableObjects(Assembly assembly)
        {
            var allScriptableObjects = assembly.GetTypes()
                .Where(x => x.IsSubclassOf(typeof (ScriptableObject)) &&
                            !x.IsSubclassOf(typeof (EditorWindow)) &&
                            !x.IsAbstract)
                .ToArray();

            return allScriptableObjects;
        }

        /// <summary>
        ///     Returns the assembly that contains the script code for this project (currently hard coded)
        /// </summary>
        private static Assembly GetAssembly()
        {
            return Assembly.Load(new AssemblyName("Assembly-CSharp"));
        }
    }
}

#endif