﻿#if UNITY_EDITOR

namespace Brutime.BruCore.Editor
{
    using EditorUtils;
    using System;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.ProjectWindowCallback;
    using UnityEngine;

    internal class EndNameEdit : EndNameEditAction
    {
        #region implemented abstract members of EndNameEditAction

        public override void Action(int instanceId, string pathName, string resourceFile)
        {
            AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
        }

        #endregion
    }

    /// <summary>
    ///     Scriptable object window.
    /// </summary>
    public class ScriptableObjectWindow : EditorWindow
    {
        private static string[] names;

        private static Type[] scriptableObjects;
        private int selectedIndex;

        private static Type[] ScriptableObjects
        {
            get { return scriptableObjects; }
            set
            {
                scriptableObjects = value;
                names = scriptableObjects
                    .Select(x => ScriptableObjectUtility.GetNameForUi(x.FullName))
                    .ToArray();
            }
        }

        public static void Init(Type[] allScriptableObjects)
        {
            ScriptableObjects = allScriptableObjects;

            var window = GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
            window.ShowPopup();
        }

        public void OnGUI()
        {
            GUILayout.Label("ScriptableObject Class");
            selectedIndex = EditorGUILayout.Popup(selectedIndex, names);

            if (GUILayout.Button("Create"))
            {
                ScriptableObjectUtility.CreateAsset(scriptableObjects[selectedIndex].Name);
                Close();
            }
        }
    }
}

#endif