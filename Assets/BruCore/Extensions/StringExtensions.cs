﻿namespace Brutime.BruCore
{
    using System.Text.RegularExpressions;

    public static class StringExtensions
    {
        public static string AsNameInWords(this string sourceString)
        {
            var nameInWords = Regex.Replace(sourceString, "(\\B[A-Z])", " $1");
            if (nameInWords.Length > 0)
            {
                nameInWords = char.ToUpper(nameInWords[0]) + nameInWords.Substring(1);
            }
            return nameInWords;
        }
    }
}
