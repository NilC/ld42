﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class GameObjectExtensions
    {
        public static int GetEntityId(this GameObject gameObject)
        {
            return gameObject.GetInstanceID();
        }

        public static Transform[] GetTransformsWithTag(this GameObject gameObject, string tag)
        {
            var allTransforms = gameObject.GetComponentsInChildren<Transform>(true);

            var transformsWithTag = new List<Transform>();

            for (var i = 0; i < allTransforms.Length; i++)
            {
                if (allTransforms[i].CompareTag(tag))
                {
                    transformsWithTag.Add(allTransforms[i]);
                }
            }

            return transformsWithTag.ToArray();
        }

        public static Transform GetTransformWithTag(this GameObject gameObject, string tag)
        {
            var transforms = gameObject.GetTransformsWithTag(tag);

            if (transforms.Length == 0)
            {
                throw new UnityException(string.Format("Component with tag \"{0}\" not found", tag));
            }

            if (transforms.Length > 1)
            {
                throw new UnityException(string.Format("Component with tag \"{0}\" should be only 1", tag));
            }

            return transforms[0];
        }
    }
}
