﻿namespace Brutime.BruCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    public class DebugConsoleWindow : EditorWindow
    {
        private float nameColumnWidth = 150;
        private float valueColumnWidth = 100;

        private GUIStyle fieldTextStyle;

        private Vector2 scrollViewPosition = Vector2.zero;

        private void OnEnable()
        {
            fieldTextStyle = new GUIStyle
            {
                fontStyle = FontStyle.Bold,
            };
        }

        [MenuItem("BruCore/Tools/DebugConsole")]
        public static void ShowWindow()
        {
            GetWindow(typeof(DebugConsoleWindow), false, "DebugConsole");
        }

        private void DrawFields()
        {
            EditorGUILayout.BeginHorizontal(fieldTextStyle);

            GUILayout.Space(20);
            EditorGUILayout.LabelField("Name", fieldTextStyle, GUILayout.Width(nameColumnWidth));
            EditorGUILayout.LabelField("Value", fieldTextStyle, GUILayout.MinWidth(valueColumnWidth), GUILayout.ExpandWidth(true));

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(2);

            scrollViewPosition = EditorGUILayout.BeginScrollView(scrollViewPosition);

            foreach (var message in DebugConsole.GetMessages())
            {
                EditorGUILayout.BeginHorizontal(fieldTextStyle);
                GUILayout.Space(20);
                EditorGUILayout.LabelField(message.Key, GUILayout.Width(nameColumnWidth));
                EditorGUILayout.LabelField(message.Value, GUILayout.MinWidth(valueColumnWidth), GUILayout.ExpandWidth(true));
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndScrollView();
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            DrawFields();
        }
    }
}