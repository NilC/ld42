﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;

    public static class DebugConsole
    {
        private static readonly SortedDictionary<string, string> messages = new SortedDictionary<string, string>();

        public static void UpdateValue(string name, object value)
        {
            messages[name] = value.ToString();
        }

        public static SortedDictionary<string, string> GetMessages()
        {
            return messages;
        }
    }
}