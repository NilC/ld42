namespace Brutime.BruCore
{
    public enum MessagesChannelType
    {
        List,
        Dictionary,
        Single
    }
}
