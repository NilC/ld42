﻿namespace Brutime.BruCore
{
    public enum ComponentFieldGeneratorFieldType
    {
        Int,
        Float,
        Vector2,
        Vector3,
        Vector4,
        Bool,
        Enum,
        Object,
        Long,        
        String
    }
}
