﻿namespace Brutime.BruCore
{
    using System;

    [Serializable]
    public class ComponentGeneratorData
    {
        public ComponentFieldGeneratorFieldType FieldType;
        public string FieldName;
        public string EnumName;
        public string ObjectTypeName;
        public string DefaultValue;
    }
}
