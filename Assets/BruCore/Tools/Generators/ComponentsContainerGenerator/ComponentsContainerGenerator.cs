﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    public class ComponentsContainerGenerator : BaseGenerator
    {
        private const string ComponentsDirectoryName = "Components";

        public void GenerateComponentsContainerFiles(
            string pathToComponentsContainerFolder,
            string namespaceName,
            string componentName,
            string сomponentDataProviderName,
            string сomponentsContainerName,
            ComponentGeneratorData[] componentGeneratorData)
        {
            Directory.CreateDirectory(pathToComponentsContainerFolder);
            WriteComponentFile(pathToComponentsContainerFolder, namespaceName, componentName, componentGeneratorData);
            WriteComponentDataProviderFile(pathToComponentsContainerFolder, namespaceName, сomponentDataProviderName, componentName);
            WriteComponentsContainerFile(pathToComponentsContainerFolder, namespaceName, сomponentsContainerName, componentName, сomponentDataProviderName);
        }

        private void WriteComponentFile(string pathToComponents, string namespaceName, string componentName, ComponentGeneratorData[] componentGeneratorData)
        {
            var template = (TextAsset)AssetDatabase.LoadAssetAtPath(Path.Combine(PathToTemplates, "ComponentTemplate.txt"), typeof(TextAsset));

            var templateText = template.text;

            templateText = templateText.Replace("<Namespace>", namespaceName);
            templateText = templateText.Replace("<ComponentName>", componentName);
            templateText = templateText.Replace("<Fields>", GetFields(componentGeneratorData));

            var pathToComponentsFile = Path.Combine(pathToComponents, string.Format("{0}.cs", componentName));
            File.WriteAllText(pathToComponentsFile, templateText);
            AssetDatabase.Refresh();
        }

        private void WriteComponentDataProviderFile(string pathToComponents, string namespaceName, string сomponentDataProviderName, string componentName)
        {
            var template = (TextAsset)AssetDatabase.LoadAssetAtPath(Path.Combine(PathToTemplates, "ComponentDataProviderTemplate.txt"), typeof(TextAsset));

            var templateText = template.text;

            templateText = templateText.Replace("<Namespace>", namespaceName);
            templateText = templateText.Replace("<ComponentDataProviderName>", сomponentDataProviderName);
            templateText = templateText.Replace("<ComponentName>", componentName);

            var pathToComponentDataProviderFile = Path.Combine(pathToComponents, string.Format("{0}.cs", сomponentDataProviderName));
            File.WriteAllText(pathToComponentDataProviderFile, templateText);
            AssetDatabase.Refresh();
        }

        private void WriteComponentsContainerFile(string pathToComponents, string namespaceName, 
            string сomponentsContainerName, string componentName, string сomponentDataProviderName)
        {
            var template = (TextAsset)AssetDatabase.LoadAssetAtPath(Path.Combine(PathToTemplates, "ComponentsContainerTemplate.txt"), typeof(TextAsset));

            var templateText = template.text;

            templateText = templateText.Replace("<Namespace>", namespaceName);
            templateText = templateText.Replace("<ComponentsContainerName>", сomponentsContainerName);
            templateText = templateText.Replace("<ComponentName>", componentName);
            templateText = templateText.Replace("<ComponentDataProviderName>", сomponentDataProviderName);

            var pathToComponentsContainerFile = Path.Combine(pathToComponents, string.Format("{0}.cs", сomponentsContainerName));
            File.WriteAllText(pathToComponentsContainerFile, templateText);
            AssetDatabase.Refresh();
        }
    }
}

#endif