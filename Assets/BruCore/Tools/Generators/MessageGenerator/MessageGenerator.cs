﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using System;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEditor;
    using UnityEngine;

    public class MessageGenerator : BaseGenerator
    {
        public void GenerateSubsystemFiles(
            string pathToMessagesFolder,
            string namespaceName,
            string messageName,
            MessagesChannelType messagesChannelType,
            ComponentGeneratorData[] attributesGeneratorData)
        {
            Directory.CreateDirectory(pathToMessagesFolder);
            WriteMessageFile(pathToMessagesFolder, namespaceName, messageName, messagesChannelType, attributesGeneratorData);
        }

        private void WriteMessageFile(string pathToMessagesFolder, string namespaceName, string messageName, MessagesChannelType messagesChannelType, ComponentGeneratorData[] attributesGeneratorData)
        {
            var template = (TextAsset)AssetDatabase.LoadAssetAtPath(Path.Combine(PathToTemplates, "MessageTemplate.txt"), typeof(TextAsset));

            var templateText = template.text;

            templateText = templateText.Replace("<Namespace>", namespaceName);
            templateText = templateText.Replace("<MessageName>", messageName);
            templateText = templateText.Replace("<Fields>", GetFields(attributesGeneratorData));
            templateText = templateText.Replace("<MessagesChannel>", GetMessagesChannel(messagesChannelType));

            var pathToMessageFile = Path.Combine(pathToMessagesFolder, string.Format("{0}.cs", messageName));
            File.WriteAllText(pathToMessageFile, templateText);
            AssetDatabase.Refresh();
        }

        private string GetMessagesChannel(MessagesChannelType messagesChannelType)
        {
            switch (messagesChannelType)
            {
                case MessagesChannelType.List: return "ListMessagesChannel";
                case MessagesChannelType.Dictionary: return "DictionaryMessagesChannel";

                default:
                    throw new UnityException(string.Format("Unknown messagesChannelType: {0}", messagesChannelType));
            }
        }
    }
}

#endif