namespace Brutime.BruCore
{
    using System.Text;
    using UnityEngine;

    public abstract class BaseGenerator
    {
        protected const string PathToTemplates = "Assets/BruCore/Tools/Generators/Templates";

        protected string GetFields(ComponentGeneratorData[] componentGeneratorData)
        {
            var parametersBuilder = new StringBuilder(componentGeneratorData.Length * 80);

            for (var i = 0; i < componentGeneratorData.Length; i++)
            {
                var fieldName = componentGeneratorData[i].FieldName;
                var fieldTypeName = GetFieldTypeName(componentGeneratorData[i]);

                parametersBuilder.AppendFormat("        public {0} {1};", fieldTypeName, fieldName);
                if (i < componentGeneratorData.Length - 1)
                {
                    parametersBuilder.AppendLine();
                }
            }

            return parametersBuilder.ToString();
        }

        protected string GetFieldTypeName(ComponentGeneratorData componentGeneratorData)
        {
            switch (componentGeneratorData.FieldType)
            {
                case ComponentFieldGeneratorFieldType.Int: return "int";
                case ComponentFieldGeneratorFieldType.Float: return "float";
                case ComponentFieldGeneratorFieldType.Vector2: return "Vector2";
                case ComponentFieldGeneratorFieldType.Vector3: return "Vector3";
                case ComponentFieldGeneratorFieldType.Vector4: return "Vector4";
                case ComponentFieldGeneratorFieldType.Bool: return "bool";
                case ComponentFieldGeneratorFieldType.Enum: return componentGeneratorData.EnumName;
                case ComponentFieldGeneratorFieldType.Object: return componentGeneratorData.ObjectTypeName;
                case ComponentFieldGeneratorFieldType.Long: return "long";
                case ComponentFieldGeneratorFieldType.String: return "string";

                default:
                    throw new UnityException(string.Format("Unknown field type: {0}", componentGeneratorData.FieldType));
            }
        }
    }
}