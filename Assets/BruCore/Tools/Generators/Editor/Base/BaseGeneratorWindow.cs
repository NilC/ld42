namespace Brutime.BruCore.Editor
{
    using System;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public abstract class BaseGeneratorWindow : EditorWindow
    {
        private string[] enumsNames;
        private string[] objectsNames = new string[] { "Transform", "Ability" };

        private readonly float[] columnsSizes = new float[] { 90.0f, 120.0f, 210.0f, 60.0f, 60.0f };

        protected void Initialize(string namespaceName)
        {
            enumsNames = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsEnum && x.Namespace == namespaceName)
                .Select(x => x.Name)
                .ToArray();
        }


        protected virtual void OnInspectorUpdate()
        {
            Repaint();
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Close();
            }
        }

        protected virtual void OnGUI()
        {
            DrawUI();
        }

        protected abstract void DrawUI();

        protected void DrawComponentGeneratorData(ref ComponentGeneratorData[] componentGeneratorData)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Field Type", GUILayout.Width(columnsSizes[0]));
            EditorGUILayout.LabelField("Enum/Object Name", GUILayout.Width(columnsSizes[1]));
            EditorGUILayout.LabelField("Field Name", GUILayout.Width(columnsSizes[2]));

                        EditorGUILayout.EndHorizontal();

            var indexToDelete = -1;
            for (var i = 0; i < componentGeneratorData.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                var componentData = componentGeneratorData[i];

                componentData.FieldType = (ComponentFieldGeneratorFieldType)EditorGUILayout.EnumPopup(componentData.FieldType, GUILayout.Width(columnsSizes[0]));

                if (componentData.FieldType == ComponentFieldGeneratorFieldType.Enum)
                {
                    var selectedIndex = ArrayUtility.IndexOf(enumsNames, componentData.EnumName);
                    selectedIndex = EditorGUILayout.Popup(selectedIndex, enumsNames, GUILayout.Width(columnsSizes[1]));
                    componentData.EnumName = selectedIndex != -1 ? enumsNames[selectedIndex] : string.Empty;
                    if (!string.IsNullOrEmpty(componentData.EnumName) && string.IsNullOrEmpty(componentData.FieldName))
                    {
                        componentData.FieldName = componentData.EnumName;
                    }
                }
                else if (componentData.FieldType == ComponentFieldGeneratorFieldType.Object)
                {
                    var selectedIndex = ArrayUtility.IndexOf(objectsNames, componentData.ObjectTypeName);
                    selectedIndex = EditorGUILayout.Popup(selectedIndex, objectsNames, GUILayout.Width(columnsSizes[1]));
                    componentData.ObjectTypeName = selectedIndex != -1 ? objectsNames[selectedIndex] : string.Empty;
                    if (!string.IsNullOrEmpty(componentData.ObjectTypeName) && string.IsNullOrEmpty(componentData.FieldName))
                    {
                        componentData.FieldName = componentData.ObjectTypeName;
                    }
                }
                else
                {
                    EditorGUILayout.LabelField(string.Empty, GUILayout.Width(columnsSizes[1]));
                }

                componentData.FieldName = EditorGUILayout.TextField(componentData.FieldName, GUILayout.Width(columnsSizes[2]));

                if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(16)))
                {
                    indexToDelete = i;
                }

                EditorGUILayout.EndHorizontal();
            }

            if (indexToDelete > -1)
            {
                ArrayUtility.RemoveAt(ref componentGeneratorData, indexToDelete);
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Add Field"))
            {
                ArrayUtility.Add(ref componentGeneratorData, new ComponentGeneratorData());
            }
        }
    }
}