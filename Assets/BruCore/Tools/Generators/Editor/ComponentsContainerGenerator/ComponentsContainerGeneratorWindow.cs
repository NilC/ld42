﻿namespace Brutime.BruCore.Editor
{
    using Brutime.BruCore.EditorUtils;
    using System;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    public class ComponentsContainerGeneratorWindow : BaseGeneratorWindow
    {
        [Serializable]
        private class ComponentsContainerParameters : UnityEngine.Object
        {
            public string NamespaceName { get; set; }
            public string BaseName { get; set; }
            public string ComponentName { get; set; }
            public string ComponentDataProviderName { get; set; }
            public string ComponentsContainerName { get; set; }
            public ComponentGeneratorData[] ComponentGeneratorData = new ComponentGeneratorData[0];
        }

        private GUIStyle fieldTextStyle;

        private ComponentsContainerParameters componentsContainerParameters;
        private ComponentsContainerGenerator componentsContainerGenerator = new ComponentsContainerGenerator();

        private void OnEnable()
        {
            fieldTextStyle = new GUIStyle
            {
                fontStyle = FontStyle.Bold,
            };

            componentsContainerParameters = new ComponentsContainerParameters
            {
                NamespaceName = EditorSettings.projectGenerationRootNamespace
            };

            Initialize(componentsContainerParameters.NamespaceName);
        }

        public static void ShowWindow()
        {
            var window = CreateInstance<ComponentsContainerGeneratorWindow>();
            window.titleContent = new GUIContent("Components Container Generator");
            window.minSize = new Vector2(620.0f, 320.0f);
            window.maxSize = new Vector2(620.0f, 720.0f);
            window.ShowUtility();
        }

        protected override void DrawUI()
        {
            var style = new GUIStyle();
            style.padding = new RectOffset(20, 20, 10, 10);
            EditorGUILayout.BeginVertical(style);
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Names", fieldTextStyle);
                EditorGUILayout.Space();

                componentsContainerParameters.NamespaceName = EditorGUILayout.TextField("Namespace Name", componentsContainerParameters.NamespaceName);
                var baseName = EditorGUILayout.TextField("Base Name", componentsContainerParameters.BaseName);
                var componentName = EditorGUILayout.TextField("Component Name", componentsContainerParameters.ComponentName);
                var componentDataProviderName = EditorGUILayout.TextField("Component Data Provider Name", componentsContainerParameters.ComponentDataProviderName);
                var componentsContainerName = EditorGUILayout.TextField("Components Containe Name", componentsContainerParameters.ComponentsContainerName);

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Fields", fieldTextStyle);
                EditorGUILayout.Space();

                DrawComponentGeneratorData(ref componentsContainerParameters.ComponentGeneratorData);

                if (baseName != componentsContainerParameters.BaseName)
                {
                    componentsContainerParameters.BaseName = baseName;
                    componentsContainerParameters.ComponentDataProviderName = string.Format("{0}ComponentDataProvider", baseName);
                    componentsContainerParameters.ComponentName = string.Format("{0}Component", baseName);
                    componentsContainerParameters.ComponentsContainerName = string.Format("{0}ComponentsContainer", baseName);
                }
                else
                {
                    componentsContainerParameters.ComponentDataProviderName = componentDataProviderName;
                    componentsContainerParameters.ComponentName = componentName;
                    componentsContainerParameters.ComponentsContainerName = componentsContainerName;
                }

                EditorGUILayout.Space();
                if (GUILayout.Button("Ok"))
                {
                    Generate();
                    Close();
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void Generate()
        {
            var pathToSubsystemFolder = Path.Combine(EditorUtils.GetSelectedPathOrFallback(), componentsContainerParameters.ComponentName);

            componentsContainerGenerator.GenerateComponentsContainerFiles(
                pathToSubsystemFolder,
                componentsContainerParameters.NamespaceName,
                componentsContainerParameters.ComponentName,
                componentsContainerParameters.ComponentDataProviderName,
                componentsContainerParameters.ComponentsContainerName,
                componentsContainerParameters.ComponentGeneratorData);
        }
    }
}