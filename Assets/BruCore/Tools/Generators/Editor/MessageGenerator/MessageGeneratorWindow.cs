﻿namespace Brutime.BruCore.Editor
{
    using Brutime.BruCore.EditorUtils;
    using System;
    using System.IO;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public class MessageGeneratorWindow : BaseGeneratorWindow
    {
        [Serializable]
        private class MessageParameters : UnityEngine.Object
        {
            public string NamespaceName { get; set; }
            public string MessageName { get; set; }
            public MessagesChannelType MessagesChannelType { get; set; }
            public ComponentGeneratorData[] AttributeGeneratorData = new ComponentGeneratorData[0];
        }

        private GUIStyle fieldTextStyle;

        private MessageParameters messageParameters;
        private MessageGenerator messageGenerator = new MessageGenerator();

        private void OnEnable()
        {
            fieldTextStyle = new GUIStyle
            {
                fontStyle = FontStyle.Bold,
            };

            messageParameters = new MessageParameters
            {
                NamespaceName = EditorSettings.projectGenerationRootNamespace,
                AttributeGeneratorData = new ComponentGeneratorData[]
                {
                    new ComponentGeneratorData
                    {
                        FieldType = ComponentFieldGeneratorFieldType.Int,
                        FieldName = "EntityId"
                    }
                }
            };

            Initialize(messageParameters.NamespaceName);
        }

        public static void ShowWindow()
        {
            var window = CreateInstance<MessageGeneratorWindow>();
            window.titleContent = new GUIContent("Message Generator");
            window.minSize = new Vector2(620.0f, 320.0f);
            window.maxSize = new Vector2(620.0f, 720.0f);
            window.ShowUtility();
        }

        protected override void DrawUI()
        {
            var style = new GUIStyle();
            style.padding = new RectOffset(20, 20, 10, 10);
            EditorGUILayout.BeginVertical(style);
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Names", fieldTextStyle);
                EditorGUILayout.Space();

                messageParameters.NamespaceName = EditorGUILayout.TextField("Namespace Name", messageParameters.NamespaceName);

                EditorGUILayout.BeginHorizontal();
                {
                    var messageName = EditorGUILayout.TextField("Message Name", messageParameters.MessageName);
                    if (messageName != messageParameters.MessageName)// && !string.IsNullOrEmpty(messageName) && !messageParameters.MessageName.EndsWith("Message"))
                    {
                        messageParameters.MessageName = string.Format("{0}Message", messageName);
                    }
                }
                EditorGUILayout.EndHorizontal();

                messageParameters.MessagesChannelType = (MessagesChannelType)EditorGUILayout.EnumPopup("Messages Channel Type", messageParameters.MessagesChannelType);

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Fields", fieldTextStyle);
                EditorGUILayout.Space();

                DrawComponentGeneratorData(ref messageParameters.AttributeGeneratorData);

                EditorGUILayout.Space();
                if (GUILayout.Button("Ok"))
                {
                    Generate();
                    Close();
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void Generate()
        {
            //messageParameters.MessageName += "Message";

            var pathToMessagesFolder = Path.Combine(EditorUtils.GetSelectedPathOrFallback(), "Messages");

            messageGenerator.GenerateSubsystemFiles(
                pathToMessagesFolder,
                messageParameters.NamespaceName,
                messageParameters.MessageName,
                messageParameters.MessagesChannelType,
                messageParameters.AttributeGeneratorData);
        }

        private string GetFirstPartOfPath(string path)
        {
            var formattedPath = path.Replace("/", "\\");

            if (formattedPath.Count(x => x == '\\') <= 1)
            {
                return formattedPath;
            }

            var index = formattedPath.IndexOf('\\', formattedPath.IndexOf('\\') + 1);
            var firstPart = formattedPath.Substring(0, index);

            return firstPart;
        }
    }
}