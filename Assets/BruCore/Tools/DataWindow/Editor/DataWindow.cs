﻿namespace Brutime.BruCore.Editor
{
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using UnityEditor;
    using UnityEngine;

    public class DataWindow : EditorWindow
    {
        private Vector2 leftPanelScrollViewPosition;
        private Vector2 rightPanelFirstColumnScrollViewPosition;
        private Vector2 rightPanelSecondColumnScrollViewPosition;

        private Color normalBackgroundColor;
        private Color selectionColor = new Color(0.8f, 0.85f, 0.95f);

        private readonly float[] columnsSizes = { 20.0f, 300.0f };

        private int selectedContainersComponentsIndex = 0;

        //private Regex camelCaseToWorlds = new Regex()

        private void OnEnable()
        {
            normalBackgroundColor = GUI.backgroundColor;
        }

        [MenuItem("BruCore/Tools/Components")]
        public static void ShowWindow()
        {
            var window = CreateInstance<DataWindow>();
            window.titleContent = new GUIContent("Components");
            window.Show();
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            DrawUI();
        }

        private void DrawUI()
        {
            if (CoreComponents.AllComponentsContainers.Count == 0)
            {
                EditorGUILayout.LabelField("Components Containers not initialized", EditorStyles.boldLabel);
                return;
            }

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                GUILayout.BeginHorizontal();
                {
                    DrawComponentsContainersList();
                    DrawFields();
                    DrawEntitiesIds();
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();

            GUI.backgroundColor = normalBackgroundColor;
        }

        private void DrawComponentsContainersList()
        {
            GUILayout.BeginVertical(GUILayout.MinWidth(columnsSizes[0] + columnsSizes[1] + 30.0f),
                                    GUILayout.MaxWidth(columnsSizes[0] + columnsSizes[1] + 30.0f));
            {
                leftPanelScrollViewPosition = EditorGUILayout.BeginScrollView(leftPanelScrollViewPosition,
                                                                              GUILayout.MinWidth(columnsSizes[0] + columnsSizes[1] + 30.0f),
                                                                              GUILayout.MaxWidth(columnsSizes[0] + columnsSizes[1] + 30.0f));

                for (var i = 0; i < CoreComponents.AllComponentsContainers.Count; i++)
                {
                    var componentsContainer = CoreComponents.AllComponentsContainers[i];
                    GUI.backgroundColor = i == selectedContainersComponentsIndex ? selectionColor : normalBackgroundColor;
                    GUILayout.BeginHorizontal();
                    {
                        var componentsContainerText = componentsContainer.Name + (componentsContainer.IsExecutingInEditMode() ? " [Editor]" : string.Empty);
                        if (GUILayout.Button(new GUIContent(componentsContainerText), GUILayout.MaxWidth(columnsSizes[1])))
                        {
                            selectedContainersComponentsIndex = i;
                        }
                    }
                    GUILayout.EndHorizontal();
                    GUI.backgroundColor = normalBackgroundColor;
                }

                EditorGUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }

        private void DrawFields()
        {
            GUILayout.BeginVertical(GUILayout.MinWidth(200.0f), GUILayout.MaxWidth(240.0f));
            {
                rightPanelSecondColumnScrollViewPosition = EditorGUILayout.BeginScrollView(rightPanelSecondColumnScrollViewPosition);
                var componentsContainer = CoreComponents.AllComponentsContainers[selectedContainersComponentsIndex];
                if (componentsContainer != null)
                {
                    GUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(string.Format("Count: {0}", componentsContainer.GetEntitiesIds().Count), EditorStyles.boldLabel);
                    }
                    GUILayout.EndHorizontal();

                    var fieldsInfo = componentsContainer
                        .GetType()
                        .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Where(x => x.GetCustomAttributes(typeof(SerializeField), false).Length > 0)
                        .ToArray();

                    foreach (var fieldInfo in fieldsInfo)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            var fieldNameInWords = Regex.Replace(fieldInfo.Name, "(\\B[A-Z])", " $1");
                            if (fieldNameInWords.Length > 0)
                            {
                                fieldNameInWords = char.ToUpper(fieldNameInWords[0]) + fieldNameInWords.Substring(1);
                            }
                            EditorGUILayout.LabelField(string.Format("{0}: ", fieldNameInWords), EditorStyles.boldLabel, GUILayout.MaxWidth(100.0f));
                            EditorGUILayout.LabelField(fieldInfo.GetValue(componentsContainer).ToString());
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }

        private void DrawEntitiesIds()
        {
            GUILayout.BeginVertical(GUILayout.MinWidth(140.0f), GUILayout.MaxWidth(220.0f));
            {
                rightPanelFirstColumnScrollViewPosition = EditorGUILayout.BeginScrollView(rightPanelFirstColumnScrollViewPosition);
                var componentsContainer = CoreComponents.AllComponentsContainers[selectedContainersComponentsIndex];
                if (componentsContainer != null)
                { 
                    GUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("Entities Ids", EditorStyles.boldLabel);
                    }
                    GUILayout.EndHorizontal();
                    var entitiesIds = componentsContainer.GetEntitiesIds();
                    foreach (var entityId in entitiesIds)
                    {
                        GUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.LabelField(entityId.ToString());
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }
    }
}