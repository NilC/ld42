﻿namespace Brutime.BruCore.Editor
{
    using UnityEditor;

    public static class AssetsMenu
    {
        [MenuItem("Assets/Generate Components Container", false, 2000)]
        public static void GenerateSubsystem()
        {
            ComponentsContainerGeneratorWindow.ShowWindow();
        }

        [MenuItem("Assets/Generate Message", false, 2001)]
        public static void GenerateMessage()
        {
            MessageGeneratorWindow.ShowWindow();
        }        
    }
}