namespace Brutime.BruCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    public class SubsystemsWindow : EditorWindow
    {
        private Vector2 leftPanelScrollViewPosition;
        private Vector2 rightPanelFirstColumnScrollViewPosition;
        private Vector2 rightPanelSecondColumnScrollViewPosition;

        private Color normalBackgroundColor;
        private Color selectionColor = new Color(0.8f, 0.85f, 0.95f);

        private readonly float[] columnsSizes = { 20.0f, 300.0f };

        private void OnEnable()
        {
            normalBackgroundColor = GUI.backgroundColor;
        }

        [MenuItem("BruCore/Tools/Subsystems")]
        public static void ShowWindow()
        {
            var window = CreateInstance<SubsystemsWindow>();
            window.titleContent = new GUIContent("Subsystems");
            window.Show();
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            DrawUI();
        }

        private void DrawUI()
        {
            if (CoreSubsystems.AllSubsystems.Count == 0)
            {
                EditorGUILayout.LabelField("Subsystems not initialized", EditorStyles.boldLabel);
                return;
            }

            DrawSubsystemsList();

            GUI.backgroundColor = normalBackgroundColor;
        }

        private void DrawSubsystemsList()
        {
            EditorGUILayout.Space();

            GUILayout.BeginVertical(GUILayout.MinWidth(columnsSizes[0] + columnsSizes[1] + 30.0f),
                                    GUILayout.MaxWidth(columnsSizes[0] + columnsSizes[1] + 30.0f));
            {
                leftPanelScrollViewPosition = EditorGUILayout.BeginScrollView(leftPanelScrollViewPosition,
                                                                              GUILayout.MinWidth(columnsSizes[0] + columnsSizes[1] + 30.0f),
                                                                              GUILayout.MaxWidth(columnsSizes[0] + columnsSizes[1] + 30.0f));

                for (var i = 0; i < CoreSubsystems.AllSubsystems.Count; i++)
                {
                    var subsystem = CoreSubsystems.AllSubsystems[i];
                    GUILayout.BeginHorizontal();
                    {
                        subsystem.IsEnabled = EditorGUILayout.Toggle(subsystem.IsEnabled, GUILayout.MaxWidth(columnsSizes[0]));
                        var subsystemText = subsystem.Name + (subsystem.IsExecutingInEditMode() ? " [Editor]" : string.Empty);
                        EditorGUILayout.LabelField(new GUIContent(subsystemText));
                    }
                    GUILayout.EndHorizontal();
                    GUI.backgroundColor = normalBackgroundColor;
                }

                EditorGUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }
    }
}