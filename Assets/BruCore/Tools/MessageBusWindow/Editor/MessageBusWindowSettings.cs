﻿namespace Brutime.BruCore.Editor
{
    using System;
    using UnityEngine;

    [Serializable]
    public class MessageBusWindowSettings
    {
        [SerializeField]
        public MessageType[] SelectedMessagesTypes;

        [SerializeField]
        public DebugMessageType[] SelectedDebugMessageTypes;
    }
}