﻿ namespace Brutime.BruCore.Editor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public class MessageBusWindow : EditorWindow
    {
        private readonly float[] columnsSizes = { 20.0f, 200 };

        private Vector2 leftPanelScrollViewPosition = Vector2.zero;
        private Vector2 frameButtonsScrollViewPosition = Vector2.zero;
        private Vector2 messagesScrollViewPosition = Vector2.zero;

        private const float frameButtonWidth = 100.0f;
        private const float frameButtonWithNoMessagesWidth = 60.0f;
        private const float frameButtonHeight = 80.0f;

        private Color normalBackgroundColor;
        private Color selectionColor = new Color(0.8f, 0.85f, 0.95f);
        private Color emptyFrameColor = new Color(0.58f, 0.65f, 0.65f);

        private bool isAllEnabled;

        private int selectedFrameIndex = 0;
        private FrameDebugMessage lastFrameDebugMessage = null;

        private List<MessageType> selectedMessagesTypes = new List<MessageType>();
        private List<DebugMessageType> selectedDebugMessageTypes = new List<DebugMessageType>();

        [MenuItem("BruCore/Tools/MessageBus")]
        public static void ShowWindow()
        {
            var window = CreateInstance<MessageBusWindow>();
            window.titleContent = new GUIContent("Message Bus");
            window.Show();
        }

        private void OnEnable()
        {
            LoadSettings();

            FillMessagesTypes();

            normalBackgroundColor = GUI.backgroundColor;

            if (!selectedDebugMessageTypes.Contains(DebugMessageType.Published))
            {
                selectedDebugMessageTypes.Add(DebugMessageType.Published);
            }
        }

        private void OnDisable()
        {
            SaveSettings();
        }

        private void OnLostFocus()
        {
            SaveSettings();
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            DrawUI();
        }

        private void DrawUI()
        {
            GUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical(GUILayout.MinWidth(columnsSizes[0] + columnsSizes[1] + 30.0f),
                                              GUILayout.MaxWidth(columnsSizes[0] + columnsSizes[1] + 30.0f));
                {
                    DrawMessagesTypes();
                }
                GUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                {
                    DrawTogglesForDebugMessageTypes();
                    DrawFrameButtons();
                    DrawMessages();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            GUI.backgroundColor = normalBackgroundColor;
        }

        private void DrawMessagesTypes()
        {
            leftPanelScrollViewPosition = EditorGUILayout.BeginScrollView(leftPanelScrollViewPosition);

            bool wasAllEnabled = isAllEnabled;
            GUILayout.BeginHorizontal();
            {
                isAllEnabled = EditorGUILayout.Toggle(isAllEnabled, GUILayout.MaxWidth(columnsSizes[0]));
                EditorGUILayout.LabelField("[All]", GUILayout.MinWidth(columnsSizes[1]), GUILayout.MaxWidth(columnsSizes[1]));
            }
            GUILayout.EndHorizontal();

            if (isAllEnabled != wasAllEnabled)
            {
                selectedMessagesTypes.ForEach(x => x.IsEnabled = isAllEnabled);
            }

            for (var i = 0; i < selectedMessagesTypes.Count; i++)
            {
                var messageType = selectedMessagesTypes[i];
                GUILayout.BeginHorizontal();
                {
                    messageType.IsEnabled = EditorGUILayout.Toggle(messageType.IsEnabled, GUILayout.MaxWidth(columnsSizes[0]));
                    var messageTypeText = messageType.TypeName;
                    EditorGUILayout.LabelField(messageTypeText, GUILayout.MinWidth(columnsSizes[1]), GUILayout.MaxWidth(columnsSizes[1]));
                }
                GUILayout.EndHorizontal();
                GUI.backgroundColor = normalBackgroundColor;
            }

            if (selectedMessagesTypes.Any(x => !x.IsEnabled))
            {
                isAllEnabled = false;
            }

            EditorGUILayout.EndScrollView();
        }

        private void DrawTogglesForDebugMessageTypes()
        {
            var style = new GUIStyle();
            style.padding = new RectOffset(10, 10, 5, 5);
            EditorGUILayout.BeginHorizontal(style, GUILayout.Height(32));
            {
                ToggleSelectedDebugMessageType(DebugMessageType.Published);
                ToggleSelectedDebugMessageType(DebugMessageType.Got);
                ToggleSelectedDebugMessageType(DebugMessageType.GotAll);
                ToggleSelectedDebugMessageType(DebugMessageType.Cleared);
            }
            EditorGUILayout.EndHorizontal();
        }

        private void ToggleSelectedDebugMessageType(DebugMessageType debugMessageType)
        {
            if (EditorGUILayout.Toggle(selectedDebugMessageTypes.Contains(debugMessageType), GUILayout.Width(30)))
            {
                if (!selectedDebugMessageTypes.Contains(debugMessageType))
                {
                    selectedDebugMessageTypes.Add(debugMessageType);
                }
            }
            else
            {
                if (selectedDebugMessageTypes.Contains(debugMessageType))
                {
                    selectedDebugMessageTypes.Remove(debugMessageType);
                }
            }
            EditorGUILayout.LabelField(debugMessageType.ToString(), GUILayout.Width(90));
        }

        private void DrawFrameButtons()
        {
            var style = new GUIStyle();
            style.padding = new RectOffset(10, 10, 0, 0);
            EditorGUILayout.BeginHorizontal(style, GUILayout.Height(frameButtonHeight + 22));
            {
                var frameDebugMessages = CoreMessageBus.GetFrameDebugMessages();
                if (frameDebugMessages.Count > 0 && lastFrameDebugMessage != frameDebugMessages.Last())
                {
                    frameButtonsScrollViewPosition = new Vector2 { x = 5000.0f, y = 0.0f };
                    lastFrameDebugMessage = frameDebugMessages.Last();
                }
                frameButtonsScrollViewPosition = EditorGUILayout.BeginScrollView(frameButtonsScrollViewPosition);
                {
                    EditorGUILayout.BeginHorizontal(GUILayout.Height(frameButtonHeight));
                    {
                        var startFrame = -1;

                        for (var i = 0; i < frameDebugMessages.Count; i++)
                        {
                            var frameDebugMessage = frameDebugMessages[i];

                            if (!frameDebugMessage
                                .DebugMessages
                                .Any(x => selectedDebugMessageTypes.Contains(x.DebugMessageType) &&
                                selectedMessagesTypes.Any(y => y.IsEnabled && y.TypeFullName == x.MessageType.FullName)))
                            {
                                if (startFrame == -1)
                                {
                                    startFrame = frameDebugMessage.FrameNumber;
                                }
                            }
                            else
                            {
                                if (startFrame != -1)
                                {
                                    GUI.backgroundColor = emptyFrameColor;
                                    GUILayout.Button(string.Format("{0}{2}-{2}{1}", startFrame, frameDebugMessage.FrameNumber, Environment.NewLine),
                                        GUILayout.Width(frameButtonWithNoMessagesWidth), GUILayout.Height(frameButtonHeight));
                                    GUI.backgroundColor = normalBackgroundColor;
                                    startFrame = -1;
                                }

                                GUI.backgroundColor = i == selectedFrameIndex ? selectionColor : normalBackgroundColor;
                                if (GUILayout.Button(frameDebugMessage.FrameNumber.ToString(),
                                        GUILayout.Width(frameButtonWidth), GUILayout.Height(frameButtonHeight)))
                                {
                                    selectedFrameIndex = i;
                                }
                                GUI.backgroundColor = normalBackgroundColor;
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawMessages()
        {
            var style = new GUIStyle();
            style.padding = new RectOffset(10, 10, 0, 0);
            messagesScrollViewPosition = EditorGUILayout.BeginScrollView(messagesScrollViewPosition, style);
            {
                var frameDebugMessages = CoreMessageBus.GetFrameDebugMessages();
                if (selectedFrameIndex < frameDebugMessages.Count)
                {
                    var frameDebugMessage = frameDebugMessages[selectedFrameIndex];

                    var publishedDebugMessages = frameDebugMessage.DebugMessages
                        .Where(x => selectedDebugMessageTypes.Contains(x.DebugMessageType))
                        .Where(x => selectedMessagesTypes.Any(y => y.IsEnabled && y.TypeFullName == x.MessageType.FullName))
                        .ToList();

                    var labelStyle = new GUIStyle();
                    labelStyle.padding = new RectOffset(0, 0, 0, 5);
                    labelStyle.wordWrap = true;

                    foreach (var debugMessage in publishedDebugMessages)
                    {
                        EditorGUILayout.LabelField(debugMessage.Message, labelStyle);
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }

        private void FillMessagesTypes()
        {
            var excludeMessages = new List<string> { "DebugMessage", "FrameDebugMessage" };

            if (selectedMessagesTypes == null)
            {
                selectedMessagesTypes = new List<MessageType>();
            }

            selectedMessagesTypes = AppDomain
                .CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.Namespace != null && x.Namespace.Contains("Brutime"))
                .Where(x => x.IsValueType)
                .Where(x => x.FullName.EndsWith("Message") && !excludeMessages.Contains(x.Name))
                .Select(x => new MessageType
                {
                    TypeName = x.Name,
                    TypeFullName = x.FullName,
                    IsEnabled = selectedMessagesTypes.SingleOrDefault(y => y.TypeFullName == x.FullName) != null
                                ? selectedMessagesTypes.SingleOrDefault(y => y.TypeFullName == x.FullName).IsEnabled
                                : false
                })
                .ToList();
        }

        private void LoadSettings()
        {
            string pathToSettingsFile = GetPathToSettingsFile();

            if (File.Exists(pathToSettingsFile))
            {
                var dataAsJson = File.ReadAllText(pathToSettingsFile);
                var settings = JsonUtility.FromJson<MessageBusWindowSettings>(dataAsJson);

                selectedMessagesTypes = settings.SelectedMessagesTypes.ToList();
                selectedDebugMessageTypes = settings.SelectedDebugMessageTypes.ToList();
            }
        }

        private void SaveSettings()
        {
            string pathToSettingsFile = GetPathToSettingsFile();

            var settings = new MessageBusWindowSettings
            {
                SelectedMessagesTypes = selectedMessagesTypes.ToArray(),
                SelectedDebugMessageTypes = selectedDebugMessageTypes.ToArray()
            };

            var dataAsJson = JsonUtility.ToJson(settings);
            File.WriteAllText(pathToSettingsFile, dataAsJson);
        }

        private string GetPathToSettingsFile()
        {
            return Path.Combine(Application.temporaryCachePath, "MessageBusWindowSettings.json");
        }
    }
}