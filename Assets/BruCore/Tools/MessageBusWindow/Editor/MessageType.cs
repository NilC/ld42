namespace Brutime.BruCore.Editor
{
    using System;
    using UnityEngine;

    [Serializable]
    public class MessageType
    {
        [SerializeField]
        public string TypeName;

        [SerializeField]
        public string TypeFullName;

        [SerializeField]
        public bool IsEnabled;
    }
}