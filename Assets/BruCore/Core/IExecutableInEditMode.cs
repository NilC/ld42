﻿namespace Brutime.BruCore
{
    public interface IExecutableInEditMode
    {
#if UNITY_EDITOR
        bool IsExecutingInEditMode();
#endif
    }
}
