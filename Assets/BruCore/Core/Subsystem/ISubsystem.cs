﻿namespace Brutime.BruCore
{
    public interface ISubsystem : IExecutableInEditMode
    {
        string Name { get; set; }
        bool IsEnabled { get; set; }

        void Start();
        void Update();
        void FixedUpdate();

#if UNITY_EDITOR
        void OnDrawGizmos();
#endif
    }
}