﻿namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

    public abstract class Subsystem : ISubsystem
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }

        protected Subsystem()
        {
            Name = GetType().Name;
            IsEnabled = true;
        }

        public virtual void Start()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void FixedUpdate()
        {
        }

#if UNITY_EDITOR
        public virtual void OnDrawGizmos()
        {
        }

        private bool isExecutingInEditMode;
        private bool isExecutingInEditModeSet;

        public bool IsExecutingInEditMode()
        {
            if (!isExecutingInEditModeSet)
            {
                isExecutingInEditMode = Attribute.GetCustomAttribute(GetType(), typeof(ExecuteInEditMode)) != null;
                isExecutingInEditModeSet = true;
            }

            return isExecutingInEditMode;
        }
#endif
    }
}