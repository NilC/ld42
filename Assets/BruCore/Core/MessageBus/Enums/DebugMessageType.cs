﻿namespace Brutime.BruCore
{
    public enum DebugMessageType
    {
        Published,
        Got,
        GotAll,
        Cleared
    }
}
