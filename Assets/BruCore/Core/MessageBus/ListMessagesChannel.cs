namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Diagnostics;
#endif

    [ExecuteInEditMode]
    public class ListMessagesChannel<T> : IMessagesChannel where T : struct
    {
        private readonly List<T> messages = new List<T>(CoreMessageBus.InitialMessagesCapacity);

        public void Publish(T message)
        {
            messages.Add(message);

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Published, message);
#endif
        }

        public List<T> GetAll()
        {
#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.GotAll);
#endif

            return messages;
        }

        public void Clear()
        {
            messages.Clear();

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Cleared);
#endif
        }

#if UNITY_EDITOR
        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, typeof(T).Name);
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, T message)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, message.ToString());
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, string debugMessage)
        {
            var stackTrace = new StackTrace();
            var method = stackTrace.GetFrame(3).GetMethod();
            var calledFrom = string.Format("{0}.{1}", method.ReflectedType.Name, method.Name);
            CoreMessageBus.AddDebugMessage(debugMessageType, typeof(T), debugMessage, calledFrom);
        }
#endif
    }
}