﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;

    public class FrameDebugMessage
    {
        public int FrameNumber;
        public List<DebugMessage> DebugMessages;
    }
}