﻿namespace Brutime.BruCore
{
    using System;

    public class DebugMessage
    {
        public DebugMessageType DebugMessageType;
        public Type MessageType;
        public string Message;
    }
}