namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Diagnostics;
#endif

    [ExecuteInEditMode]
    public class DictionaryMessagesChannel<T> : IMessagesChannel where T : struct
    {
        private readonly Dictionary<int, T> messages = new Dictionary<int, T>(CoreMessageBus.InitialMessagesCapacity);

        public void Publish(int messageKey, T message)
        {
            messages.Add(messageKey, message);

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Published, message);
#endif
        }

        public bool Contains(int messageKey)
        {
            return messages.ContainsKey(messageKey);
        }

        public bool TryGetValue(int messageKey, out T message)
        {
#if UNITY_EDITOR
            var contains = messages.TryGetValue(messageKey, out message);
            if (contains)
            {
                AddDebugMessageIfNeeded(DebugMessageType.Got, message);
            }

            return contains;
#else
            return messages.TryGetValue(messageKey, out message);
#endif
        }

        public Dictionary<int, T> GetAll()
        {
#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.GotAll);
#endif
            return messages;
        }

        public void Clear()
        {
            messages.Clear();

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Cleared);
#endif
        }

        public bool IsEmpty()
        {
            return messages.Count == 0;
        }

#if UNITY_EDITOR
        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, typeof(T).Name);
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, T message)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, message.ToString());
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, string debugMessage)
        {
            var stackTrace = new StackTrace();
            var method = stackTrace.GetFrame(3).GetMethod();
            var calledFrom = string.Format("{0}.{1}", method.ReflectedType.Name, method.Name);
            CoreMessageBus.AddDebugMessage(debugMessageType, typeof(T), debugMessage, calledFrom);
        }
#endif
    }
}