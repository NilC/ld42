﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Reflection;

#if UNITY_EDITOR
    using System;
#endif

    [ExecuteInEditMode]
    public class CoreMessageBus : Controller
    {
        public static readonly bool IsDebugMessagesEnabled = false;

        public const int InitialMessagesCapacity = 30;
        public const int FrameDebugMessageCount = 180;

        protected void Clear()
        {
            ClearChannels();

#if UNITY_EDITOR
            if (IsDebugMessagesEnabled)
            {
                frameDebugMessages.Clear();
            }            
#endif
        }

        private void ClearChannels()
        {
            var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.Static);
            for (var i = 0; i < fields.Length; i++)
            {
                var channel = fields[i].GetValue(null) as IMessagesChannel;
                if (channel != null)
                {
                    channel.Clear();
                }
            }
        }

#if UNITY_EDITOR
        private static int currentFrameNumber = 1;
        private static readonly List<FrameDebugMessage> frameDebugMessages = new List<FrameDebugMessage>(FrameDebugMessageCount);

        public static void StartFrame()
        {
            if (frameDebugMessages.Count == frameDebugMessages.Capacity)
            {
                frameDebugMessages.RemoveAt(0);
            }

            frameDebugMessages.Add(new FrameDebugMessage
            {
                FrameNumber = currentFrameNumber,
                DebugMessages = new List<DebugMessage>(100)
            });

            currentFrameNumber++;
        }

        public static void AddDebugMessage(DebugMessageType debugMessageType, Type messageType, string debugMessage, string calledFrom)
        {
            if (frameDebugMessages.Count == 0)
            {
                return;
            }

            var debugMessageTypeString = GetDebugMessageTypeString(debugMessageType);
            var fullDebugMessage = string.Format("{0}: {1}. Called From: {2}", debugMessageTypeString, debugMessage, calledFrom);
            frameDebugMessages[frameDebugMessages.Count - 1].DebugMessages.Add(new DebugMessage
            {
                DebugMessageType = debugMessageType,
                MessageType = messageType,
                Message = fullDebugMessage
            });
        }

        public static List<FrameDebugMessage> GetFrameDebugMessages()
        {
            return frameDebugMessages;
        }

        private static string GetDebugMessageTypeString(DebugMessageType debugMessageType)
        {
            switch (debugMessageType)
            {
                case DebugMessageType.Published: return "Published";
                case DebugMessageType.Got: return "Got";
                case DebugMessageType.GotAll: return "Got All";
                case DebugMessageType.Cleared: return "Cleared";

                default:
                    throw new UnityException("Unknown DebugMessageType");
            }
        }
#endif

    }
}