namespace Brutime.BruCore
{
    using UnityEngine;

#if UNITY_EDITOR
    using System.Diagnostics;
#endif

    [ExecuteInEditMode]
    public class SingleMessageChannel<T> : IMessagesChannel where T : struct
    {
        private bool isMessageSet;
        private T message;

        public void Publish(T message)
        {
            this.message = message;
            isMessageSet = true;

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Published, message);
#endif
        }

        public bool Contains()
        {
            return isMessageSet;
        }

        public T Get()
        {
#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Got, message);
#endif
            return message;
        }

        public void Clear()
        {
            isMessageSet = false;

#if UNITY_EDITOR
            AddDebugMessageIfNeeded(DebugMessageType.Cleared);
#endif
        }

        public bool IsEmpty()
        {
            return !isMessageSet;
        }

#if UNITY_EDITOR
        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, typeof(T).Name);
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, T message)
        {
            if (CoreMessageBus.IsDebugMessagesEnabled)
            {
                AddDebugMessageIfNeeded(debugMessageType, message.ToString());
            }
        }

        private void AddDebugMessageIfNeeded(DebugMessageType debugMessageType, string debugMessage)
        {
            var stackTrace = new StackTrace();
            var method = stackTrace.GetFrame(3).GetMethod();
            var calledFrom = string.Format("{0}.{1}", method.ReflectedType.Name, method.Name);
            CoreMessageBus.AddDebugMessage(debugMessageType, typeof(T), debugMessage, calledFrom);
        }
#endif
    }
}