﻿#if UNITY_EDITOR

namespace Brutime.BruCore
{
    using UnityEditor;
    using UnityEngine;

    public class GizmoPropertiesDrawer
    {
        public void Draw(int entityId, GizmoPropertiesProvider[] propertiesProviders)
        {
            var text = string.Empty;
            for (var i = 0; i < propertiesProviders.Length; i++)
            {
                var properties = propertiesProviders[i].GetProperties(entityId);
                for (var j = 0; j < properties.Count; j++)
                {
                    text += string.Format("{0}: {1}\n", properties[j].Name, properties[j].Value);
                }
            }

            var transform = CoreComponents.Entities[entityId].GameObject.transform;

            var startPosition = new Vector3(transform.position.x - 0.2f, transform.position.y - 0.4f, transform.position.z);

            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.white;
            style.alignment = TextAnchor.MiddleLeft;

            Handles.Label(startPosition, text, style);
        }
    }
}

#endif