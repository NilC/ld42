﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;

    public abstract class GizmoPropertiesProvider
    {
        public abstract List<GizmoProperty> GetProperties(int entityId);
    }
}
