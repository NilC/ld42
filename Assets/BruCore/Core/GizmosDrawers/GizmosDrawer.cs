namespace Brutime.BruCore
{
    using UnityEngine;

    public abstract class GizmosDrawer
    {
        public abstract void Draw(int entityId);

        protected void DrawCross(Vector3 position, Color color)
        {
            DrawCross(position, 0.2f, color);
        }

        protected void DrawCross(Vector3 position, float size, Color color)
        {
            Gizmos.color = color;

            Gizmos.DrawLine(
                position + new Vector3(-size, 0.0f, 0.0f),
                position + new Vector3(size, 0.0f, 0.0f));

            Gizmos.DrawLine(
                position + new Vector3(0.0f, -size, 0.0f),
                position + new Vector3(0.0f, size, 0.0f));
        }

        protected void DrawRectangle(float minX, float minY, float maxX, float maxY, Color color)
        {
            Gizmos.color = color;
            Gizmos.DrawLine(new Vector3(minX, minY), new Vector3(maxX, minY));
            Gizmos.DrawLine(new Vector3(maxX, minY), new Vector3(maxX, maxY));
            Gizmos.DrawLine(new Vector3(maxX, maxY), new Vector3(minX, maxY));
            Gizmos.DrawLine(new Vector3(minX, maxY), new Vector3(minX, minY));
        }

        protected void DrawEllipse(Vector3 center, float width, float height, Color color)
        {
            Gizmos.color = color;

            var segmentsCount = 120;

            float theta = 2.0f * Mathf.PI / segmentsCount;
            float c = Mathf.Cos(theta);
            float s = Mathf.Sin(theta);
            float t;

            float x = 1;
            float y = 0;

            var previousPoint = new Vector3(center.x + width, center.y, 0.0f);

            for (int ii = 0; ii <= segmentsCount; ii++)
            {
                var currentPoint = new Vector3(center.x + x * width, center.y + y * height, 0.0f);

                Gizmos.DrawLine(previousPoint, currentPoint);

                previousPoint = currentPoint;

                //apply the rotation matrix
                t = x;
                x = c * x - s * y;
                y = s * t + c * y;
            }
        }
    }
}
