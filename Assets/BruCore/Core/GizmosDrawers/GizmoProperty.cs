namespace Brutime.BruCore
{
    public struct GizmoProperty
    {
        public GizmoProperty(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name;
        public string Value;
    }
}