﻿namespace Brutime.BruCore
{
    public interface IGizmosProvider
    {
#if UNITY_EDITOR
        GizmosDrawer[] GetGizmosDrawers();
        GizmoPropertiesProvider[] GetGizmoPropertiesProviders();
#endif
    }
}
