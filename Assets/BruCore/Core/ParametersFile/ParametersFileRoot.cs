﻿namespace Brutime.BruCore
{
    using System;
    using UnityEngine;
    
    [Serializable]
    public struct ParametersFileRoot
    {
        [SerializeField]
        public ParametersFileItem[] Parameters;
    }
}