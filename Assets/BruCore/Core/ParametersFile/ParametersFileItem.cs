﻿namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct ParametersFileItem
    {
        [SerializeField]
        public string Key;

        [SerializeField]
        public string Value;
    }
}