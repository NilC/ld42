﻿namespace Brutime.BruCore
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public class ParametersFile
    {
        private const string pathToFilePrefix = "Assets/Resources/Parameters/";
        private const string fileExtension = "json";

        private Dictionary<string, string> parameters = new Dictionary<string, string>();

        public bool Load(string parametersFile)
        {
            parameters.Clear();

            var pathToParametersFile = GetPathToParametersFile(parametersFile);
            if (!File.Exists(pathToParametersFile))
            {
                return false;
            }
            
            var json = File.ReadAllText(pathToParametersFile);
            var parametersFileRoot = JsonUtility.FromJson<ParametersFileRoot>(json);

            if (parametersFileRoot.Parameters == null)
            {
                return false;
            }

            foreach (var parametersFileItem in parametersFileRoot.Parameters)
            {
                parameters.Add(parametersFileItem.Key, parametersFileItem.Value);
            }

            return true;
        }

        public bool Save(string parametersFile)
        {
            var json = JsonUtility.ToJson(parameters);
            var pathToParametersFile = GetPathToParametersFile(parametersFile);
            File.WriteAllText(pathToParametersFile, json);
            return true;
        }

        public T[] GetArrayParameter<T>(string parameterName)
        {
            CheckParameterName(parameterName);

            var stringParameters = parameters[parameterName].Split(',');
            var arrayParameters = new T[stringParameters.Length];
            for (var i = 0; i < stringParameters.Length; i++)
            {
                arrayParameters[i] = (T)Convert.ChangeType(stringParameters[i], typeof(T));
            }
            return arrayParameters;
        }

        public T GetParameter<T>(string parameterName)
        {
            CheckParameterName(parameterName);

            return (T)Convert.ChangeType(parameters[parameterName], typeof(T));
        }

        private void CheckParameterName(string parameterName)
        {
            if (!parameters.ContainsKey(parameterName))
            {
                throw new UnityException(string.Format("Parameter with name {0} not found", parameterName));
            }
        }

        private static string GetPathToParametersFile(string parametersFile)
        {
            return string.Format("{0}/{1}.{2}", pathToFilePrefix, parametersFile, fileExtension);
        }
    }
}