﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;

    public class BehaviorStateMachine
    {
        private readonly Dictionary<int, BehaviorState> states = new Dictionary<int, BehaviorState>(10);

        public int UpdateStates(int entityId, int currentStateId)
        {
            var currentState = states[currentStateId];
            currentState.Update(entityId);
            var nextStateId = currentState.GetNextStateId(entityId);
            if (currentState.StateId != nextStateId)
            {
                currentState.OnLeave(entityId);
                var nextState = states[nextStateId];
                nextState.OnEnter(currentState, entityId);

                return nextStateId;
            }

            return currentState.StateId;
        }

        public void AddState(BehaviorState state)
        {
            states.Add(state.StateId, state);
        }

        public BehaviorState GetState(int stateId)
        {
            return states[stateId];
        }
    }
}
