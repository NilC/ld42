﻿namespace Brutime.BruCore
{
    public abstract class BehaviorState 
    {
        public abstract int StateId { get; }

        public virtual void OnEnter(BehaviorState previousState, int entityId)
        {
        }

        public virtual void OnLeave(int entityId)
        {
        }

        public virtual void Update(int entityId)
        {
        }

        public abstract int GetNextStateId(int entityId);
    }
}
