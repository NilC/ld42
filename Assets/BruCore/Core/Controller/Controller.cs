﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class Controller : MonoBehaviour
    {
        protected static T FindComponentWithTagOnScene<T>(string tag) where T : Behaviour
        {
            var gameObject = GameObject.FindGameObjectWithTag(tag);
            if (gameObject == null)
            {
                throw new UnityException(string.Format("Dependency with tag \"{0}\" not found on scene", tag));
            }

            return gameObject.GetComponent<T>();
        }          
    }
}