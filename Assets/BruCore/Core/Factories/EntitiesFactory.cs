namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    [ExecuteInEditMode]
    public class EntitiesFactory : Controller
    {
        public static string Tag = "EntitiesFactory";

        [SerializeField]
        private EntitiesDatabase entitiesDatabase;

        [SerializeField]
        private PoolCapacityRecord[] capacitiesRecords;

        private Dictionary<int, Pool> pools = new Dictionary<int, Pool>();
        private GameObject poolsRoot;

        private void Start()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
#endif
            {
                CreatePools();
            }

            var allExistingentityComponentDataProviders = GetAllExistingentityComponentDataProviders();

            foreach (var entityComponentDataProvider in allExistingentityComponentDataProviders)
            {
                CoreSubsystems.Entities.Add(entityComponentDataProvider, AddingType.WithoutInitialization);
            }

            foreach (var entityComponentDataProvider in allExistingentityComponentDataProviders)
            {
                var entityId = CoreSubsystems.Entities.GetEntityIdByGameObjectEntityId(entityComponentDataProvider.gameObject.GetEntityId());
                var entityComponent = CoreComponents.Entities[entityId];
                CoreSubsystems.Entities.InitializeComponent(entityComponent);
#if UNITY_EDITOR
                if (Application.isPlaying && entityComponentDataProvider.MustBeInPool)
#else
                if (entityComponentDataProvider.MustBeInPool)
#endif
                {
                    AddToPoolExistingObject(entityComponent.EntityDatabaseId, entityId);
                }
            }
        }

        public int CreateEntity(int entityDatabaseId, Vector3 position)
        {
            CheckIsPoolContainsEntityDatabaseId(entityDatabaseId);

            var newEntityId = pools[entityDatabaseId].Pop(position);

            return newEntityId;
        }

        public void DestroyEntity(int entityDatabaseId, int entityId)
        {
            CheckIsPoolContainsEntityDatabaseId(entityDatabaseId);

            pools[entityDatabaseId].Push(entityId);
        }

        public void AddToPoolExistingObject(int entityDatabaseId, int entityId)
        {
            CheckIsPoolContainsEntityDatabaseId(entityDatabaseId);

            pools[entityDatabaseId].Add(entityId);
        }

        public EntitiesDatabase GetEntitiesDatabase()
        {
            return entitiesDatabase;
        }

        private void CreatePools()
        {
            poolsRoot = new GameObject("PoolsRoot");
            poolsRoot.transform.parent = transform;

            foreach (var entityRecordGroup in entitiesDatabase.EntityRecordGroups)
            {
                foreach (var entityRecord in entityRecordGroup.EntitiesRecords)
                {
                    var capacity = GetCapacity(entityRecord.EntityDatabaseId);
                    var pool = new Pool(poolsRoot, entityRecord.GameObject, capacity, 1);
                    pools.Add(entityRecord.EntityDatabaseId, pool);
                }
            }
        }

        private int GetCapacity(int entityDatabaseId)
        {
            for (var i = 0; i < capacitiesRecords.Length; i++)
            {
                var capacityRecord = capacitiesRecords[i];
                if (capacityRecord.Id == entityDatabaseId)
                {
                    return capacityRecord.Capacity;
                }
            }

            throw new UnityException(string.Format("Can not find capacity data for entity record id {0}", entityDatabaseId));
        }

        private void CheckIsPoolContainsEntityDatabaseId(int entityDatabaseId)
        {
            if (!pools.ContainsKey(entityDatabaseId))
            {
                throw new UnityException(string.Format("Can not find pool for entity database id {0}", entityDatabaseId));
            }
        }

        private EntityComponentDataProvider[] GetAllExistingentityComponentDataProviders()
        {
            return FindObjectsOfType<EntityComponentDataProvider>();
        }
    }
}