﻿namespace Brutime.BruCore
{
    using System;
    using System.Collections.Generic;

    public interface IComponentsContainer : IExecutableInEditMode
    {
        string Name { get; set; }

        //void Initialize(int componentsCapacity);

        //void CreateNewComponentAndAddWithCast(EntityComponent entityComponent, ComponentAttributes attributes, AddingType addingType);

        void CreateNewComponentAndAddWithCast(int entityId, IComponentDataProvider componentDataProvider);

        void InitializeComponent(int entityId);
        void RemoveComponent(int entityId);
        void ResetComponent(int entityId);
        void PushComponentToPool(int entityId);
        void PopComponentFromPool(int currentEntityId, int newEntityId);

        HashSet<int> GetEntitiesIds();

        Type GetComponentType();
        //Type GetComponentAttributesType();

        void Clear();
    }
}
