﻿namespace Brutime.BruCore
{
    public abstract class CommonComponentsContainer<TComponentType, TComponentDataProviderType> : ComponentsContainer<TComponentType, TComponentDataProviderType>
        where TComponentType : struct
        where TComponentDataProviderType : CommonComponentDataProvider<TComponentType>
    {
        public override void GetOrCreateNewComponentAndAdd(int entityId, TComponentDataProviderType componentDataProvider)
        {
            var component = componentDataProvider.GetComponent();
            AddComponent(entityId, component);
        }
    }
}
