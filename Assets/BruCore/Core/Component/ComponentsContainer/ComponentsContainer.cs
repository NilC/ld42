﻿namespace Brutime.BruCore
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class ComponentsContainer<TComponentType, TComponentDataProviderType> : IComponentsContainer
        where TComponentType : struct
        where TComponentDataProviderType : class, IComponentDataProvider
    {
        public string Name { get; set; }

        private const int ComponentsCapacity = 1000;

        // To change component in foreach
        protected HashSet<int> EntitiesIds = new HashSet<int>();
        protected Dictionary<int, TComponentType> Components;
        protected Dictionary<int, TComponentType> CachedComponents;

        public ComponentsContainer()
        {
            Name = GetType().Name;

            Components = new Dictionary<int, TComponentType>(ComponentsCapacity);
            CachedComponents = new Dictionary<int, TComponentType>(ComponentsCapacity);
            EntitiesIds = new HashSet<int>(new List<int>(ComponentsCapacity));
        }

        public void CreateNewComponentAndAddWithCast(int entityId, IComponentDataProvider componentDataProvider)
        {
            GetOrCreateNewComponentAndAdd(entityId, componentDataProvider as TComponentDataProviderType);
        }

        public abstract void GetOrCreateNewComponentAndAdd(int entityId, TComponentDataProviderType componentDataProvider);

        public virtual void InitializeComponent(int entityId)
        {
        }

        public virtual void AddComponent(int entityId, TComponentType component)
        {
            Components.Add(entityId, component);
            EntitiesIds.Add(entityId);
        }

        public virtual void AddComponentToPool(int entityId, TComponentType component)
        {
            CachedComponents.Add(entityId, component);
        }

        public virtual void RemoveComponent(int entityId)
        {
            Components.Remove(entityId);
            EntitiesIds.Remove(entityId);
        }

        public virtual void ResetComponent(int entityId)
        {
        }

        public virtual void PopComponentFromPool(int currentEntityId, int newEntityId)
        {
            var component = CachedComponents[currentEntityId];
            Components.Add(newEntityId, component);
            EntitiesIds.Add(newEntityId);
            CachedComponents.Remove(currentEntityId);
        }

        public virtual void PushComponentToPool(int entityId)
        {
            var component = Components[entityId];
            CachedComponents.Add(entityId, component);
            Components.Remove(entityId);
            EntitiesIds.Remove(entityId);
        }

        public virtual bool Contains(int entityId)
        {
            return Components.ContainsKey(entityId);
        }

        public virtual TComponentType this[int entityId]
        {
            get
            {
#if UNITY_EDITOR
                if (!Components.ContainsKey(entityId))
                {
                    throw new UnityException(string.Format("Entity with entity id {0} not found", entityId));
                }
#endif

                return Components[entityId];
            }
            set
            {
#if UNITY_EDITOR
                if (!Components.ContainsKey(entityId))
                {
                    throw new UnityException(string.Format("Entity with entity id {0} not found", entityId));
                }
#endif

                Components[entityId] = value;
            }
        }

        public virtual HashSet<int> GetEntitiesIds()
        {
            return EntitiesIds;
        }

        public virtual bool TryGet(int entityId, out TComponentType component)
        {
            return Components.TryGetValue(entityId, out component);
        }

        public Type GetComponentType()
        {
            return typeof(TComponentType);
        }

        //public Type GetComponentAttributesType()
        //{
        //    return typeof(TAttributesType);
        //}

        public void Clear()
        {
            EntitiesIds.Clear();
            Components.Clear();
            CachedComponents.Clear();
        }

#if UNITY_EDITOR
        private bool isExecutingInEditMode;
        private bool isExecutingInEditModeSet;

        public bool IsExecutingInEditMode()
        {
            if (!isExecutingInEditModeSet)
            {
                isExecutingInEditMode = Attribute.GetCustomAttribute(GetType(), typeof(ExecuteInEditMode)) != null;
                isExecutingInEditModeSet = true;
            }

            return isExecutingInEditMode;
        }
#endif
    }
}
