﻿namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    public abstract class ComponentDataProvider : ScriptableObject, IComponentDataProvider, IGizmosProvider
    {
        public virtual void Initialize()
        {
        }

        public abstract Type GetComponentType();

#if UNITY_EDITOR
        public virtual void ApplyDefaultValues()
        {
        }

        public virtual bool IsRequired()
        {
            return false;
        }

        public virtual string GetCaption()
        {
            return GetType().Name.Replace("DataProvider", string.Empty).AsNameInWords();
        }

        public virtual bool IsUsingSerializedObject()
        {
            return false;
        }

        public virtual void InitializeLayout()
        {
        }

        public virtual void InitializeLayout(SerializedObject serializedObject)
        {
        }

        public virtual void DrawLayout()
        {
        }

        public virtual void DrawLayout(SerializedObject serializedObject)
        {
        }

        public virtual GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[0];
        }

        public virtual GizmoPropertiesProvider[] GetGizmoPropertiesProviders()
        {
            return new GizmoPropertiesProvider[0];
        }
#endif
    }
}