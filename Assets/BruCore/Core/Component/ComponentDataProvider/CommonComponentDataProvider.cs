﻿namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

#if UNITY_EDITOR
    using System.Reflection;
    using UnityEditor;
    using System.Linq;
#endif

    public abstract class CommonComponentDataProvider<TComponentType> : ComponentDataProvider where TComponentType : new()
    {
        [SerializeField]
        protected TComponentType Component = new TComponentType();

        public override Type GetComponentType()
        {
            return typeof(TComponentType);
        }

        public virtual TComponentType GetComponent()
        {
            return Component;
        }

#if UNITY_EDITOR
        public virtual string[] GetHiddenFieldsNames()
        {
            return new string[0];
        }

        public override void DrawLayout()
        {
            object componentObject = Component;            

            var fieldsInfos = componentObject.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            var hiddenFieldsNames = GetHiddenFieldsNames();

            fieldsInfos = fieldsInfos
                .Where(x => !hiddenFieldsNames.Contains(x.Name))
                .ToArray();

            for (var i = 0; i < fieldsInfos.Length; i++)
            {
                var fieldsInfo = fieldsInfos[i];
                var fieldType = fieldsInfo.FieldType;
                var fieldNameInWords = fieldsInfo.Name.AsNameInWords();

                if (fieldType == typeof(bool))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.Toggle(fieldNameInWords, (bool)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(int))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.IntField(fieldNameInWords, (int)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(long))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.LongField(fieldNameInWords, (long)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(float))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.FloatField(fieldNameInWords, (float)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(string))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.TextField(fieldNameInWords, (string)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(Vector2))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.Vector2Field(fieldNameInWords, (Vector2)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(Vector3))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.Vector3Field(fieldNameInWords, (Vector3)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType == typeof(Vector4))
                {
                    fieldsInfo.SetValue(componentObject, EditorGUILayout.Vector4Field(fieldNameInWords, (Vector4)fieldsInfo.GetValue(componentObject)));
                }
                else if (fieldType.IsEnum)
                {
                    var enumIndex = (int)fieldsInfo.GetValue(componentObject);
                    var enumNames = Enum.GetNames(fieldType);

                    var selectedEnumIndex = EditorGUILayout.Popup(fieldNameInWords, enumIndex, enumNames);

                    var enumValue = Enum.GetValues(fieldType).GetValue(selectedEnumIndex);
                    fieldsInfo.SetValue(componentObject, enumValue);
                }
                else if (typeof(ScriptableObject).IsAssignableFrom(fieldType))
                {
                    var scriptableObject = (ScriptableObject)EditorGUILayout.ObjectField(fieldNameInWords,
                        (ScriptableObject)fieldsInfo.GetValue(componentObject), fieldType, false);
                    fieldsInfo.SetValue(componentObject, scriptableObject);
                }
                else
                {
                    throw new UnityException(string.Format("Unknown field type: {0}", fieldType));
                }
            }

            Component = (TComponentType)componentObject;
        }
#endif
    }
}
