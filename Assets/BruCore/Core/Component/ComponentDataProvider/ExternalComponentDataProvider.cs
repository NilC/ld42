﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class ExternalComponentDataProvider : MonoBehaviour, IComponentDataProvider, IGizmosProvider
    {
#if UNITY_EDITOR
        public virtual GizmosDrawer[] GetGizmosDrawers()
        {
            return new GizmosDrawer[0];
        }

        public virtual GizmoPropertiesProvider[] GetGizmoPropertiesProviders()
        {
            return new GizmoPropertiesProvider[0];
        }
#endif
    }
}