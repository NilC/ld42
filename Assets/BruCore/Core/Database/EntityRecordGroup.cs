namespace Brutime.BruCore
{
    using System;

    [Serializable]
    public struct EntityRecordGroup
    {
        public EntityRecordGroup(string name)
        {
            Name = name;
            EntitiesRecords = new EntityRecord[0];
        }

        public string Name;
        public EntityRecord[] EntitiesRecords;
    }
}