﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class ScriptableObjectsDatabase : ScriptableObject
    {
        public ScriptableObjectsRecordGroup[] ScriptableObjectRecordGroups = { };
    }
}