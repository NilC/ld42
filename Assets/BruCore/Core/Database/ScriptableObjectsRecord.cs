namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct ScriptableObjectsRecord
    {
        public int Id;
        public ScriptableObject ScriptableObject;

        public ScriptableObjectsRecord(int id, ScriptableObject scriptableObject)
        {
            Id = id;
            ScriptableObject = scriptableObject;
        }
    }
}