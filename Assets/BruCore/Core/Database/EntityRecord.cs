namespace Brutime.BruCore
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct EntityRecord
    {
        public int EntityDatabaseId;
        public GameObject GameObject;

        public EntityRecord(int entityDatabaseId, GameObject gameObject)
        {
            EntityDatabaseId = entityDatabaseId;
            GameObject = gameObject;
        }
    }
}