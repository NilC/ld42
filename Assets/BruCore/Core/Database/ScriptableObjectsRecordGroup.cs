namespace Brutime.BruCore
{
    using System;

    [Serializable]
    public struct ScriptableObjectsRecordGroup
    {
        public ScriptableObjectsRecordGroup(string name)
        {
            Name = name;
            ScriptableObjectsRecords = new ScriptableObjectsRecord[0];
        }

        public string Name;
        public ScriptableObjectsRecord[] ScriptableObjectsRecords;
    }
}