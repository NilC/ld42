﻿namespace Brutime.BruCore
{
    using UnityEngine;

    public class EntitiesDatabase : ScriptableObject
    {
        public EntityRecordGroup[] EntityRecordGroups = { };
    }
}