namespace Brutime.BruCore
{
    using System;

    [Serializable]
    public struct PoolCapacityRecord
    {
        public int Id;
        public int Capacity;

        public PoolCapacityRecord(int id, int capacity)
        {
            Id = id;
            Capacity = capacity;
        }
    }
}