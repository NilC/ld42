﻿namespace Brutime.BruCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public class Pool
    {
        private static readonly Vector3 HiddenPosition = new Vector3(-100.0f, -100.0f, 0.0f);

        private readonly GameObject poolRoot;
        private readonly Transform poolRootTransform;

        public readonly GameObject MasterObject;
        private readonly int growingCount;
        //private readonly PoolWarmOnLoadPolicy poolWarmOnLoadPolicy; // ToDo
        //private readonly PoolResetTransformsPolicy poolResetTransformsPolicy;  // ToDo

        private readonly Stack<EntityComponent> pool = new Stack<EntityComponent>();
        private readonly List<EntityComponent> linkedObjectLogics = new List<EntityComponent>();

        private bool isLocked;

        public Pool(
            GameObject poolsRoot,
            GameObject masterObject,
            int capacity,
            int growingCount)
        //PoolWarmOnLoadPolicy poolWarmOnLoadPolicy,
        //PoolResetTransformsPolicy poolResetTransformsPolicy
        {
            poolRoot = new GameObject("Pool " + masterObject.name);
            poolRoot.transform.parent = poolsRoot.transform;
            poolRootTransform = poolRoot.transform;

            MasterObject = masterObject;

            this.growingCount = growingCount;
            //this.poolWarmOnLoadPolicy = poolWarmOnLoadPolicy;
            //this.poolResetTransformsPolicy = poolResetTransformsPolicy;

            Create(capacity);
        }

        //public void Diagnostics()
        //{
        //    var index = 0;
        //    foreach (var EntityComponent in pool)
        //    {
        //        if (EntityComponent == null)
        //        {
        //            Debug.Log("Null in pool: " + poolRoot.name + "; index: " + index);
        //        }
        //        index++;
        //    }
        //}

        public int Pop(Vector3 position)
        {
            return Pop(poolRootTransform, position, 0);
        }

        public int Pop(Transform parent, Vector3 position)
        {
            return Pop(parent, position, 0);
        }

        public int Pop(Vector3 position, int deltaSortingOrder)
        {
            return Pop(poolRootTransform, position, deltaSortingOrder);
        }

        public int Pop(Transform parent, Vector3 position, int deltaSortingOrder)
        {
            if (isLocked)
            {
                throw new UnityException("The pool is locked");
            }

            if (pool.Count == 0)
            {
                //Debug.Log("Pool is empty. Prefab name " + MasterObject.name);
                Create(growingCount);
            }

            var entityComponent = pool.Pop();
            if (!entityComponent.IsInPool)
            {
                throw new UnityException("Not in pool");
            }

            entityComponent.GameObject.transform.parent = parent;
            entityComponent.GameObject.transform.localPosition = position;

            entityComponent.IsInPool = false;
            entityComponent.GameObject.SetActive(true);

            var newEntityId = CoreSubsystems.Entities.PopComponentFromPool(entityComponent.EntityId);

            return newEntityId;
        }

        public void Push(int entityId)
        {
            var entityComponent = CoreComponents.Entities[entityId];
            CoreSubsystems.Entities.PushComponentToPool(entityId);
            PushInternal(entityComponent);
        }

        public void Add(int entityId)
        {
            var entityComponent = CoreComponents.Entities[entityId];
            entityComponent.GameObject.transform.parent = poolRootTransform;
            entityComponent.Pool = this;
            linkedObjectLogics.Add(entityComponent);
        }

        private void PushInternal(EntityComponent entityComponent)
        {
            if (entityComponent.IsInPool)
            {
                throw new UnityException(entityComponent.GameObject.name + " is already in pool");
            }

            entityComponent.GameObject.SetActive(false);
            entityComponent.GameObject.transform.parent = poolRootTransform;
            entityComponent.IsInPool = true;
            pool.Push(entityComponent);
        }

        public void PushAll()
        {
            //Debug.Log("Pool " + MasterObject.name + " linkedObjectLogics.Count " + linkedObjectLogics.Count);
            for (var i = 0; i < linkedObjectLogics.Count; i++)
            {
                if (linkedObjectLogics[i].IsInPool == false)
                {
                    linkedObjectLogics[i].Pool.Push(linkedObjectLogics[i].EntityId);
                }
            }
        }

        public void Lock()
        {
            isLocked = true;
        }

        public void Unlock()
        {
            isLocked = false;
        }

        public void DeactivateAll()
        {
            for (var i = 0; i < linkedObjectLogics.Count; i++)
            {
                if (linkedObjectLogics[i].IsInPool)
                {
                    linkedObjectLogics[i].GameObject.SetActive(false);
                }
            }
        }

        private void Create(int capacity)
        {
            for (var i = 0; i < capacity; i++)
            {
                var poolObject = Object.Instantiate(MasterObject, HiddenPosition, Quaternion.identity) as GameObject;
                var entityComponentDataProvider = poolObject.GetComponent<EntityComponentDataProvider>();
                var entityId = CoreSubsystems.Entities.Add(entityComponentDataProvider, AddingType.WithoutInitialization);
                var entityComponent = CoreComponents.Entities[entityId];
                entityComponent.Pool = this;
                PushInternal(entityComponent);
                linkedObjectLogics.Add(entityComponent);
                CoreComponents.Entities[entityId] = entityComponent;
                CoreSubsystems.Entities.PushComponentToPool(entityId);
            }
        }
    }
}